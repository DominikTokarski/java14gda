package pl.sda.mock.messageService;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceTest {
    private NotificationService notificationService;

    @Mock
    private PigeonService pigeonService;

    @Mock
    private EmailService emailService;

    @Before
    public void setup() {
        notificationService = new NotificationService(emailService, pigeonService);
    }

    @Test
    public void shouldSendMessageByEmail(){
        String message = "abc";
        when(emailService.isAvailable()).thenReturn(true);
        notificationService.sendNotification(message);
        verify(emailService).sendEmail(message);
    }

    @Test
    public void shouldSendMessageByPigeon(){
        String message = "abc";
        when(pigeonService.isAvailable()).thenReturn(true);
        notificationService.sendNotification(message);
        verify(pigeonService).sendMessage(message);
    }

    @Test
    public void shouldNotSendMessage(){
        String message = "abc";
        when(emailService.isAvailable()).thenReturn(false);
        when(pigeonService.isAvailable()).thenReturn(false);
        notificationService.sendNotification(message);
        verify(emailService,never()).sendEmail(message);
        verify(pigeonService,never()).sendMessage(message);
    }



}


