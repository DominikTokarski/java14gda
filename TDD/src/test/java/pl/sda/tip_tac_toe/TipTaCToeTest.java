package pl.sda.tip_tac_toe;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TipTaCToeTest {
    private TipTacToe tipTacToe;

    @Mock
    private Board board;

    @Before
    public void setup(){
        tipTacToe = new TipTacToe();
        board.createBoard();
    }

    @Test
    public void shouldGetNotWinnerIfBoardIsEmpty(){
        when(board.isEmpty()).thenReturn(true);
        String result = tipTacToe.whoWins(board);
        Assertions.assertThat(result).isEqualTo("Draw");
    }

    @Test
    public void shouldGetWinnerInLine(){
        when(board.getElement(0,0)).thenReturn('x');
        when(board.getElement(0,1)).thenReturn('x');
        when(board.getElement(0,2)).thenReturn('x');
        String result = tipTacToe.whoWins(board);
        Assertions.assertThat(result).isEqualTo("player2");
    }

    @Test
    public void shouldGetWinnerInLineDown(){
        when(board.getElement(0,0)).thenReturn('x');
        when(board.getElement(1,0)).thenReturn('x');
        when(board.getElement(2,0)).thenReturn('x');
        String result = tipTacToe.whoWins(board);
        Assertions.assertThat(result).isEqualTo("player2");
    }

    @Test
    public void shouldGetWinnerInLineCross(){
        when(board.getElement(0,0)).thenReturn('x');
        when(board.getElement(1,1)).thenReturn('x');
        when(board.getElement(2,2)).thenReturn('x');
        String result = tipTacToe.whoWins(board);
        Assertions.assertThat(result).isEqualTo("player2");
    }


}
