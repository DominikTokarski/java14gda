package pl.sda.tip_tac_toe;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class BoardTest {
    private Board board;

    @Before
    public void setup() {
        board = new Board();
        board.createBoard();
    }

    @Test
    public void shouldCreateNewBoard(){
        Assertions.assertThat(board.createBoard()).isEqualTo(new char[3][3]);
    }

    @Test
    public void shouldCheckIfBoardIsEmpty(){
        boolean result = board.isEmpty();
        Assertions.assertThat(result).isEqualTo(true);
    }

    @Test
    public void shouldMadeRightMoveCircle(){
        board.addMove(1,1,'o');
        Assertions.assertThat(board.isEmpty()).isEqualTo(false);
    }

    @Test
    public void shouldMadeRightMoveCross(){
        board.addMove(0,0,'x');
        Assertions.assertThat(board.isEmpty()).isEqualTo(false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForSameIndex(){
        board.addMove(0,0,'o');
        board.addMove(0,0,'x');
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForArgument(){
        board.addMove(0,0,'o');
        board.addMove(1,1,'o');
    }

    @Test
    public void shouldGetElementFromBoard(){
        board.addMove(1,1,'o');
        char result = board.getElement(1,1);
        Assertions.assertThat(result).isEqualTo('o');
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotGetElementFromBoard(){
        board.addMove(1,1,'o');
        char result = board.getElement(4,4);
    }

}
