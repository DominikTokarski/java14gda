package pl.sda.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;
import static org.junit.runners.Parameterized.*;

@RunWith(value = Parameterized.class)
public class CalculatorTestByConstructorSubtract {
    private Calculator calculator = new Calculator();
    private Integer numberA;
    private Integer numberB;
    private Integer expected;

    public CalculatorTestByConstructorSubtract(Integer numberA, Integer numberB, Integer expected) {
        this.numberA = numberA;
        this.numberB = numberB;
        this.expected = expected;
    }

    @Parameters(name = "{index}: testSubtract({0}-{1} = {2}")
    public static Iterable<? extends Object> data(){
        return Arrays.asList(new Object[][]{
                {1,1,0},
                {-5, -5, 0},
                {-3,8,-11}
        });
    }

    @Test
    public void testSubtract(){
        assertEquals(expected,calculator.subtract(numberA,numberB));
    }
}
