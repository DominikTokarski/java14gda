package pl.sda.calculator;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

public class CalculatorTest {
    private Calculator calculator = new Calculator();

    @Test
    public void shouldAddTwoNumbers(){
        int result = calculator.add(2,3);
        Assertions.assertThat(result).isEqualTo(5);
    }

    @Test
    public void shouldSubtractTwoNumbers() {
        int result = calculator.subtract(5, 3);
        Assertions.assertThat(result).isEqualTo(2);
    }

    @Test
    public void shouldMultiplyTwoNumbers() {
        int result = calculator.multiply(2, 3);
        Assertions.assertThat(result).isEqualTo(6);
    }

    @Test
    public void shouldDivideTwoNumbers() {
        int result = calculator.divide(4, 2);
        Assertions.assertThat(result).isEqualTo(2);
    }

    @Test
    public void shouldDivideTwoNumbersSecond()
            throws ArithmeticException{
        try {
            int result = calculator.divide(1, 0);
            fail();
        }catch (IllegalArgumentException exceptionI){
            Assertions.assertThat(exceptionI.getMessage()).isEqualTo("Divisor cannot be 0.");
        }

    }

    @Test
    public void shouldMultiplyTwoNumbersSecond(){
        int result = calculator.multiply(4,0);
        Assertions.assertThat(result).isEqualTo(0);
    }

    @Test
    public void shouldAddTwoNumbersSecond(){
        int result = calculator.add(-2,3);
        Assertions.assertThat(result).isEqualTo(1);
    }

    @Test
    public void shouldSubtractTwoNumbersSecond() {
        int result = calculator.subtract(-4, 3);
        Assertions.assertThat(result).isEqualTo(-7);
    }

    @Test
    public void shouldNotBeEquals(){
        int result = calculator.add(5,5);
        Assertions.assertThat(result).isNotEqualTo(4);
    }

}
