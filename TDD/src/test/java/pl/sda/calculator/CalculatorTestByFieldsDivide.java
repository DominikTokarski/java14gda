package pl.sda.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;
import static org.junit.runners.Parameterized.*;

@RunWith(Parameterized.class)
public class CalculatorTestByFieldsDivide {
    private Calculator calculator = new Calculator();
    @Parameter(value = 0)
    public Integer numberA;
    @Parameter(value = 1)
    public Integer numberB;
    @Parameter(value = 2)
    public Integer expected;


    @Parameters(name = "{index}: testMultiply({0}/{1} = {2}")
    public static Iterable<? extends Object> data(){
        return Arrays.asList(new Object[][]{
                {1,1,1},
                {-5, -5, 1},
                {-3,0,0}
        });
    }

    @Test
    public void testDivide() throws ArithmeticException{
        try {
            assertEquals(expected,calculator.divide(numberA,numberB));
            if (numberB == 0){
                fail();
            }
        }catch (ArithmeticException exceptionA){
            System.out.println(
                    exceptionA.getMessage().equals("/ by zero"));
        }
    }
}
