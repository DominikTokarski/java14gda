package pl.sda.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static junit.framework.TestCase.assertTrue;
import static org.junit.runners.Parameterized.*;

@RunWith(Parameterized.class)
public class CalculatorTestsIsNumberOddByConstructor {
    private Calculator calculator = new Calculator();
    private int number;

    public CalculatorTestsIsNumberOddByConstructor(int number) {
        this.number = number;
    }

    @Parameters(name = "{index}: isOdd{0}")
    public static Iterable<? extends Object> data(){
        return Arrays.asList(-1, 1, 3, 5, 7, 9, 11);
    }

    @Test
    public void testIsOdd(){
        assertTrue(calculator.isOdd(number));
    }
}
