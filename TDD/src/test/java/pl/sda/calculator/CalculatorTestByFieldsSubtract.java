package pl.sda.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;
import static org.junit.runners.Parameterized.*;


@RunWith(value = Parameterized.class)
public class CalculatorTestByFieldsSubtract {
    private Calculator calculator = new Calculator();
    @Parameter(value = 0)
    public Integer numberA;
    @Parameter(value = 1)
    public Integer numberB;
    @Parameter(value = 2)
    public Integer expected;



    @Parameters(name = "{index}: testSubtract({0}-{1} = {2}")
    public static Iterable<? extends Object> data(){
        return Arrays.asList(new Object[][]{
                {1, 1, 0},
                {-5, -5, 0},
                {-3, 8, -11}
        });
    }

    @Test
    public void testSubtract(){
        assertEquals(expected,calculator.subtract(numberA,numberB));
    }
}
