package pl.sda.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;

@RunWith(value = Parameterized.class)
public class CalculatorTestByConstructorMultiply {
    private Calculator calculator = new Calculator();
    private Integer numberA;
    private Integer numberB;
    private Integer expected;

    public CalculatorTestByConstructorMultiply(Integer numberA, Integer numberB, Integer expected) {
        this.numberA = numberA;
        this.numberB = numberB;
        this.expected = expected;
    }

    @Parameterized.Parameters(name = "{index}: testMultiply({0}*{1} = {2}")
    public static Iterable<? extends Object> data(){
        return Arrays.asList(new Object[][]{
                {1,1,1},
                {-5, -5, 25},
                {-3,0,0}
        });
    }

    @Test
    public void testMultiply(){
        assertEquals(expected,calculator.multiply(numberA,numberB));
    }
}
