package pl.sda.calculator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

public class CalculatorTestExceptions {
    private Calculator calculator = new Calculator();

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionByExpected(){
        calculator.divide(2,0);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionByAddByExpected(){
        calculator.add(null,5);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionBySubtractByExpected(){
        calculator.subtract(null,5);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionByMultiplyByExpected(){
        calculator.multiply(null,5);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionByDivideByExpected(){
        calculator.divide(null,5);
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionByTryCatch(){
        try {
            calculator.divide(2,0);
            fail("Operation should have resulted IllegalArgumentException.");
        }catch (IllegalArgumentException exceptionI){
            assertEquals("Divisor cannot be 0.",exceptionI.getMessage());
        }
    }

    @Test
    public void shouldThrowNullPointerExceptionByAddByTryCatch() {
        try {
            calculator.add(null, 5);
            fail("Operation should have resulted in NullPointerException.");
        } catch (NullPointerException exceptionI) {
            assertEquals("Number is null" , exceptionI.getMessage());
        }
    }

    @Test
    public void shouldThrowNullPointerExceptionBySubtractByTryCatch() {
        try {
            calculator.subtract(null, 5);
            fail("Operation should have resulted in NullPointerException.");
        } catch (NullPointerException exceptionI) {
            assertEquals("Number is null" , exceptionI.getMessage());
        }
    }

    @Test
    public void shouldThrowNullPointerExceptionByMultiplyByTryCatch() {
        try {
            calculator.multiply(null, 5);
            fail("Operation should have resulted in NullPointerException.");
        } catch (NullPointerException exceptionI) {
            assertEquals("Number is null" , exceptionI.getMessage());
        }
    }

    @Test
    public void shouldThrowNullPointerExceptionByDivideByTryCatch(){
        try {
            calculator.divide(null,5);
            fail("Operation should have resulted in NullPointerException.");
        }catch (NullPointerException exceptionI){
            assertEquals("Number is null" , exceptionI.getMessage());
        }
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionByRule(){
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Divisor cannot be 0.");

        calculator.divide(2,0);
    }



}
