package pl.sda.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;
import static org.junit.runners.Parameterized.*;

@RunWith(value = Parameterized.class)
public class CalculatorTestByFieldsAdd {
    private Calculator calculator = new Calculator();
    @Parameter(value = 0)
    public Integer numberA;
    @Parameter(value = 1)
    public Integer numberB;
    @Parameter(value = 2)
    public Integer expected;


    @Parameters(name = "{index}: testAdd({0}+{1} = {2}")
    public static Iterable<? extends Object> data() {
        return Arrays.asList(new Object[][]{
                    {1, 1, 2},
                    {-5, -5, -10},
                    {-3, 8, 5}
            });
        }
        @Test
        public void testAdd() {
            assertEquals(expected, calculator.add(numberA, numberB));
        }
}
