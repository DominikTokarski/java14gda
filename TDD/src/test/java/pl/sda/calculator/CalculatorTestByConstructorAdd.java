package pl.sda.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;
import static org.junit.runners.Parameterized.*;

@RunWith(value = Parameterized.class)
public class CalculatorTestByConstructorAdd {
    private Calculator calculator = new Calculator();
    private Integer numberA;
    private Integer numberB;
    private Integer expected;

    public CalculatorTestByConstructorAdd(Integer numberA, Integer numberB, Integer expected) {
        this.numberA = numberA;
        this.numberB = numberB;
        this.expected = expected;
    }

    @Parameters(name = "{index}: testAdd({0}+{1} = {2}")
    public static Iterable<? extends Object> data(){
        return Arrays.asList(new Object[][]{
                {1,1,2},
                {-5, -5, -10},
                {-3,8,5}
        });
    }

    @Test
    public void testAdd(){
        assertEquals(expected,calculator.add(numberA,numberB));
    }
}

