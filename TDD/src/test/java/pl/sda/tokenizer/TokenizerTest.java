package pl.sda.tokenizer;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class TokenizerTest {
    private Tokenizer tokenizer;

    @Before
    public void setup(){
        tokenizer = new Tokenizer();
    }

    @Test
    public void shouldReturnEmptyArrayIfNull(){
        ArrayList<String> result = tokenizer.getContent(null);
        ArrayList<String> check = new ArrayList<String>();
        Assertions.assertThat(result).isEqualTo(check);
    }

    @Test
    public void shouldReturnEmptyArrayIfEmpty(){
        ArrayList<String> result = tokenizer.getContent("");
        ArrayList<String> check = new ArrayList<String>();
        Assertions.assertThat(result).isEqualTo(check);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldReturnIllegalArgumentExceptionWhenLetters(){
        tokenizer.getContent("abc");
    }

    @Test
    public void shouldReturnOneElementArray(){
        ArrayList<String> result = tokenizer.getContent("1.0");
        ArrayList<String> check = new ArrayList<String>();
        check.add("1.0");
        Assertions.assertThat(result).isEqualTo(check);
    }

    @Test
    public void shouldReturnOThreeElementArray(){
        ArrayList<String> result = tokenizer.getContent("1+1");
        ArrayList<String> check = new ArrayList<String>();
        check.add("1");
        check.add("+");
        check.add("1");
        Assertions.assertThat(result).isEqualTo(check);
    }

    @Test
    public void shouldReturnOThreeElementArraySecond(){
        ArrayList<String> result = tokenizer.getContent("1/2");
        ArrayList<String> check = new ArrayList<String>();
        check.add("1");
        check.add("/");
        check.add("2");
        Assertions.assertThat(result).isEqualTo(check);
    }





}
