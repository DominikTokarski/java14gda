package pl.sda.array;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class CenterTest {
    private Center center = new Center();

    @Test
    public void shouldReturnMinusOneNumberIfEmpty(){
        int result = center.getCenterNumber(new int[0]);
        assertEquals(-1,result);
    }

    @Test
    public void shouldReturnNumberIfOneElementArray(){
        int result = center.getCenterNumber(new  int[]{1});
        assertEquals(1,result);
    }

    @Test
    public void shouldReturnCenterNumberIfArrayIsEven(){
        int result = center.getCenterNumber(new int[]{1,2,3,4});
        assertEquals(2,result);
    }

    @Test
    public void shouldReturnCenterNumberIfArrayIsOdd(){
        int result = center.getCenterNumber(new int[]{5,2,1,4,3});
        assertEquals(3,result);
    }




}


