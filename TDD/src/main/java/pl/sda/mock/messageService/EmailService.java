package pl.sda.mock.messageService;

public interface EmailService {
    public boolean isAvailable();
    public void sendEmail(String message);
}
