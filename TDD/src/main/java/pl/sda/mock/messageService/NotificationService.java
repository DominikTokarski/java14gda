package pl.sda.mock.messageService;

public class NotificationService {
private EmailService emailService;
private PigeonService pigeonService;

    public NotificationService(EmailService emailService, PigeonService pigeonService) {
        this.emailService = emailService;
        this.pigeonService = pigeonService;
    }

    public void sendNotification(String message){
        message = "abc";
        if (emailService.isAvailable()){
         emailService.sendEmail(message);
     }else if (pigeonService.isAvailable()){
         pigeonService.sendMessage(message);
     }

    }

}
