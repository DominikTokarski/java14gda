package pl.sda.mock.messageService;

public interface PigeonService {
    public boolean isAvailable();
    public void sendMessage(String message);
}
