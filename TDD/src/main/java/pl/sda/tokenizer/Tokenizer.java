package pl.sda.tokenizer;

import java.util.ArrayList;


public class Tokenizer {
private ArrayList<String> list = new ArrayList();

    public ArrayList getList() {
        return list;
    }

    public ArrayList<String> getContent(String expression) {
        list = new ArrayList<String>();
        if (expression == null || expression.equals("")) {
            return list;
        }
        String tmp = expression;
        String[] letters = new String[expression.length()];
        letters = expression.split("");
        if (expression.matches("[A-Za-ząĄćĆęĘłŁńŃóÓśŚźŹżŻ]*")){
            throw new IllegalArgumentException("It's not from math");
        } else if (letters[1].equals(".")){
            list.add(tmp);
        }else for (int i = 0; i <letters.length; i++) {
            list.add(letters[i]);
        }
        return list;
    }

}
