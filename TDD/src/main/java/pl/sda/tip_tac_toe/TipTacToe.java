package pl.sda.tip_tac_toe;

public class TipTacToe {

    public String whoWins(Board board) {
       if (board.isEmpty()){
           return "Draw";
       }
       if (isThreeSignsInOneLineX(board)){
           return "player2";
       }else if (isThreeSignsInOneLineO(board)){
           return "player1";
       }
       return null;
    }

    private boolean isThreeSignsInOneLineX(Board board) {
        if (board.getElement(0,0) == 'x'
                && board.getElement(0,1) == 'x'
                && board.getElement(0,2) == 'x'){
            return true;
        }else if (board.getElement(1,0) == 'x'
                && board.getElement(1,1) == 'x'
                && board.getElement(1,2) == 'x'){
            return true;
        }else if (board.getElement(2,0) == 'x'
                && board.getElement(2,1) == 'x'
                && board.getElement(2,2) == 'x'){
            return true;
        }else if (board.getElement(0,0) == 'x'
                && board.getElement(1,0) == 'x'
                && board.getElement(2,0) == 'x'){
            return true;
        }else if (board.getElement(0,1) == 'x'
                && board.getElement(1,1) == 'x'
                && board.getElement(2,1) == 'x'){
            return true;
        }else if (board.getElement(0,2) == 'x'
                && board.getElement(1,2) == 'x'
                && board.getElement(2,2) == 'x'){
            return true;
        }else if (board.getElement(0, 0) == 'x'
                && board.getElement(1, 1) == 'x'
                && board.getElement(2, 2) == 'x') {
            return true;
        } else if (board.getElement(2, 0) == 'x'
                && board.getElement(1, 1) == 'x'
                && board.getElement(0, 2) == 'x') {
            return true;
        }else return false;
    }


        private boolean isThreeSignsInOneLineO (Board board){
            if (board.getElement(0, 0) == 'o'
                    && board.getElement(0, 1) == 'o'
                    && board.getElement(0, 2) == 'o') {
                return true;
            } else if (board.getElement(1, 0) == 'o'
                    && board.getElement(1, 1) == 'o'
                    && board.getElement(1, 2) == 'o') {
                return true;
            } else if (board.getElement(2, 0) == 'o'
                    && board.getElement(2, 1) == 'o'
                    && board.getElement(2, 2) == 'o') {
                return true;
            } else if (board.getElement(0, 0) == 'o'
                    && board.getElement(1, 0) == 'o'
                    && board.getElement(2, 0) == 'o') {
                return true;
            } else if (board.getElement(0, 1) == 'o'
                    && board.getElement(1, 1) == 'o'
                    && board.getElement(2, 1) == 'o') {
                return true;
            } else if (board.getElement(0, 2) == 'o'
                    && board.getElement(1, 2) == 'o'
                    && board.getElement(2, 2) == 'o') {
                return true;
            }else if (board.getElement(0, 0) == 'o'
                    && board.getElement(1, 1) == 'o'
                    && board.getElement(2, 2) == 'o') {
                return true;
            } else if (board.getElement(2, 0) == 'o'
                    && board.getElement(1, 1) == 'o'
                    && board.getElement(0, 2) == 'o') {
                return true;
            } else return false;
        }

}
