package pl.sda.tip_tac_toe;

import java.util.Arrays;

public class Board {
    private char[][] tipTacToe;
    private TipOrTac ox;

    public char[][] createBoard() {
        tipTacToe = new char[3][3];
        return tipTacToe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Board board = (Board) o;
        return Arrays.equals(tipTacToe, board.tipTacToe);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(tipTacToe);
    }

    public boolean isEmpty() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (tipTacToe[i][j] != 0) {
                    return false;
                }
            }

        }
        return true;
    }



    public void addMove(int x, int y,char xo) {
        int numberOfX = 0;
        int numberOfO = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (tipTacToe[i][j] == 'x'){
                    numberOfX = numberOfX + 1;
                }else if (tipTacToe[i][j] == 'o'){
                    numberOfO = numberOfO + 1;
                }
            }
        }
        if (numberOfO > numberOfX && xo == 'o'){
            throw new IllegalArgumentException("it's not your turn");
        }else if (numberOfX > numberOfO && xo == 'x'){
            throw new IllegalArgumentException("it's not your turn");
        }
        if (tipTacToe[x][y] == 0){
            tipTacToe[x][y] = xo;
        }else throw new IllegalArgumentException("space is filled");

    }

    public char getElement(int x, int y) {
        if (x > 3 || y > 3){
            throw new IllegalArgumentException("out of board");
        }
        return tipTacToe[x][y];
    }
}
