package pl.sda.array;


import java.util.Arrays;

public class Center {

    public int getCenterNumber(int[] array) {
        if (array.length == 0){
            return -1;
        }else {
            int a = (array.length  -1)/2;
            Arrays.sort(array);
            return array[a];
        }
    }
}
