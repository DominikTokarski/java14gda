package pl.sda.calculator;

import javax.print.attribute.standard.NumberUp;

public class Calculator {

    public Integer add(Integer a,Integer b){
        if (a == null || b == null){
            throw new NullPointerException("Number is null");
        }
        return a + b;
    }

    public Integer subtract(Integer a, Integer b){
        if (a == null || b == null){
            throw new NullPointerException("Number is null");
        }
        return a - b;
    }

    public Integer multiply(Integer a, Integer b){
        if (a == null || b == null){
            throw new NullPointerException("Number is null");
        }
        return a * b;
    }

    public Integer divide(Integer a,Integer b){
        if (a == null || b == null){
            throw new NullPointerException("Number is null");
        }
        if (b == 0){
            throw new IllegalArgumentException("Divisor cannot be 0.");
        }
        return a / b;
    }

    public boolean isOdd(Integer a){
        if (a % 2 == 1 || a % -2 == -1){
            return true;
        }else return false;
    }

}

