<%@ page import="com.sda.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.sda.exercise.GENDER" %><%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 24.09.2018
  Time: 20:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Formularz</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<%
    List<User> users;
    if (session.getAttribute("user_list") != null) {
        users = (List<User>) session.getAttribute("user_list");
    }else {
        users = new ArrayList<>();
    }

    String userId = request.getParameter("user_id");
    User searched = null;
    boolean female = true;
    if (userId != null) {
        Long modifyId = Long.parseLong(userId);
        Iterator<User> it = users.iterator();
        while (it.hasNext()) {
            User user = it.next();
            if (user.getId() == modifyId) {
                searched = user;
                break;
            }
        }
        if (searched != null){
         female = searched.getGender() == GENDER.FEMALE;
        }
    }



%>

<form action="<%= searched == null? "register_user.jsp" : "modify_user.jsp"%>" method="post">
    <input type="hidden" hidden value="<%=searched == null ? "" : searched.getId() %>" name="modified_id">
    <div>
        <label for="name">Name</label>
        <input id="name" name="firstName" type="text" value="<%=searched == null ? "" : searched.getName()%>">
    </div>
    <div>
        <label for="lastName">Last name</label>
        <input id="lastName" name="lastName" type="text" value="<%=searched == null ? "" : searched.getLastName()%>">
    </div>
    <div>
        <label for="userName">Username</label>
        <input id="userName" name="userName" type="text" value="<%=searched == null ? "" : searched.getUserName()%>">
    </div>
    <div>
        <label for="birthDate">Birth Date</label>
        <input id="birthDate" name="birthDate" type="datetime-local"
               value="<%=searched == null ? "" : searched.getBirthDate().toString()%>">
    </div>
    <div>

    </div>
    <div>
        <label for="address">Address</label>
        <input id="address" name="address" type="text" value="<%=searched == null ? "" : searched.getAddress()%>">
    </div>
    <div>
        <label for="height">Height</label>
        <input id="height" name="height" type="number"
               value="<%=searched == null ? "" :searched.getHeight()%>" min="60" max="230">
    </div>
    <div id="gender">
        <label for="gender">Gender</label>
        <input name="gender" type="radio" value="MALE" <%=female ? "" : "checked" %>>MALE</input>
        <input name="gender" type="radio" value="FEMALE" <%=female ? "checked" : "" %>>FEMALE</input>
    </div>
    <input type="submit" value="Wygeneruj">


</form>
</body>
</html>
