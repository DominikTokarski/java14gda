<%@ page import="com.sda.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.time.LocalDateTime" %><%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 25.09.2018
  Time: 19:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Remove User</title>
</head>
<body>
<%
List<User> users;
    if (session.getAttribute("user_list") != null) {
    users = (List<User>) session.getAttribute("user_list");
        }else {
        users = new ArrayList<>();
        }

    String removeUserId = request.getParameter("user_id");
    Long removeId = Long.parseLong(removeUserId);
    Iterator<User> it=users.iterator();
    while (it.hasNext()){
        User user =it.next();
        if (user.getId() == removeId){
            it.remove();
            break;
        }
    }

    session.setAttribute("user_list",users);

    response.sendRedirect("user_list.jsp");
    %>
</body>
</html>
