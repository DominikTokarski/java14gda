<%@ page import="java.util.List" %>
<%@ page import="com.sda.exercise.User" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<table>
    <tr>
        <td><a href="user_list.jsp">User List</a><td></td>
        <td><a href="register_form.jsp">Register Form</a><td></td>
    </tr>
</table>
<%!
   public List<User> getUserListFromSession(HttpSession session){
       List<User> userList;
       if (session.getAttribute("user_list") != null) {
           userList = (List<User>) session.getAttribute("user_list");
       }else {
           userList = new ArrayList<>();
       }
       return userList;
   }
%>
