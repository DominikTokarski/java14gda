<%@ page import="com.sda.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="com.sda.exercise.GENDER" %><%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 25.09.2018
  Time: 19:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Modify User</title>
</head>
<body>
<%
List<User> users;
    if (session.getAttribute("user_list") != null) {
    users = (List<User>) session.getAttribute("user_list");
        }else {
        users = new ArrayList<>();
        }

        String userId = request.getParameter("modified_id");
        User searched = null;
        if (userId != null) {
            Long modifyId = Long.parseLong(userId);
            Iterator<User> it = users.iterator();
            while (it.hasNext()) {
                User user = it.next();
                if (user.getId() == modifyId) {
                    searched = user;
                    it.remove();
                    break;
                }
            }


        }

    searched.setName(request.getParameter("firstName"));
    searched.setLastName(request.getParameter("lastName"));
    searched.setUserName(request.getParameter("userName"));
    LocalDateTime birthDate = LocalDateTime.parse(request.getParameter("birthDate"));
    DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
    searched.setBirthDate(birthDate);
    searched.setHeight(Integer.parseInt(request.getParameter("height")));
    GENDER gender = GENDER.valueOf(request.getParameter("gender"));
    searched.setGender(gender);
    searched.setAddress(request.getParameter("address"));

    users.add(searched);

    session.setAttribute("user_list", users);
    response.sendRedirect("user_list.jsp");
%>
</body>
</html>
