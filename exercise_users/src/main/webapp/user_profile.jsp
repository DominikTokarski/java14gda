<%@ page import="com.sda.exercise.User" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="com.sda.exercise.GENDER" %><%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 24.09.2018
  Time: 20:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Profile</title>
</head>
<body>
<%! int licznik = 0; %>
<%
        User u = new User();
        u.setId(licznik++);
        u.setName(request.getParameter("firstName"));
        u.setLastName(request.getParameter("lastName"));
        u.setUserName(request.getParameter("userName"));
        LocalDateTime birthDate = LocalDateTime.parse(request.getParameter("birthDate"));
        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        u.setBirthDate(birthDate);
        u.setJoinDate(LocalDateTime.now());
        u.setHeight(Integer.parseInt(request.getParameter("height")));
        GENDER gender = GENDER.valueOf(request.getParameter("gender"));
        u.setGender(gender);
        u.setAddress(request.getParameter("address"));

        boolean isOK = true;
    if (!Character.isUpperCase(u.getName().charAt(0))) {
        isOK=false;
    }
    if (!Character.isUpperCase(u.getLastName().charAt(0))){
        isOK=false;
    }
    if (u.getHeight() < 60 || u.getHeight() > 230){
        isOK=false;
    }
    if (u.getAddress().isEmpty()){
        isOK=false;
    }
    if (u.getUserName().contains(" ")){
        isOK=false;
    }
    if (!isOK){
        response.sendRedirect("register_form.jsp" +
                "?firstName=" + request.getParameter("firstName") +
                "&lastName=" + request.getParameter("lastName") +
                "&height=" + request.getParameter("height"));
    }
%>
    <div >
        <table width="100%"  align="center" border="1">
            <thead>
            <th>Nazwa parametru</th>
            <th>Wartość</th>
            </thead>
            <tr>
                <td>Id:</td>
                <td><%= u.getId() %>
                </td>
            </tr>
            <tr>
                <td>Username:</td>
                <td><%= u.getUserName()%>
                </td>
            </tr>
            <tr>
                <td>First name:</td>
                <td><%= u.getName()%>
                </td>
            </tr>
            <tr>
                <td>Last name:</td>
                <td><%= u.getLastName()%>
                </td>
            </tr>
            <tr>
                <td>Gender:</td>
                <td><%= u.getGender()%>
                </td>
            </tr>
            <tr>
                <td>Height</td>
                <td><%= u.getHeight()%>
                </td>
            </tr>
            <tr>
                <td>Join date:</td>
                <td><%= u.getJoinDate()%>
                </td>
            </tr>
            <tr>
                <td>Birth date:</td>
                <td><%= u.getBirthDate()%>
                </td>
            </tr>
            <tr>
                <td>Address:</td>
                <td><%= u.getAddress()%>
                </td>
            </tr>
        </table>
    </div>


</body>
</html>
