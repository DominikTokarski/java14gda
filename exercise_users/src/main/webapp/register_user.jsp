<%@ page import="com.sda.exercise.User" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.sda.exercise.GENDER" %><%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 25.09.2018
  Time: 18:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Action Register User</title>
</head>
<body>

<%! int licznik = 0; %>
<%

    User u = new User();
    u.setId(licznik++);
    u.setName(request.getParameter("firstName"));
    u.setLastName(request.getParameter("lastName"));
    u.setUserName(request.getParameter("userName"));
    LocalDateTime birthDate = LocalDateTime.parse(request.getParameter("birthDate"));
    DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
    u.setBirthDate(birthDate);
    u.setJoinDate(LocalDateTime.now());
    u.setHeight(Integer.parseInt(request.getParameter("height")));
    GENDER gender = GENDER.valueOf(request.getParameter("gender"));
    u.setGender(gender);
    u.setAddress(request.getParameter("address"));

    List<User> users;
    if (session.getAttribute("user_list") != null) {
        users = (List<User>) session.getAttribute("user_list");
    }else {
        users = new ArrayList<>();
    }
    users.add(u);

    session.setAttribute("user_list", users);
    response.sendRedirect("user_list.jsp");

%>

</body>
</html>
