package pl.sda.password_menager;

import pl.sda.password_menager.commons.PasswordEntry;
import pl.sda.password_menager.fileReader.FileReaderFacade;
import pl.sda.password_menager.fileWriter.SaveFileFacade;
import pl.sda.password_menager.fileWriter.SaveFileType;
import pl.sda.password_menager.generators.GeneratorType;
import pl.sda.password_menager.generators.PasswordGeneratorFacade;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Application {
   private static ApplicationState state = ApplicationState.APPLICATION;
   private static Scanner scanner = new Scanner(System.in);
   private static List<PasswordEntry> entries;
   private static PasswordGeneratorFacade generator = new PasswordGeneratorFacade();
   private static File file = new File(
           "C:/Users/Dominik/Desktop/Java14GDA/PasswordManager/Passwords");
   private static String path = "C:/Users/Dominik/Desktop/Java14GDA/PasswordManager/Passwords";
   private static FileReaderFacade reader = new FileReaderFacade();
   private static SaveFileFacade saver = new SaveFileFacade();
   private static String name;




    private static void setState(ApplicationState state) {
        Application.state = state;
    }

    public static void main(String[] args) throws IOException {
        String scan = "";
        while (!scan.equals("quit")) {
            System.out.println("Welcome to your passwords generator");
            System.out.println("     What you want to do .?");
            System.out.println("1 -> generate password");
            System.out.println("2 -> read file with password");
            System.out.println("3 -> add entry");
            System.out.println("4 -> remove entry");
            System.out.println("5 -> add entries to file");
            System.out.println("6 -> change path to file");
            System.out.println(" Write 'quit' to exit application");
            scan = scanner.nextLine();
            if (scan.equals("1")) {
                if (entries == null) {
                    System.out.println("Add some entries");
                } else {
                    System.out.println("For which entry you want generate password");
                    int index = 1;
                    for (PasswordEntry entry : entries) {
                        System.out.println(index + ". " + entry.getWebsite());
                        index++;
                    }
                    index = scanner.nextInt() - 1;
                    System.out.println("Which method you want to use .?");
                    System.out.println("1 -> UpperCase,LowerCase,Numbers,Special Sign");
                    System.out.println("2 -> UpperCase,LowerCase,Numbers");
                    System.out.println("3 -> UpperCase,LowerCase,Special Sign");
                    scanner.nextLine();
                    scan = scanner.nextLine();
                    System.out.println("How long password do you want .?");
                    int length = scanner.nextInt();
                    String password;
                    if (scan.equals("1")) {
                        password = generator.getPassword(length, GeneratorType.SECOND);
                        entries.get(index).setPassword(password);
                    } else if (scan.equals("2")) {
                        password = generator.getPassword(length, GeneratorType.FIRST);
                        entries.get(index).setPassword(password);
                    } else if (scan.equals("3")) {
                        password = generator.getPassword(length, GeneratorType.THIRD);
                        entries.get(index).setPassword(password);
                    } else {
                        System.out.println("There is no method");
                    }
                }
                scanner.nextLine();
            } else if (scan.equals("2")) {
                System.out.println("Which file you want to read .?");
                for (File files : Objects.requireNonNull(file.listFiles())) {
                    System.out.println(files.getName());
                }
                name = scanner.nextLine();
                System.out.println("Which method you want use .?");
                System.out.println("1 -> BufferedReader");
                System.out.println("2 -> OpenCSV");
                scan = scanner.nextLine();
                List<String> list = reader.readFile(scan, path + name);
                for (String string : list) {
                    System.out.println(string);
                }
            } else if (scan.equals("3")) {
                System.out.println("Set new entry website");
                scan = scanner.nextLine();
                PasswordEntry passwordEntry = new PasswordEntry();
                passwordEntry.setWebsite(scan);
                if (entries == null){
                    entries = new ArrayList<>();
                }
                entries.add(passwordEntry);
            } else if (scan.equals("4")) {
                System.out.println("Which entry you want remove .?");
                int index = 1;
                for (PasswordEntry entry : entries) {
                    System.out.println(index + ". " + entry.getWebsite());
                }
                index = scanner.nextInt();
                entries.remove(index - 1);
            } else if (scan.equals("5")) {
                if (entries != null) {
                    System.out.println("Name the file");
                    name = scanner.nextLine();
                    System.out.println("Choose the method");
                    System.out.println("1 -> CSV");
                    System.out.println("2 -> Print Writer");
                    scan = scanner.nextLine();
                    if (scan.equals("1")) {
                        saver.saveTofile(path + name, entries, SaveFileType.CSV);
                        System.out.println("Saving done");
                    } else if (scan.equals("2")) {
                        saver.saveTofile(path + name, entries, SaveFileType.PRINT_WRITER);
                        System.out.println("Saving done");
                    }
                } else {
                    System.out.println("There is nothing to save");
                }
            } else if (scan.equals("quit")) {
                System.out.println("Goodbye");
            } else if (scan.equals("6")) {
                path = scanner.nextLine();
            } else {
                System.out.println("Bad command");
            }
        }
    }
}
