package pl.sda.password_menager.fileReader;

import pl.sda.password_menager.commons.PasswordEntry;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractFileReader implements FileReader {
    @Override
    public abstract List<String> read(String path) throws IOException;

    @Override
    public List<PasswordEntry> getPasswordEntries(String path) throws IOException {
        List<PasswordEntry> result = new ArrayList<>();
        List<String> lines = read(path);

        for (String line : lines) {
            String[] spited = line.split(";");
            PasswordEntry passwordEntry = new PasswordEntry(spited[0], spited[1]);
            result.add(passwordEntry);
        }
        return result;
    }

}
