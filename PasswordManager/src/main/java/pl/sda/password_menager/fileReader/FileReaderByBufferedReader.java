package pl.sda.password_menager.fileReader;

import java.io.*;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class FileReaderByBufferedReader extends AbstractFileReader {

    @Override
    public List<String> read(String path) throws IOException {
        File file = new File(path);
        BufferedReader reader = new BufferedReader(new FileReader(path));
        List<String> passwords = new ArrayList<>();
        String line;
        while ((line  = reader.readLine()) != null){
            passwords.add(line);
        }
        return passwords;
    }
}
