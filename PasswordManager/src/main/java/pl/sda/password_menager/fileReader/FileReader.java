package pl.sda.password_menager.fileReader;

import pl.sda.password_menager.commons.PasswordEntry;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface FileReader {
    List<String> read(String path) throws IOException;
    List<PasswordEntry> getPasswordEntries(String path) throws IOException;
}
