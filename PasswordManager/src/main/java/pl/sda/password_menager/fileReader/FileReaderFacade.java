package pl.sda.password_menager.fileReader;

import pl.sda.password_menager.encryption.EncryptionService;

import java.io.IOException;
import java.util.List;

public class FileReaderFacade {
    private AbstractFileReader reader = new FileReaderByBufferedReader();
    private AbstractFileReader reader1 = new FileReaderByCSV();
    private String key = "abcdefghijklmnop";

    public List<String> readFile(String which,String path) throws IOException {
        List<String> readen = null;
        int index = 0;
        if (which.equals("1")){
            readen = reader.read(path);
            for (String read: readen){
                String decrypted = EncryptionService.decrypt(key,read);
                readen.set(index,decrypted);
                index++;
            }
        }else if (which.equals("2")){
            readen = reader1.read(path);
            for (String read: readen){
                String decrypted = EncryptionService.decrypt(key,read);
                readen.set(index,decrypted);
                index++;
            }
        }
        return readen;
    }
}
