package pl.sda.password_menager.fileReader;


import com.opencsv.CSVReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileReaderByCSV extends AbstractFileReader {

    @Override
    public List<String> read(String path) {
        List<String> passwords = new ArrayList<>();
        try {
            CSVReader reader = new CSVReader(new java.io.FileReader(path));
            String[] line;
            while ((line = reader.readNext()) != null) {
                passwords.add(Arrays.toString(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return passwords;
    }


}
