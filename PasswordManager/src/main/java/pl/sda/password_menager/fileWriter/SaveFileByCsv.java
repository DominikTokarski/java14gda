package pl.sda.password_menager.fileWriter;

import com.opencsv.CSVWriter;
import pl.sda.password_menager.commons.PasswordEntry;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;


public class SaveFileByCsv implements ToFile{
    public void toFile(String name, Collection<PasswordEntry> passwords) {
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(name + ".txt"), ';',CSVWriter.NO_QUOTE_CHARACTER);
            for (PasswordEntry password : passwords) {
                writer.writeNext(password.toStringArray());
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
