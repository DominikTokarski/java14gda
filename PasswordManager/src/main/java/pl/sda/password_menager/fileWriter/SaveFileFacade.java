package pl.sda.password_menager.fileWriter;

import pl.sda.password_menager.commons.PasswordEntry;
import pl.sda.password_menager.encryption.EncryptionService;

import java.util.Collection;

public class SaveFileFacade {
    private SaveFileFactory factory;
    private String key = "abcdefghijklmnop";

    public SaveFileFacade() {
        this.factory = new SaveFileFactory();
    }



    public void saveTofile(String name, Collection<PasswordEntry> passwords,SaveFileType type){
        for (PasswordEntry entry : passwords){
            String encrypted = EncryptionService.encrypt(key,entry.getPassword());
            entry.setPassword(encrypted);
        }
        factory.saveType(type).toFile(name,passwords);
    }

}
