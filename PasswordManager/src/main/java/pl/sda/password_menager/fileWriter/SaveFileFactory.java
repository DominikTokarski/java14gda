package pl.sda.password_menager.fileWriter;



public class SaveFileFactory {
    ToFile saveType(SaveFileType type){
        if (type == SaveFileType.CSV){
            return new SaveFileByCsv();
        }else if (type == SaveFileType.PRINT_WRITER){
            return new SaveFileByPrintWriter();
        }else {
            return new SaveFileException("There is no such method");
        }
    }
}
