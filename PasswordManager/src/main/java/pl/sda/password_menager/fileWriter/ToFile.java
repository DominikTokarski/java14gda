package pl.sda.password_menager.fileWriter;

import pl.sda.password_menager.commons.PasswordEntry;

import java.util.Collection;

public interface ToFile {
   void toFile(String name,Collection<PasswordEntry> passwords);
}
