package pl.sda.password_menager.fileWriter;

import pl.sda.password_menager.commons.PasswordEntry;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

public class SaveFileByPrintWriter implements ToFile{
    public void toFile(String name, Collection<PasswordEntry> password) {
        try {
            PrintWriter writer = new PrintWriter(new FileWriter(name + ".txt"));
            for (PasswordEntry entry : password) {
                for (int i = 0; i < entry.toStringArray().length; i++) {
                    writer.write(entry.toStringArray()[i]);

                }
                writer.flush();
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
