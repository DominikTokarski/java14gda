package pl.sda.password_menager.generators;

public class FirstPasswordGenerator implements PasswordGenerator{

    public String generate(int length) {
        String password = "";
        for (int i = 0; i < length; i++) {
            int which = random.nextInt(3);
            int index;
            if (which == 0){
                index = random.nextInt(lowerChars.length);
                password += lowerChars[index];
            }else if (which == 1){
                index = random.nextInt(upperChars.length);
                password += upperChars[index];
            }else if (which == 2){
                index = random.nextInt(numbers.length);
                password += numbers[index];
            }
        }
        return password;

    }
}
