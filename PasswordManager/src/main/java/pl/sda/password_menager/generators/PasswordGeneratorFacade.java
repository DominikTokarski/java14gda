package pl.sda.password_menager.generators;

public class PasswordGeneratorFacade {

    private PasswordGeneratorFactory factory;

    public PasswordGeneratorFacade() {
        this.factory = new PasswordGeneratorFactory();
    }

    public String getPassword(int length, GeneratorType type){
        return factory.getGenerator(type).generate(length);
    }
}
