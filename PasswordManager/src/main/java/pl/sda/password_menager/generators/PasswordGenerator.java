package pl.sda.password_menager.generators;

import java.util.Random;

public interface PasswordGenerator {
    Random random = new Random();
    String[] upperChars = new String[]
            {"A","B","C","D","E","F","G","H","I","J","K",
                    "L","M","N","O","P","R","S","T","U","W","X","Y","Z"};
    String[] lowerChars = new String[]{"a","b","c","d","e","f","g","h","i","j","k",
            "l","m","n","o","p","r","s","t","u","w","x","y","z"};
    String[] numbers = new String[]{"0","1","2","3","4","5","6","7","8","9"};
    String[] specialChars = new String[]{".","!","?","@","#","$","%","^","&","*","(",
    ")","_","+",":","<",">","/","{","[","}","]","|"};


    String generate(int length);
}
