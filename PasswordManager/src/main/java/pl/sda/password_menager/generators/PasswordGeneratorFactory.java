package pl.sda.password_menager.generators;

import static pl.sda.password_menager.generators.GeneratorType.FIRST;
import static pl.sda.password_menager.generators.GeneratorType.SECOND;
import static pl.sda.password_menager.generators.GeneratorType.THIRD;

public class PasswordGeneratorFactory {

    PasswordGenerator getGenerator(GeneratorType type){
        if (type == FIRST){
            return new FirstPasswordGenerator();
        }else if (type == SECOND){
            return new SecondPasswordGenerator();
        }else if (type == THIRD){
            return new ThirdPasswordGenerator();
        }
        return new PasswordGeneratorException("Bad generator type");
    }
}
