package pl.sda.password_menager;

public enum ApplicationState {
    APPLICATION,
    GENERATE_PASS,
    READ_PASS,
    REMOVE_ENTRY;
}
