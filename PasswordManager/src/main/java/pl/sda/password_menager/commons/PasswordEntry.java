package pl.sda.password_menager.commons;

public class PasswordEntry {

    private String website;
    private String password;

    public PasswordEntry() {
    }

    public PasswordEntry(String website, String password) {
        this.website = website;
        this.password = password;
    }

    public String[] toStringArray() {
        return new String[] {website, password};
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "PasswordEntry{" +
                "website='" + website + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
