package pl.sda.password_menager.encryption;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EncryptionServiceTest {


    @Test
    void shouldBeSame(){
        String encryptionKey = "abcdefghijklmnop";
        String toEncrypt = "Facebook";

       String encrypt = EncryptionService.encrypt(encryptionKey,toEncrypt);
       String decrypt = EncryptionService.decrypt(encryptionKey,encrypt);

        Assertions.assertEquals(decrypt,toEncrypt);
    }
}
