<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 09.10.2018
  Time: 18:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Event List</title>
</head>
<body>
<%@include file="/header.jsp"%>
<h2>User List:</h2>
<table>
    <thead>
    <th>Id</th>
    <th>Name</th>
    <th>Description</th>
    <th>Time</th>
    <th>Length</th>
    <th>Action Remove</th>
    <th>Action modify</th>
    </thead>
    <tbody>
    <c:forEach var="user" items="${eventList}">
        <tr>
            <td>
                <c:out value="${event.id}"/>
            </td>
            <td>
                <c:out value="${event.name}"/>
            </td>
            <td>
                <c:out value="${event.description}"/>
            </td>
            <td>
                <c:out value="${event.time}"/>
            </td>
            <td>
                <c:out value="${event.length}"/>
            </td>
            <td>
                <a href="/event/remove?id=<c:out value="${user.id}"/>">Remove</a>
            </td>
            <td>
                <a href="/event/modify?id=<c:out value="${user.id}"/>">Modify</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</body>
</html>
