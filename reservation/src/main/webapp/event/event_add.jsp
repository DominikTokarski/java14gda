<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 08.10.2018
  Time: 18:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Event add</title>
</head>
<body>
<%@include file="/header.jsp"%>

<h2>Event Form:</h2>

<span style="color: red; ">
<c:out value="${error_messege}"/>
</span>
<form action="/event/add" method="post">
    <input name="owner_id" type="hidden" value="<c:out value="${sessionScope.logged_id}"/>">
    <div>
        <label for="name">Name:</label>
        <input id="name" name="name" type="text">
    </div>
    <div>
        <label for="description">Description:</label>
        <input id="description" name="description" type="text">
    </div>
    <div>
        <label for="time">Time:</label>
        <input id="time" name="time" type="datetime-local">
    </div>
    <div>
        <label for="length">Length:</label>
        <input id="length" name="length" type="number">
    </div>
    <input type="submit" value="add">
</form>
</body>
</html>
