<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 03.10.2018
  Time: 20:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Form</title>
</head>
<body>
<%@include file="/header.jsp"%>

<h2>User Form:</h2>

<span style="color: red; ">
<c:out value="${error_message}"/>
</span>

    <form action="/user/register" method="post">
    <div>
    <label for="email">Email :</label>
    <input id="email" name="email" type="email">
    </div>
    <div>
            <label for="username">Username:</label>
            <input id="username" name="username" type="text">
        </div>
        <div>
            <label for="password">Password:</label>
            <input id="password" name="password" type="password">
        </div>
        <div>
            <label for="password-confirm">Password confirm:</label>
            <input id="password-confirm" name="password-confirm" type="password">
        </div>
        <input type="submit" value="Register">
    </form>
    </body>
</html>
