<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 08.10.2018
  Time: 19:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<%@include file="/header.jsp"%>
<h2>Login Form:</h2>

<span style="color: red; ">
<c:out value="${error_message}"/>
</span>

<form action="/login" method="post">
    <div>
        <label for="username">Username:</label>
        <input id="username" name="username" type="text">
    </div>
    <div>
        <label for="password">Password:</label>
        <input id="password" name="password" type="password">
    </div>
    <input type="submit" value="Login">
</form>
</body>
</html>
