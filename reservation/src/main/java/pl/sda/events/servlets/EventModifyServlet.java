package pl.sda.events.servlets;

import pl.sda.events.model.Event;
import pl.sda.events.service.EventService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/event/modify")
public class EventModifyServlet extends HttpServlet {

    @EJB
    private EventService eventService;

//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        long id = Integer.parseInt(req.getParameter("id"));
//
//        Optional<Event> event = eventService.getEventWithId(id);
//        if (event.isPresent()) {
//            req.setAttribute("event_to_modify", event.get());
//
//            req.getRequestDispatcher("/event/event_form.jsp").forward(req, resp);
//        }else {
//            resp.sendRedirect("/event/list");
//        }
//    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String description = req.getParameter("description");
        String length = req.getParameter("length");
        String time = req.getParameter("time");
        String owner_id = req.getParameter("owner_id");

        if (name.isEmpty() || description.isEmpty() || length.isEmpty() || time.isEmpty()){
            req.setAttribute("error_message","None can be empty!");

            req.getRequestDispatcher("/event/event_form.jsp").forward(req,resp);
            return;
        }

//        int identifier = Long.parseLong(id);
//        Optional<AppUser> user = userService.getUserWithId(identifier);
//        if (user.isPresent()){
//            AppUser userToModify = user.get();
//
//            userToModify.setLogin(username);
//            userToModify.setPassword(password);
//
//            userService.modify(userToModify);
//        }
//
//
//        resp.sendRedirect("/user/list");
    }
}
