package pl.sda.events.servlets;

import pl.sda.events.model.Event;
import pl.sda.events.service.EventService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/event/remove")
public class EventRemoveServlet extends HttpServlet {

    @EJB
    private EventService eventService;

//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        if (req.getParameter("id")!= null){
//            long id = Long.parseLong(req.getParameter("id"));
//
//            eventService.removeEventWithId(id);
//        }
//
//        resp.sendRedirect("/event/list");
//    }
}
