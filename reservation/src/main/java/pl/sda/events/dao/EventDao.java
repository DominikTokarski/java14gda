package pl.sda.events.dao;

import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import pl.sda.events.model.Event;
import pl.sda.events.util.HibernateUtil;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@LocalBean
@Singleton
public class EventDao {

    public void save(Event event){
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()){
            transaction = session.beginTransaction();
            session.save(event);

            transaction.commit();
        }catch (SessionException sessionException){
            transaction.rollback();
            System.out.printf("Error");
        }
    }

//    public List<Event> getAllEvents() {
//        List<Event> list;
//        Transaction transaction = null;
//        try (Session session = HibernateUtil.getSessionFactory().openSession()){
//            transaction = session.beginTransaction();
//            list = session.createQuery("from Event", Event.class).list();
//
//            return list;
//        }catch (SessionException sessionException){
//            transaction.rollback();
//            System.out.printf("Error");
//        }
//        return new ArrayList<>();
//    }

//    public void removeEventWithId(long id) {
//        Optional<Event> eventOpt = findEventWith(id);
//        if (eventOpt.isPresent()){
//            Event event = eventOpt.get();
//
//            Transaction transaction = null;
//            try (Session session = HibernateUtil.getSessionFactory().openSession()){
//                transaction = session.beginTransaction();
//                session.delete(event);
//
//                transaction.commit();
//            }catch (SessionException sessionException){
//                transaction.rollback();
//                System.out.printf("Error");
//            }
//        }
//    }
//
//    public Optional<Event> findEventWith(long id) {
//        Transaction transaction = null;
//        try (Session session = HibernateUtil.getSessionFactory().openSession()){
//            transaction = session.beginTransaction();
//            Query<Event> query = session.createQuery("from Event where id= :id");
//            query.setParameter("id",id);
//
//            Event result = query.uniqueResult();
//
//            return Optional.of(result);
//        }catch (SessionException sessionException){
//            transaction.rollback();
//            System.out.printf("Error");
//        }
//        return Optional.empty();
//    }

    public void findEventWithId(long id) {
    }

    public List<Event> getAllEvents(){
        try (Session session = HibernateUtil.getSessionFactory().openSession()){
           List<Event> events = session.createQuery("from Event",Event.class).list();

           return events;
        }catch (SessionException sessionException){
            System.out.printf("Error");
        }
        return new ArrayList<>();
    }
}

