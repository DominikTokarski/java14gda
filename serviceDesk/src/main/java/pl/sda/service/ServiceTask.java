package pl.sda.service;



public class ServiceTask {
    private PriorityTask priority;
    private String name;
    private String description;

    public ServiceTask() {
    }

    @Override
    public String toString() {
        return "ServiceTask{" +
                "priority=" + priority +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public PriorityTask getPriority() {
        return priority;
    }

    public void setPriority(PriorityTask priority) {
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
