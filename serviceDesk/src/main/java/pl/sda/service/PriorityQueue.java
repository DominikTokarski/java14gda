package pl.sda.service;

import java.util.Arrays;

public class PriorityQueue {
    private int size = 150000;
    private ServiceTask[] heapArray;
    private int nextIndex = 0;

    public PriorityQueue() {
        heapArray = new ServiceTask[size];
        Arrays.fill(heapArray,null);
    }

    public void put(ServiceTask task){
        heapArray[nextIndex] = task;
        heapifyUp(nextIndex);
        nextIndex++;

    }

    private void heapifyUp(int index) {
        if (heapArray[index].getPriority().value < heapArray[getParent(index)].getPriority().value
                || index == 0){
            return;
        }
        ServiceTask temp = heapArray[index];
        heapArray[index] = heapArray[getParent(index)];
        heapArray[getParent(index)] = temp;
        heapifyUp(getParent(index));

    }
    private int getParent(int n){
        return (n - 1) / 2;
    }

    public ServiceTask extractMax(){
        if (nextIndex == 0){
            return null;
        }else if (nextIndex == 1) {
            ServiceTask max = heapArray[0];
            heapArray[0] = null;
            return max;
        }
        ServiceTask max = heapArray[0];
        heapArray[0] = heapArray[nextIndex - 1];
        heapArray[nextIndex -1] = null;
        nextIndex--;
        heapifyDown(0);
        return max;
    }

    private void heapifyDown(int i) {
        int rightIndex = getRightChild(i);
        int leftIndex = getLeftChild(i);
        int strongerChildIndex;
        if (heapArray[rightIndex] != null && heapArray[leftIndex] != null){
            if (heapArray[rightIndex].getPriority().getValue() > heapArray[leftIndex].getPriority().getValue()){
                strongerChildIndex = rightIndex;
            }else {
                strongerChildIndex = leftIndex;
            }
        }else if (heapArray[rightIndex] != null && heapArray[leftIndex] == null){
            strongerChildIndex = rightIndex;
        }else if (heapArray[rightIndex] == null && heapArray[leftIndex] != null){
            strongerChildIndex = leftIndex;
        }else return;

        if (heapArray[i].getPriority().getValue() < heapArray[strongerChildIndex].getPriority().getValue()){
            ServiceTask temp = heapArray[i];
            heapArray[i] = heapArray[strongerChildIndex];
            heapArray[strongerChildIndex] = temp;
            heapifyDown(strongerChildIndex);
        }
    }

    private int getLeftChild(int n){
        return 2 * n + 1;
    }
    private int getRightChild(int n){
        return 2 * n + 2;
    }

    public void sort(){
        ServiceTask[] sorted = new ServiceTask[size];
        Arrays.fill(sorted, null);
        for (int i = 0; i < sorted.length; i++) {
            if (heapArray[0] != null) {
                sorted[i] = extractMax();
            }else {
                break;
            }
        }
        heapArray = sorted;


    }

    public void remove(ServiceTask value) {
        for (int i = 0; i < nextIndex; i++) {
            if (value == heapArray[i]) {
                heapArray[i] = heapArray[nextIndex - 1];
                heapArray[nextIndex - 1] = null;
                nextIndex--;
                heapifyDown(i);
                heapifyUp(i);
            }
        }
    }
}
