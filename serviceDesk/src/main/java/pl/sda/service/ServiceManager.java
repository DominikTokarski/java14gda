package pl.sda.service;

import java.util.Scanner;

public class ServiceManager{
    private static PriorityQueue queue = new PriorityQueue();

    public static void createTask(ServiceTask task){
        Scanner scanner = new Scanner(System.in);
        System.out.println("---Create Task---");
        System.out.println("Choose priority:");
        System.out.println("-Critical");
        System.out.println("-High");
        System.out.println("-Average");
        System.out.println("-Low");
        System.out.println("-VeryLow");
        String priority = scanner.nextLine().toUpperCase();
        task.setPriority(PriorityTask.valueOf(priority));
        System.out.println("Give name");
        String name = scanner.nextLine();
        task.setName(name);
        System.out.println("Give a description");
        String description = scanner.nextLine();
        task.setDescription(description);
        queue.put(task);
    }

    public static ServiceTask takeTask(){
        System.out.println("---Take Task---");
        return queue.extractMax();

    }
}
