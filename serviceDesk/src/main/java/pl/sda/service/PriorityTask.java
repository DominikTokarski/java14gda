package pl.sda.service;

public enum PriorityTask {
    CRITICAL(5),
    HIGH(4),
    AVERAGE(3),
    LOW(2),
    VERYLOW(1);
    int value;

    public int getValue() {
        return value;
    }

    PriorityTask(int i) {

    }

}
