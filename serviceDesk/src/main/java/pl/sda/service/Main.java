package pl.sda.service;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ServiceManager manager = new ServiceManager();
        Scanner scanner = new Scanner(System.in);
        String user;
        do {
            ServiceTask task = new ServiceTask();
            System.out.println("---Menu---");
            System.out.println("Create task");
            System.out.println("Take task");
            System.out.println("Exit");
            user = scanner.nextLine().toLowerCase();
            if (user.equals("create task")){
                manager.createTask(task);
            }else if (user.equals("take task")){
                System.out.println(manager.takeTask());
            }else if (user.equals("exit")){
                System.out.println("Good Bye");
            }else {
                System.out.println("Bad message");
            }
        } while (!user.equals("exit"));

    }

}
