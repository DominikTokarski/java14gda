package pl.sda.registry.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.registry.model.dto.DeleteAppUserDto;
import pl.sda.registry.model.dto.NameAndSurnameFilterDto;
import pl.sda.registry.model.dto.UpdateAppUserDto;
import pl.sda.registry.model.AppUser;
import pl.sda.registry.model.dto.CreateUserDto;
import pl.sda.registry.repository.AppUserRepository;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    public Optional<AppUser> addUser(CreateUserDto user) {
        if (user.getPassword().equals(user.getPasswordConfirm())) {
            AppUser createdUser = AppUser.createFromDto(user);
            createdUser = appUserRepository.save(createdUser);
            return Optional.of(createdUser);
        }
        return Optional.empty();
    }

    public Optional<AppUser> getUserById(Long id) {
        return appUserRepository.findById(id);
    }

    public Optional<AppUser> updateName(Long id, String name) {
        Optional<AppUser> user = appUserRepository.findById(id);
        if (user.isPresent()) {
            AppUser appUser = user.get();
            appUser.setName(name);
            appUser = appUserRepository.save(appUser);
            user = Optional.of(appUser);
            return user;
        }
        return Optional.empty();
    }


    public Optional<AppUser> updateSurname(Long id, String surname) {
        Optional<AppUser> user = appUserRepository.findById(id);
        if (user.isPresent()) {
            AppUser appUser = user.get();
            appUser.setSurname(surname);
            appUser = appUserRepository.save(appUser);
            user = Optional.of(appUser);
            return user;
        }
        return Optional.empty();
    }

    public List<AppUser> getAll() {
        return appUserRepository.findAll();
    }

    public List<AppUser> getUserBySurname(String surname) {
        return appUserRepository.findAllBySurname(surname);
    }

    public Optional<AppUser> update(UpdateAppUserDto updateAppUserDto) {
        Optional<AppUser> appUserOptional = appUserRepository.findById(updateAppUserDto.getUpdatedUserId());
        if (appUserOptional.isPresent()){
            AppUser appUser = appUserOptional.get();

            if (updateAppUserDto.getName() != null) {
                appUser.setName(updateAppUserDto.getName());
            }
            if (updateAppUserDto.getSurname() != null){
                appUser.setSurname(updateAppUserDto.getSurname());
            }
            if (updateAppUserDto.getPassword() != null){
                appUser.setPassword(updateAppUserDto.getPassword());
            }

            appUser = appUserRepository.save(appUser);

            return Optional.of(appUser);
        }

        return Optional.empty();
    }

    public boolean deleteUser(Long id) {
        if (appUserRepository.existsById(id)) {
            appUserRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public int deleteAppUser(DeleteAppUserDto deleteAppUserDto) {
        return appUserRepository.deleteByNameAndSurname(deleteAppUserDto.getName(),deleteAppUserDto.getSurname());
    }


    public List<AppUser> getAllUsersByNameAndSurname(NameAndSurnameFilterDto dto) {
        return appUserRepository.findAllByNameContainingIgnoreCaseAndSurnameContainingIgnoreCase(dto.getName()
                ,dto.getSurname());
    }
}
