package pl.sda.registry.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.registry.model.Grade;
import pl.sda.registry.model.Subject;
import pl.sda.registry.model.dto.AddGradeDto;
import pl.sda.registry.repository.GradeRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class GradeService {

    @Autowired
    private GradeRepository gradeRepository;

    public Optional<Grade> addGrade(AddGradeDto dto) {
        Grade createdGrade = Grade.createFromGradeDto(dto);
        createdGrade = gradeRepository.save(createdGrade);
        return Optional.of(createdGrade);

    }

    public AddGradeDto setParams(double grade, Subject subject){
        AddGradeDto dto = new AddGradeDto();
        dto.setGrade(grade);
        dto.setSubject(subject);
        return dto;
    }

    public List<Grade> getAll() {
        return gradeRepository.findAll();
    }

    public List<Grade> getAllWithGradeAbove3() {
        return gradeRepository.findAllByGradeGreaterThan(3);
    }
}
