package pl.sda.registry.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.registry.model.AppUser;
import pl.sda.registry.model.Grade;
import pl.sda.registry.model.Subject;
import pl.sda.registry.model.dto.AddGradeToUserDto;
import pl.sda.registry.model.dto.GetUserGradesBySubjectDTO;
import pl.sda.registry.model.dto.SubjectGradesDto;
import pl.sda.registry.model.dto.UserStatsDto;
import pl.sda.registry.repository.AppUserRepository;
import pl.sda.registry.repository.GradeRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class RegistryService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private GradeRepository gradeRepository;


    public Optional<AppUser> addGrade(AddGradeToUserDto dto) {
        return addGrade(dto.getUserId(),dto.getDto().getSubject(),dto.getDto().getGrade());
    }

    public Optional<AppUser> addGrade(Long studentId, Subject subject, double gradex) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(studentId);
        if (optionalAppUser.isPresent()){
            AppUser appUser = optionalAppUser.get();

            Grade grade = new Grade();
            grade.setGrade(gradex);
            grade.setSubject(subject);

            gradeRepository.save(grade);

            appUser.getGradeList().add(grade);

            appUserRepository.save(appUser);

            return Optional.of(appUser);
        }
        return  Optional.empty();
    }

    public List<Grade> getGradesBySubject(GetUserGradesBySubjectDTO dto) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(dto.getUserId());
        if (optionalAppUser.isPresent()){
            List<Grade> grades = optionalAppUser.get().getGradeList();

            return grades.stream().filter(grade -> grade.getSubject() == dto.getSubject()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public List<AppUser> getWeakStudents(double mean) {
        List<AppUser> appUsers = appUserRepository.findAll();
        return appUsers.stream().filter(appUser -> {
            return appUser.getGradeList()
                    .stream()
                    .map(Grade::getGrade)
                    .mapToDouble(p -> p).average()
                    .getAsDouble() < mean;
        }).collect(Collectors.toList());
    }

    public Optional<UserStatsDto> getStats(Long userId) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(userId);
        if (optionalAppUser.isPresent()){
            AppUser user = optionalAppUser.get();
            UserStatsDto userStatsDto = new UserStatsDto();
            userStatsDto.setName(user.getName());
            userStatsDto.setSurname(user.getSurname());
            userStatsDto.setMean(user.getGradeList().stream()
                    .map(Grade::getGrade)
                    .mapToDouble(p -> p).average().orElse(0.0));

            Set<Subject> subjects = user.getGradeList()
                    .stream()
                    .map(Grade::getSubject)
                    .collect(Collectors.toSet());

            List<SubjectGradesDto> subjectGrades = subjects.stream().map(subject -> {
                SubjectGradesDto subjectGradesDto = new SubjectGradesDto();
                subjectGradesDto.setSubject(subject);


                subjectGradesDto.setGrades(user.getGradeList()
                        .stream()
                        .filter(grade -> grade.getSubject() == subject)
                        .map(Grade::getGrade)
                        .collect(Collectors.toList()));

                subjectGradesDto.setMean(
                        subjectGradesDto.getGrades()
                                .stream()
                                .mapToDouble(grade -> grade)
                                .average()
                                .orElse(0.0));
                return subjectGradesDto;
            }).collect(Collectors.toList());

            userStatsDto.setGrades(subjectGrades);

            return Optional.of(userStatsDto);
        }
        return Optional.empty();

    }
}
