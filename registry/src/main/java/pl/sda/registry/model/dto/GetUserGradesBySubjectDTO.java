package pl.sda.registry.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.registry.model.Subject;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GetUserGradesBySubjectDTO {
    @NotNull
    private Long userId;
    @NotNull
    private Subject subject;
}
