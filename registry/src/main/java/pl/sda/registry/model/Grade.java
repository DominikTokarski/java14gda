package pl.sda.registry.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import pl.sda.registry.model.dto.AddGradeDto;



import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
public class Grade {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    private double grade;
    private Subject subject;
    private LocalDateTime dateAdded;


    public Grade() {
        dateAdded = LocalDateTime.now();
    }

    public Grade(Long id, double grade, Subject subject, LocalDateTime dateAdded) {
        this.id = id;
        this.grade = grade;
        this.subject = subject;
        this.dateAdded = LocalDateTime.now();
    }

    public static Grade createFromGradeDto(AddGradeDto dto) {
        Grade grade = new Grade();
        grade.setGrade(dto.getGrade());
        grade.setSubject(dto.getSubject());
        return grade;
    }

}
