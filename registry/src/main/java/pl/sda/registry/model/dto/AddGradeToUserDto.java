package pl.sda.registry.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class AddGradeToUserDto {

    @NotNull
    private AddGradeDto dto;

    @NotNull
    private Long userId;
}
