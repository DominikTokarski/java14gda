package pl.sda.registry.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.registry.model.Subject;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class AddGradeDto {

    @NotNull
    @Min(value = 1)
    @Max(value = 6)
    private double grade;

    @NotNull
    private Subject subject;
}
