package pl.sda.registry.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.registry.model.Subject;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SubjectGradesDto {
    private Subject subject;
    private List<Double> grades;
    private Double mean;
}
