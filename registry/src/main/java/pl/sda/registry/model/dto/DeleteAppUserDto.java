package pl.sda.registry.model.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class DeleteAppUserDto {

    @NotNull
    private String name;

    @NotNull
    private String surname;
}
