package pl.sda.registry.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserStatsDto {
    private String name;
    private String surname;
    private List<SubjectGradesDto> grades;
    private Double mean;
}
