package pl.sda.registry.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class UpdateAppUserDto {
    @NotEmpty
    private Long updatedUserId;

    private String name;
    private String surname;

    @Size(min = 4, max = 255)
    private String password;

}
