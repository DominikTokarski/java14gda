package pl.sda.registry.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pl.sda.registry.model.Grade;

import java.util.List;

@Repository
public interface GradeRepository extends JpaRepository<Grade,Long>, JpaSpecificationExecutor<Grade> {

    List<Grade> findAllByGradeGreaterThan(double grade);
}
