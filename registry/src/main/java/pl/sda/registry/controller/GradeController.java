package pl.sda.registry.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.registry.model.Grade;
import pl.sda.registry.model.Subject;
import pl.sda.registry.model.dto.AddGradeDto;
import pl.sda.registry.service.GradeService;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(path = "/grade/")
public class GradeController {

    @Autowired
    private GradeService gradeService;

    @PostMapping("/add")
    public ResponseEntity addGrade(@RequestBody AddGradeDto dto){
        Optional<Grade> gradeOptional = gradeService.addGrade(dto);
        if (gradeOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/add2")
    public ResponseEntity addGrade(@RequestParam(name = "grade") double grade,
                                 @RequestParam(name = "subject")Subject subject){
        AddGradeDto dto = gradeService.setParams(grade,subject);
        Optional<Grade> gradeOptional = gradeService.addGrade(dto);
        if (gradeOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/all")
    public ResponseEntity getAll(){
        List<Grade> gradesList = gradeService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(gradesList);
    }

    @GetMapping("/all2")
    public ResponseEntity getAllAbove3(){
        List<Grade> gradesList = gradeService.getAllWithGradeAbove3();
        return ResponseEntity.status(HttpStatus.OK).body(gradesList);
    }

}
