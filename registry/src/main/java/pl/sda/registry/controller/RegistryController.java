package pl.sda.registry.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;
import pl.sda.registry.model.AppUser;
import pl.sda.registry.model.Grade;
import pl.sda.registry.model.Subject;
import pl.sda.registry.model.dto.AddGradeToUserDto;
import pl.sda.registry.model.dto.GetUserGradesBySubjectDTO;
import pl.sda.registry.model.dto.UserStatsDto;
import pl.sda.registry.service.RegistryService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/registry/")
public class RegistryController {

    @Autowired
    private RegistryService registryService;

    @PostMapping("/addgrade")
    public ResponseEntity addGradeToStudent(@RequestBody AddGradeToUserDto dto){
        Optional<AppUser> optionalAppUser = registryService.addGrade(dto);
        if (optionalAppUser.isPresent()) {
            return ResponseEntity.ok(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/addgrade2")
    public ResponseEntity addGradeToStudent(@RequestParam(name = "grade") double grade,
                                            @RequestParam(name = "subject") Subject subject,
                                            @RequestParam(name = "StudentId")Long studentId){
        Optional<AppUser> optionalAppUser = registryService.addGrade(studentId,subject,grade);
        if (optionalAppUser.isPresent()) {
            return ResponseEntity.ok(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/addgrade/{id}/{subject}/{grade}")
    public ResponseEntity addGradeToStudent(@PathVariable(name = "id")Long studentId,
                                            @PathVariable(name = "subject") Subject subject,
                                            @PathVariable(name = "grade") double grade){
        Optional<AppUser> optionalAppUser = registryService.addGrade(studentId,subject,grade);
        if (optionalAppUser.isPresent()) {
            return ResponseEntity.ok(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/getusergrades")
    public ResponseEntity getGradesBySubject(@Valid @RequestBody GetUserGradesBySubjectDTO dto){
        List<Grade> gradeList = registryService.getGradesBySubject(dto);
        return ResponseEntity.ok(gradeList);

    }

    @GetMapping("/getWeakStudents/{mean}")
    public ResponseEntity getWeakStudents (@PathVariable(name = "mean") double mean){
        List<AppUser> users = registryService.getWeakStudents(mean);
        return ResponseEntity.ok(users);
    }

    @GetMapping("/getUserStats/{id}")
    public ResponseEntity getStats(@PathVariable(name = "id") Long userId){
        Optional<UserStatsDto> userStatsDto = registryService.getStats(userId);
        if (userStatsDto.isPresent()){
            return ResponseEntity.ok(userStatsDto.get());
        }
        return ResponseEntity.badRequest().build();
    }

}
