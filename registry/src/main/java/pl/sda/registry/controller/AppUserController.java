package pl.sda.registry.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.registry.model.AppUser;
import pl.sda.registry.model.dto.CreateUserDto;
import pl.sda.registry.model.dto.DeleteAppUserDto;
import pl.sda.registry.model.dto.NameAndSurnameFilterDto;
import pl.sda.registry.model.dto.UpdateAppUserDto;
import pl.sda.registry.service.AppUserService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(path = "/user/")
public class AppUserController {

    @Autowired
    private AppUserService appUserService;

    @PostMapping("/add")
    public ResponseEntity addUser(@RequestBody CreateUserDto user){
        Optional<AppUser> appUserOptional = appUserService.addUser(user);
        if (appUserOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/get/{id}")
    public ResponseEntity get(@PathVariable(name = "id",required = true) Long id){
        Optional<AppUser> appUserOptional = appUserService.getUserById(id);
        if (appUserOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(appUserOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/updatename/{id}/{name}")
    public ResponseEntity updateName(@PathVariable(name = "id",required = true) Long id,
                               @PathVariable(name = "name",required = true) String name){
        Optional<AppUser> appUserOptional = appUserService.updateName(id,name);
        if (appUserOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(appUserOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/updatelastname/{id}/{surname}")
    public ResponseEntity updateSurname(@PathVariable(name = "id",required = true) Long id,
                               @PathVariable(name = "surname",required = true) String surname){
        Optional<AppUser> appUserOptional = appUserService.updateSurname(id,surname);
        if (appUserOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(appUserOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/all")
    public ResponseEntity getAll(){
        List<AppUser> appUsersList = appUserService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(appUsersList);
    }

    @GetMapping("/all/{surname}")
    public ResponseEntity getBySurname(@PathVariable(name = "surname",required = true) String surname){
        List<AppUser> appUsersList = appUserService.getUserBySurname(surname);
        return ResponseEntity.status(HttpStatus.OK).body(appUsersList);

    }

    @PostMapping("/update")
    public ResponseEntity update(@Valid @RequestBody UpdateAppUserDto updateAppUserDto){
        Optional<AppUser> optionalAppUser = appUserService.update(updateAppUserDto);
        if (optionalAppUser.isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();

    }

    @PostMapping("/delete")
    public ResponseEntity delete(@RequestParam(name = "id") Long id){
        boolean success = appUserService.deleteUser(id);
        if (success) {
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/delete2")
    public ResponseEntity deleteRequestBody(@Valid @RequestBody DeleteAppUserDto deleteAppUserDto){
        int howManyRemoved = appUserService.deleteAppUser(deleteAppUserDto);
        if (howManyRemoved > 0){
            return ResponseEntity.ok(howManyRemoved);
        }
        return ResponseEntity.badRequest().build();

    }

    @PostMapping("/findByNameAndSurname")
    public ResponseEntity findByNameAndSurname(@Valid @RequestBody NameAndSurnameFilterDto dto ){
        List<AppUser> appUsersList = appUserService.getAllUsersByNameAndSurname(dto);
        return ResponseEntity.status(HttpStatus.OK).body(appUsersList);

    }



}

















