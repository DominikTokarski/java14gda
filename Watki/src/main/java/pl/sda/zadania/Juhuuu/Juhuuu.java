package pl.sda.zadania.Juhuuu;

public class Juhuuu {
    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Juhuuu!");
        });
        thread.start();
    }
}
