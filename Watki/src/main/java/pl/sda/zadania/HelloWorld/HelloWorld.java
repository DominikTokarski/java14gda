package pl.sda.zadania.HelloWorld;


public class HelloWorld implements Runnable{
    private String text = "";

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void run() {
        int number = 1;
        while (true){
            System.out.print(number + ".Hello World");
            for (int i = 0; i < number; i++) {
                System.out.print("!");
            }
            System.out.print(" - " + text);
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            number += 1;
            System.out.println();

        }
    }
}

