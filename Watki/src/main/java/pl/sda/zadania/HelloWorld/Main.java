package pl.sda.zadania.HelloWorld;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        HelloWorld helloWorld = new HelloWorld();
        Thread thread = new Thread(helloWorld);
        thread.start();
        Scanner scanner = new Scanner(System.in);
        String line;
        do {
            line = scanner.nextLine();
            helloWorld.setText(line);
        }while(!line.equalsIgnoreCase("quit"));
    }
}
