package pl.sda.gwit.jasImalgosia;

public class Jas implements Runnable {

    @Override
    public void run() {
        System.out.println("Jaś przygotowuje śniadanie.");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Jaś kończy  jeść śniadanie.");
        System.out.println("Jaś bierze prysznic.");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Jaś wychodzi z pod prysnica.");
        System.out.println("Jaś zaczyna się ubierać.");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Jaś jest ubrany.");
        System.out.println("Jaś wychodzi po zakupy.");
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Jaś wraca z zakupów.");
        System.out.println("Jaś zaczyna grać na konsoli.");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Jaś kończy grać na konsoli.");
        Main.latch.countDown();

    }
}
