package pl.sda.gwit.jasImalgosia;

import java.util.concurrent.CountDownLatch;

public class Main {
    public static CountDownLatch latch = new CountDownLatch(2);

    public static void main(String[] args) {
        Jas jas = new Jas();
        Thread dzienJasia = new Thread(jas);
        Malgosia malgosia = new Malgosia();
        Thread dzienMalgosi = new Thread(malgosia);
        dzienJasia.start();
        dzienMalgosi.start();
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Koniec dnia!");


    }
}
