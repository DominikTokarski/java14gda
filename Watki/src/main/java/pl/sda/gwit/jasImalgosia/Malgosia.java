package pl.sda.gwit.jasImalgosia;

public class Malgosia implements Runnable {
    @Override
    public void run() {
        System.out.println("Małgosia zaczyna poranne bieganie");
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Małgosia kończy poranne bieganie.");
        System.out.println("Małgosia bierze prysznic.");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Małgosia wychodzi z pod prysznica");
        System.out.println("Małgosia zaczyna jesć śniadanie.");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Małgosia kończy jeść śniadanie.");
        System.out.println("Małgosia zaczyna się ubierać.");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Małgosia jest ubrana.");
        System.out.println("Małgosia wychodzi na spotkanie z koleżanką.");
        try {
            Thread.sleep(25000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Małgosia wraca ze spotkania z koleżanką.");
        Main.latch.countDown();

    }
}
