package pl.sda.gwit.calkowanie;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Calki {
    private static double function(double x) {
        return (3*Math.sin(x)) - (0.2*Math.pow(x, 3)) + (3*Math.pow(x, 2));
    }
    private static double integration(double sX,double eX, int accuracy){
        double sum = 0.0;
        double base =(eX - sX)/accuracy;

        for (int i = 0; i < accuracy; i++) {
            double x = sX + base/2 + i*base;
            double height = function(x);
            double area = base*height;
            sum += area;

        }

        return sum;
    }

    private static double totalArea =0.0;
    private static final Object totalAreaLock = new Object();

    public static void main(String[] args) throws InterruptedException {
        long startTime = System.currentTimeMillis();
        double area = integration(0,15,15_000_000);
        long endTime = System.currentTimeMillis();
        System.out.println(area + " took " + (endTime - startTime) + "ms");

        ExecutorService threadPool = Executors.newFixedThreadPool(8);
        final CountDownLatch latch = new CountDownLatch(15);

        startTime = System.currentTimeMillis();
        for (int i = 0; i < 15; i++) {
            final int ii = i;
            threadPool.submit(new Runnable() {
                public void run() {
                    double smallArea = integration(ii, ii + 1, 10_000_00);
                    synchronized (totalAreaLock) {
                        totalArea += smallArea;
                    }
                    latch.countDown();
                }
            });
        }
        latch.await();
        threadPool.shutdown();
        endTime = System.currentTimeMillis();
        System.out.println(totalArea + " took " + (endTime - startTime) + "ms");
    }
}
