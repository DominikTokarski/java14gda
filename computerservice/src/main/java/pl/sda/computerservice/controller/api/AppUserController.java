package pl.sda.computerservice.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.computerservice.model.AppUser;
import pl.sda.computerservice.model.dto.CreateUserDto;
import pl.sda.computerservice.model.dto.IsUserExistsDto;
import pl.sda.computerservice.model.dto.UpdateAppUserDto;
import pl.sda.computerservice.service.AppUserService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/user")
public class AppUserController {

    @Autowired
    private AppUserService appUserService;

    @PostMapping("/add")
    public ResponseEntity addUser(@RequestBody CreateUserDto user){
        Optional<AppUser> appUserOptional = appUserService.addUser(user);
        if (appUserOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.badRequest().build();
    }


    @PostMapping("/update")
    public ResponseEntity update(@Valid @RequestBody UpdateAppUserDto updateAppUserDto) {
        Optional<AppUser> optionalAppUser = appUserService.update(updateAppUserDto);
        if (optionalAppUser.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/exist")
    public ResponseEntity exist(@Valid @RequestBody IsUserExistsDto isUserExistsDto){
        List<AppUser> listExist = appUserService.exist(isUserExistsDto);
        return ResponseEntity.ok(listExist);

    }

    @GetMapping("/get/{id}")
    public ResponseEntity get(@PathVariable(name = "id",required = true) Long id){
        Optional<AppUser> appUserOptional = appUserService.getUserById(id);
        if (appUserOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(appUserOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/all")
    public ResponseEntity getAll(){
        List<AppUser> appUsersList = appUserService.getAll();
        return ResponseEntity.status(HttpStatus.OK).body(appUsersList);
    }

}
