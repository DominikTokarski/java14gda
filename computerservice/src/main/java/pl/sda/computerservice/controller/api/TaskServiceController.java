package pl.sda.computerservice.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.computerservice.model.AppUser;
import pl.sda.computerservice.model.Computer;
import pl.sda.computerservice.model.ServiceTask;
import pl.sda.computerservice.model.dto.AddTaskToComputerDto;
import pl.sda.computerservice.model.dto.UpdateTaskDto;
import pl.sda.computerservice.service.LoginService;
import pl.sda.computerservice.service.TaskService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/task")
public class TaskServiceController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private LoginService loginService;

    @PostMapping("/add")
    public ResponseEntity addTaskToUser(@RequestBody AddTaskToComputerDto dto) {
        Optional<Computer> optionalComputer = taskService.addTask(dto);
        if (optionalComputer.isPresent()) {
            return ResponseEntity.ok(optionalComputer.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/computerTasks")
    public ResponseEntity getTasksFromComputer(@RequestParam(name = "id") Long id) {
        List<ServiceTask> serviceTaskList = taskService.getTasksFromComputer(id);
        return ResponseEntity.status(HttpStatus.OK).body(serviceTaskList);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable(name = "id") Long id) {
        boolean success = taskService.deleteTask(id);
        if (success) {
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/modify")
    public ResponseEntity modifyTask(@Valid @RequestBody UpdateTaskDto dto) {
        Optional<ServiceTask> optionalComputer = taskService.update(dto);
        if (optionalComputer.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(optionalComputer.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/userTasks")
    public ResponseEntity getTasksFromUser(@RequestParam(name = "id") Long id) {
        List<ServiceTask> serviceTaskList = taskService.getTasksFromUser(id);
        return ResponseEntity.status(HttpStatus.OK).body(serviceTaskList);
    }





}
