package pl.sda.computerservice.controller.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.computerservice.model.AppUser;
import pl.sda.computerservice.model.Computer;
import pl.sda.computerservice.model.ServiceTask;
import pl.sda.computerservice.model.dto.AddComputerToUserDto;
import pl.sda.computerservice.service.ComputerService;
import pl.sda.computerservice.service.LoginService;
import pl.sda.computerservice.service.TaskService;

import java.util.List;
import java.util.Optional;


@Controller
@RequestMapping("/view/computer")
public class ComputerViewController {

    @Autowired
    private ComputerService computerService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private TaskService taskService;

    @GetMapping("/add")
    public String add(Model model) {
        Optional<AppUser> appUserOptional = loginService.getLoggedInUser();
        if (!appUserOptional.isPresent()) {
            return "redirect:/login";
        }
        AddComputerToUserDto addComputerToUserDto = new AddComputerToUserDto();

        addComputerToUserDto.setUserId(appUserOptional.get().getId());

        model.addAttribute("added_computer", addComputerToUserDto);
        return "computer/add_form";
    }

    @PostMapping("/add")
    public String add(Model model, AddComputerToUserDto dto) {
        if (!loginService.IsDtoAndLoggedUserIdIsEquals(dto.getUserId())) {
            return "redirect:/login";
        }
        if (dto.getUserId() == null || dto.getName().isEmpty() || dto.getPrice() == null) {
            model.addAttribute("added_computer", dto);
            model.addAttribute("error_message", "Some field is empty.");
            return "computer/add_form";
        }
        if (dto.getModifiedComputerId() == null) {
            computerService.addComputer(dto);
            return "redirect:/view/user/profile";
        }
        computerService.update(dto.getModifiedComputerId(), dto.getName(), dto.getPrice());
        return "redirect:/view/user/profile";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        Optional<Computer> optionalComputer = computerService.findById(id);
        Computer computer = optionalComputer.get();
        List<ServiceTask> taskList = computer.getServiceTaskList();
        for (ServiceTask task : taskList) {
            taskService.deleteTask(task.getId());
        }

        computerService.delete(id);
        return "redirect:/view/user/profile";
    }

    @GetMapping("/modify/{id}")
    public String update(Model model, @PathVariable(name = "id") Long id) {
        Optional<AppUser> appUserOptional = loginService.getLoggedInUser();
        if (appUserOptional.isPresent()) {
            Optional<Computer> optionalComputer = computerService.findById(id);
            if (optionalComputer.isPresent()) {
                Computer computer = optionalComputer.get();

                AddComputerToUserDto dto = new AddComputerToUserDto();

                dto.setModifiedComputerId(id);
                dto.setUserId(appUserOptional.get().getId());
                dto.setName(computer.getName());
                dto.setPrice(computer.getPrice());
                model.addAttribute("added_computer", dto);
                return "computer/add_form";
            }
        }
        return "redirect:/login";
    }
}
