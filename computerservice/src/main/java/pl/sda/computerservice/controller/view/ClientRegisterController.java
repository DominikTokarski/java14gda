package pl.sda.computerservice.controller.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.computerservice.model.AppUser;
import pl.sda.computerservice.model.dto.CreateUserDto;
import pl.sda.computerservice.service.AppUserService;
import pl.sda.computerservice.service.LoginService;

import java.util.Optional;

@Controller
public class ClientRegisterController {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/register")
    public String getAddView(Model model) {
        model.addAttribute("added_object", new CreateUserDto());
        return "user/register_form";
    }


    @PostMapping("/register")
    public String getAddView(Model model,CreateUserDto dto) {
        if (dto.getPassword().isEmpty() || !dto.getPassword().equals(dto.getPasswordConfirm())){
            model.addAttribute("added_object",dto);
            model.addAttribute("error_message","Passwords not match.");
            return "user/register_form";
        }
        Optional<AppUser> appUserOptional = appUserService.addUser(dto);
        if (!appUserOptional.isPresent()){
            model.addAttribute("added_object",dto);
            model.addAttribute("error_message","User with that email already exists.");
            return "user/register_form";
        }

        return "redirect:/";
    }

    @GetMapping("/login")
    public String getLoginView(){
        return "user/login_form";
    }

    @GetMapping("/")
    public String getIndex(Model model){
        Optional<AppUser> appUserOptional = loginService.getLoggedInUser();
        if (appUserOptional.isPresent()){
            AppUser appUser = appUserOptional.get();
            model.addAttribute("username",appUser.getEmail());
        }
        return "index";
    }
}
