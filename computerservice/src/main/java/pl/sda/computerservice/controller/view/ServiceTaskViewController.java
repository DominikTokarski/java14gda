package pl.sda.computerservice.controller.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.computerservice.model.AppUser;
import pl.sda.computerservice.model.Computer;
import pl.sda.computerservice.model.ServiceTask;
import pl.sda.computerservice.model.dto.AddTaskToComputerDto;
import pl.sda.computerservice.model.dto.UpdateTaskDto;
import pl.sda.computerservice.service.ComputerService;
import pl.sda.computerservice.service.LoginService;
import pl.sda.computerservice.service.TaskService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/view/task")
public class ServiceTaskViewController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ComputerService computerService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/add")
    public String add(Model model){
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        if (optionalAppUser.isPresent()){
            AppUser appUser = optionalAppUser.get();
            model.addAttribute("computer_list",appUser.getComputerList());
            AddTaskToComputerDto addTaskToComputerDto = new AddTaskToComputerDto();
            model.addAttribute("added_task",addTaskToComputerDto);
            return "task/add_form";
        }
        return "redirect:/login";

    }

    @PostMapping("/add")
    public String add(Model model, AddTaskToComputerDto dto){
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        if (optionalAppUser.isPresent()){
            AppUser appUser = optionalAppUser.get();
            Optional<Computer> optionalComputer = appUser.getComputerList().stream().filter(computer ->
                    computer.getId().equals(dto.getComputerId())).findAny();
            if (optionalComputer.isPresent()){
                taskService.addTask(dto);
                return "redirect:/view/task/list";
            }
        }
        return "redirect:/view/task/add";
    }

    @GetMapping("/list")
    public String getTasksFromUser(Model model){
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        if (optionalAppUser.isPresent()) {
            List<ServiceTask> serviceTaskList = taskService.getTasksFromUser(optionalAppUser.get().getId());
            model.addAttribute("serviceTasks",serviceTaskList);
            return "task/list";
        }
        return "redirect:/login";
    }

    @GetMapping("/delete/{id}")
    public String deleteTask(@PathVariable(name = "id") Long id) {
        taskService.delete(id);
        return "redirect:/view/task/list";
    }

    @GetMapping("/modify/{id}")
    public String update(Model model, @PathVariable(name = "id") Long id) {
        Optional<AppUser> appUserOptional = loginService.getLoggedInUser();
        if (appUserOptional.isPresent()) {
            Optional<ServiceTask> optionalServiceTask = taskService.findById(id);
            if (optionalServiceTask.isPresent()) {
                ServiceTask task = optionalServiceTask.get();

                UpdateTaskDto dto = new UpdateTaskDto();

                dto.setId(id);
                dto.setDone(task.isDone());
                dto.setDescription(task.getDescription());
                model.addAttribute("modify_task", dto);
                return "task/modify";
            }
        }
        return "redirect:/login";
    }

    @PostMapping("/modify")
    public String update(UpdateTaskDto dto){
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        if (optionalAppUser.isPresent()){
            taskService.update(dto);
                return "redirect:/view/task/list";
        }
        return "redirect:/login";
    }

    @GetMapping("/add/{device_id}")
    public String getAddForm(Model model, @PathVariable(name = "device_id") Long id) {
        Optional<AppUser> loggedInClient = loginService.getLoggedInUser();
        if (loggedInClient.isPresent()) {
            AppUser appUser = loggedInClient.get();

            Optional<Computer> deviceOpt = appUser.getComputerList()
                    .stream().filter(device -> device.getId() == id).findAny();
            if (deviceOpt.isPresent()) {
                AddTaskToComputerDto addServiceTask = new AddTaskToComputerDto();
                addServiceTask.setComputerId(id);
                model.addAttribute("added_task", addServiceTask);
                return "task/add_form";
            }
        }

        return "redirect:/login";
    }
}
