package pl.sda.computerservice.controller.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.computerservice.model.AppUser;
import pl.sda.computerservice.model.Computer;
import pl.sda.computerservice.model.dto.CreateUserDto;
import pl.sda.computerservice.service.AppUserService;
import pl.sda.computerservice.service.ComputerService;
import pl.sda.computerservice.service.LoginService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/view/user")
public class AppUserViewController {


    @Autowired
    private LoginService loginService;

    @GetMapping("/profile")
    public String viewProfile(Model model){
        Optional<AppUser> appUserOptional = loginService.getLoggedInUser();
        if (!appUserOptional.isPresent()){
            return "redirect:/";
        }
        AppUser appUser = appUserOptional.get();
        List<Computer> list = appUser.getComputerList();
        model.addAttribute("user", appUser);
        model.addAttribute("computer_list",list);
        return "user/profile";
    }


}
