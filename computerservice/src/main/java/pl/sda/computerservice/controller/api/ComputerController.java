package pl.sda.computerservice.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sda.computerservice.model.AppUser;
import pl.sda.computerservice.model.Computer;
import pl.sda.computerservice.model.dto.AddComputerToUserDto;
import pl.sda.computerservice.service.ComputerService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/computer")
public class ComputerController {

    @Autowired
    private ComputerService computerService;

    @PostMapping("/add")
    public ResponseEntity addComputerToUser(@RequestBody AddComputerToUserDto dto){
        Optional<AppUser> optionalAppUser = computerService.addComputer(dto);
        if (optionalAppUser.isPresent()) {
            return ResponseEntity.ok(optionalAppUser.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/modify")
    public ResponseEntity modifyComputer(@RequestParam(name = "id") Long id,
                                         @RequestParam(name = "name") String name,
                                         @RequestParam(name = "price") double price){
        Optional<Computer> optionalComputer = computerService.update(id,name,price);
        if (optionalComputer.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(optionalComputer.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/userComputers")
    public ResponseEntity getComputersFromUser(@RequestParam(name = "id")Long id){
        List<Computer> computerList = computerService.getComputersFromUser(id);
        return ResponseEntity.status(HttpStatus.OK).body(computerList);
    }


}
