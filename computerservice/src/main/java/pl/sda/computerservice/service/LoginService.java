package pl.sda.computerservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.computerservice.model.AppUser;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class LoginService implements UserDetailsService {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<AppUser> optionalAppUser =appUserService.findByEmail(email);
        if (optionalAppUser.isPresent()){
            AppUser appUser = optionalAppUser.get();
            return new User(appUser.getEmail(),appUser.getPassword(),createAuthorities(appUser.getEmail()));
        }
        return null;
    }

    private Collection<? extends GrantedAuthority> createAuthorities(String email) {
        Set<GrantedAuthority> authoritySet = new HashSet<>();

        if (email.equals("admin@admin.admin")){
            authoritySet.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }
        authoritySet.add(new SimpleGrantedAuthority("ROLE_USER"));

        return authoritySet;
    }

    public Optional<AppUser> getLoggedInUser(){
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()){
           return Optional.empty();
        }
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return appUserService.findByEmail(user.getUsername());
        }
        return Optional.empty();
    }

    public boolean IsDtoAndLoggedUserIdIsEquals(Long id){
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()){
            return false;
        }
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (appUserService.findByEmail(user.getUsername()).get().getId() == id){
                return true;
            }
        }
        return false;
    }
}
