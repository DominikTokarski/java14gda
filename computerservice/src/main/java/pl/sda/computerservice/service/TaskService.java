package pl.sda.computerservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.computerservice.model.AppUser;
import pl.sda.computerservice.model.Computer;
import pl.sda.computerservice.model.ServiceTask;
import pl.sda.computerservice.model.dto.AddTaskToComputerDto;
import pl.sda.computerservice.model.dto.UpdateTaskDto;
import pl.sda.computerservice.repository.AppUserRepository;
import pl.sda.computerservice.repository.ComputerRepository;
import pl.sda.computerservice.repository.TaskRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskService {

    @Autowired
    private ComputerRepository computerRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private AppUserRepository appUserRepository;

    public boolean deleteTask(Long id) {
        if (taskRepository.existsById(id)) {
            taskRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public Optional<Computer> addTask(AddTaskToComputerDto dto) {
        Optional<Computer> optionalComputer = computerRepository.findById(dto.getComputerId());
        if (optionalComputer.isPresent()){
            Computer computer = optionalComputer.get();

            ServiceTask serviceTask = new ServiceTask();
            serviceTask.setDescription(dto.getDescription());

            taskRepository.save(serviceTask);

            computer.getServiceTaskList().add(serviceTask);

            computerRepository.save(computer);

            return Optional.of(computer);
        }
        return  Optional.empty();
    }

    public List<ServiceTask> getTasksFromComputer(Long id) {
        Optional<Computer> optionalComputer = computerRepository.findById(id);
        if (optionalComputer.isPresent()) {
            Computer computer = optionalComputer.get();
            return computer.getServiceTaskList();
        }
        return new ArrayList<>();
    }

    public Optional<ServiceTask> update(UpdateTaskDto dto) {
        Optional<ServiceTask> optionalOrder = taskRepository.findById(dto.getId());
        if (optionalOrder.isPresent()){

            ServiceTask serviceTask = optionalOrder.get();
            serviceTask.setDescription(dto.getDescription());
            if (dto.isDone()){
                serviceTask.setDone(true);
                serviceTask.setEndDate(LocalDateTime.now());
            }
            taskRepository.save(serviceTask);
            return Optional.of(serviceTask);
        }
        return  Optional.empty();
    }

    public List<ServiceTask> getTasksFromUser(Long id) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(id);
        if (optionalAppUser.isPresent()){
            AppUser appUser = optionalAppUser.get();
            List<Computer> computerList = appUser.getComputerList();
            List<ServiceTask> serviceTaskList = new ArrayList<>();
            for (Computer computer: computerList){
                for (ServiceTask serviceTask :computer.getServiceTaskList()){
                    serviceTaskList.add(serviceTask);
                }
            }
            return serviceTaskList;
        }
        return new ArrayList<>();
    }

    public Optional<ServiceTask> findById(Long id) {
        return taskRepository.findById(id);
    }


    public boolean delete(Long id) {
            if (taskRepository.existsById(id)) {
                taskRepository.deleteById(id);
                return true;
            }
            return false;
    }
}
