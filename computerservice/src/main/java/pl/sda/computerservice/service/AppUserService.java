package pl.sda.computerservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.computerservice.model.AppUser;
import pl.sda.computerservice.model.dto.CreateUserDto;
import pl.sda.computerservice.model.dto.IsUserExistsDto;
import pl.sda.computerservice.model.dto.UpdateAppUserDto;
import pl.sda.computerservice.repository.AppUserRepository;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;


    public Optional<AppUser> addUser(CreateUserDto user) {
        if (!appUserRepository.findByEmail(user.getEmail()).isPresent())
        if (user.getPassword().equals(user.getPasswordConfirm())) {
            AppUser createdUser = AppUser.createFromDto(user);
            createdUser.setPassword(encoder.encode(user.getPassword()));
            createdUser = appUserRepository.save(createdUser);
            return Optional.of(createdUser);
        }
        return Optional.empty();
    }

    public Optional<AppUser> update(UpdateAppUserDto updateAppUserDto) {
        Optional<AppUser> appUserOptional = appUserRepository.findById(updateAppUserDto.getUpdatedUserId());
        if (appUserOptional.isPresent()){
            AppUser appUser = appUserOptional.get();

            if (updateAppUserDto.getEmail() != null) {
                appUser.setName(updateAppUserDto.getEmail());
            }
            if (updateAppUserDto.getSurname() != null){
                appUser.setSurname(updateAppUserDto.getSurname());
            }
            if (updateAppUserDto.getPassword() != null){
                appUser.setPassword(updateAppUserDto.getPassword());
            }
            if (updateAppUserDto.getName() != null){
                appUser.setName(updateAppUserDto.getName());
            }

            appUser = appUserRepository.save(appUser);

            return Optional.of(appUser);
        }

        return Optional.empty();
    }

    public List<AppUser> exist(IsUserExistsDto isUserExistsDto) {
        List<AppUser> listExist = appUserRepository.
                existsAllByNameAndSurnameAndEmail(isUserExistsDto.getName(),
                        isUserExistsDto.getSurname(),
                        isUserExistsDto.getEmail());
        if (listExist != null){
            return listExist;
        }
        return appUserRepository.findAll();
    }

    public Optional<AppUser> getUserById(Long id) {
        return appUserRepository.findById(id);
    }

    public List<AppUser> getAll() {
        return appUserRepository.findAll();
    }


    public Optional<AppUser> findByEmail(String email) {
        return appUserRepository.findByEmail(email);
    }
}
