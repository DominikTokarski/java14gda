package pl.sda.computerservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.computerservice.model.AppUser;
import pl.sda.computerservice.model.Computer;
import pl.sda.computerservice.model.dto.AddComputerToUserDto;
import pl.sda.computerservice.repository.AppUserRepository;
import pl.sda.computerservice.repository.ComputerRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ComputerService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private ComputerRepository computerRepository;

    public Optional<AppUser> addComputer(AddComputerToUserDto dto) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(dto.getUserId());
        if (optionalAppUser.isPresent()){
            AppUser appUser = optionalAppUser.get();

            Computer computer = new Computer();
            computer.setName(dto.getName());
            computer.setPrice(dto.getPrice());

            computerRepository.save(computer);

            appUser.getComputerList().add(computer);

            appUserRepository.save(appUser);

            return Optional.of(appUser);
        }
        return  Optional.empty();
    }

    public Optional<Computer> update(Long id, String name, double price) {
        Optional<Computer> optionalComputer = computerRepository.findById(id);
        if (optionalComputer.isPresent()){
            Computer computer = optionalComputer.get();
            computer.setName(name);
            computer.setPrice(price);
            computerRepository.save(computer);
            return Optional.of(computer);
        }
        return Optional.empty();
    }

    public List<Computer> getComputersFromUser(Long id) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(id);
        if (optionalAppUser.isPresent()) {
            AppUser appUser = optionalAppUser.get();
            return appUser.getComputerList();
        }
        return new ArrayList<>();
    }


    public List<Computer> getAll() {
        return computerRepository.findAll();
    }

    public boolean delete(Long id) {
        if (computerRepository.existsById(id)) {
            computerRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public Optional<Computer> findById(Long id) {
        return computerRepository.findById(id);
    }


}
