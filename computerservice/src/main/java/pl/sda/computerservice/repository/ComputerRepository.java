package pl.sda.computerservice.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.computerservice.model.Computer;

@Repository
public interface ComputerRepository extends JpaRepository<Computer,Long> {
}
