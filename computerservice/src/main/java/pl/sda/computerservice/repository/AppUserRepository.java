package pl.sda.computerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pl.sda.computerservice.model.AppUser;

import java.util.List;
import java.util.Optional;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser,Long>, JpaSpecificationExecutor<AppUser> {

    List<AppUser> existsAllByNameAndSurnameAndEmail(String name, String surname, String email);

    Optional<AppUser> findByEmail(String email);
}
