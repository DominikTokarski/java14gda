package pl.sda.computerservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.computerservice.model.ServiceTask;


@Repository
public interface TaskRepository extends JpaRepository<ServiceTask,Long> {


}
