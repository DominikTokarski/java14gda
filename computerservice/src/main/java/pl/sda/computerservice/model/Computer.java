package pl.sda.computerservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;


@Getter
@Setter
@Entity
public class Computer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private Double price;
    private LocalDateTime addDate;

    @OneToMany(fetch = FetchType.EAGER)
    private List<ServiceTask> serviceTaskList;

    public Computer() {
        addDate = LocalDateTime.now();
    }

    public Computer(String name, Double price, List<ServiceTask> serviceTaskList) {
        this.name = name;
        this.price = price;
        this.addDate = LocalDateTime.now();
        this.serviceTaskList = serviceTaskList;
    }
}
