package pl.sda.computerservice.model.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
public class UpdateTaskDto {

    private Long id;
    private boolean isDone;
    private String description;
    private LocalDateTime endDate;

    public boolean isDone() {
        return isDone;
    }
}
