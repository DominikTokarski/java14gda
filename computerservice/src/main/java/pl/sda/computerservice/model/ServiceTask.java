package pl.sda.computerservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@AllArgsConstructor
public class ServiceTask {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDateTime addDate;
    private boolean isDone;
    private LocalDateTime endDate;
    private String description;

    public ServiceTask() {
        addDate = LocalDateTime.now();
        isDone = false;
    }

    public ServiceTask(LocalDateTime endDate, String description) {
        this.addDate = LocalDateTime.now();
        this.isDone = false;
        this.endDate = endDate;
        this.description = description;
    }
}
