package pl.sda.computerservice.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.computerservice.model.dto.CreateUserDto;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Email
    private String email;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String password;

    private String name;
    private String surname;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Computer> computerList;

    public static AppUser createFromDto(CreateUserDto appUserDto){
        AppUser appUser = new AppUser();
        appUser.setEmail(appUserDto.getEmail());
        appUser.setPassword(appUserDto.getPassword());
        return appUser;
    }
}
