package pl.sda.computerservice.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AddTaskToComputerDto {

    @NotNull
    private Long computerId;
    private String description;
}
