package pl.sda;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

public class PanTadeusz {
    public static void main(String[] args) {
        File file = new File("panTadeusz.txt");

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String laneFromStart;
            Map<String,Integer> map = new HashMap<>();
            while ((laneFromStart = reader.readLine()) != null) {
                laneFromStart = laneFromStart.trim();
                laneFromStart = laneFromStart.replaceAll("[^A-Za-ząĄćĆęĘłŁńŃóÓśŚźŹżŻ]]"," ");
                String[] words = laneFromStart.toLowerCase().split(" ");
                for (int i = 0; i < words.length; i++) {
                    if (map.containsKey(words[i])){
                        int a = map.get(words[i]) + 1;
                            map.put(words[i],a);
                    }else{
                            map.put(words[i],1);
                    }
                }
            }
            List<Map.Entry<String, Integer>> list = new ArrayList<>(map.entrySet());
            Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
                @Override
                public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                    return (o2.getValue()).compareTo( o1.getValue() );
                }
            });
            int end = 0;
            for(Map.Entry<String, Integer> entry:list){
                end++;
                System.out.println(entry.getKey()+" = "+entry.getValue());
                if (end == 5){
                    break;
                }
            }
            int once = 0;
            for(String word :map.keySet()){
                int i = map.get(word);
                if (i == 1){
                once++;
                }
            }
            System.out.println(once);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {

        }
    }



}
