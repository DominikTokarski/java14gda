package pl.sda.game;

import java.io.*;
import java.util.*;

public class Quiz {
    private static File folder = new File("C:/Users/Dominik/Desktop/Java14GDA/Gra/quiz");
    private static File[] files = folder.listFiles();
    private static Scanner scanner = new Scanner(System.in);

    private static Map<String,Map<String,List<String>>> loadFromFile() throws IOException {
        Map<String,Map<String,List<String>>> categories= new HashMap<>();
        for (int i = 0; i <files.length ; i++) {
            Map<String,List<String>> questions = new HashMap<>();
            BufferedReader reader = new BufferedReader(new FileReader(files[i]));
            String laneFromStart;
            String tmp = null;
            List<String> answers = null;
            while ((laneFromStart = reader.readLine()) != null){
                if (laneFromStart.endsWith(".")
                        || laneFromStart.endsWith("?")
                        || laneFromStart.endsWith("!")){
                    answers = new ArrayList<>();
                    questions.put(laneFromStart,null);
                    tmp = laneFromStart;
                    reader.readLine();
                }else {
                    answers.add(laneFromStart);
                    questions.replace(tmp,answers);
                }
            }
            categories.put(files[i].getName(),questions);
        }
        return categories;
    }

    private static Map<String,List<String>> chooseCategory(String category) throws IOException {
        return loadFromFile().get(category);
    }

    private static void start(String category) throws IOException {
        int good = 0;
        for (int i = 1; i < 11; i++) {
            System.out.println(i + ". question");
            String real = getQuestion(category).trim();
            System.out.println("choose your answer");
            String answer = scanner.nextLine();
            if (answer.equals(real)) {
                good++;
                System.out.println("Good answer");
            } else {
                System.out.println("Bad answer");
            }

        }
        System.out.println(good + "/10");
    }

    private static String getQuestion(String category) throws IOException {
        String[] questions = new String[chooseCategory(category).size()];
        Map<String,List<String>> qa = chooseCategory(category);
        int index = 0;
        for (String key : chooseCategory(category).keySet()){
            questions[index] = key;
            index += 1;
        }
        Random random = new Random();
        index = random.nextInt(questions.length);
        List<String> answers = qa.get(questions[index]);
        String good = answers.get(0);
        System.out.println(questions[index]);
        Collections.shuffle(answers);
        System.out.println(Arrays.toString(new List[]{answers}));
        return good;
    }

    public static void main(String[] args) throws IOException {
        System.out.println("Let's get start");
        System.out.println("Choose one of categories :");
        Map<Integer,String> choose = new HashMap<>();

        String[] categories = new String[files.length];
        for (int i = 0; i < files.length ; i++) {
            choose.put(i,files[i].getName());
            categories[i] = i + "." + files[i].getName()
                    .replace("txt","")
                    .replace("_"," ");
            System.out.println(categories[i]);
        }
        int chosen = Integer.parseInt(scanner.nextLine());
        start(choose.get(chosen));


    }

}

