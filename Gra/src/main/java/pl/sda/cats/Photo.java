package pl.sda.cats;

import com.google.gson.Gson;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.*;
import java.net.URL;

public class Photo {
    public static void main(String[] args) {
        for (int i = 1; i < 11; i++) {
            try {
                URL url = new URL("https://dog.ceo/api/breeds/image/random");
                InputStream is = url.openStream();
                BufferedReader in = new BufferedReader(new InputStreamReader(is));
                String inputLine = in.readLine();
                Gson gson = new Gson();
                Dog dog = gson.fromJson(inputLine,Dog.class);
                BufferedImage reader = ImageIO.read(new URL(dog.message));
                File file;
                if (dog.message.endsWith("g")) {
                    file = new File("C:/Users/Dominik/Desktop/Java14GDA/Gra/Psy/Pies_0" + i + ".jpg");
                    ImageIO.write(reader, "jpg",
                            file);
                }else {
                    file = new File("C:/Users/Dominik/Desktop/Java14GDA/Gra/Psy/Pies_0" + i + ".gif");
                    ImageIO.write(reader, "gif",
                            file);
                }
                int height = reader.getHeight();
                int width = reader.getWidth();
                int resolution = height*width;
                float[] data = new float[height * width];
                float weight = 1.0f / (height * width);
                System.out.println(i + " . " + resolution + " size: " + file.length());
                for (int j = 0; j < data.length; j++) {
                    data[i] = weight;
                }
                Kernel kernel = new Kernel(width,height,data);
                ConvolveOp op = new ConvolveOp(kernel,ConvolveOp.EDGE_NO_OP,null);
                BufferedImage image = op.filter(reader,null);
                ImageIO.write(image,"jpg",file);

            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }
}
