<%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 24.09.2018
  Time: 18:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tabliczka mnozenia static</title>
</head>
<body>
<%
    out.print("<table border=\"1\">");
    for (int i = 1; i <= 50; i++) {
        out.print("<tr>");
        for (int j = 1; j <= 50; j++) {
            out.print("<td>" + i*j + "</td>");
        }
    }
    out.print("</tr>");

    out.print("</table>");
%>
</body>
</html>
