<%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 24.09.2018
  Time: 19:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tabliczka Mnożenia Dwa</title>
</head>
<body>
<h2>Formularz wielkości tabliczki mnożenia:</h2>
<form action="tabliczka_mnozenia_dwa.jsp" method="get">

    <p>Podaj wysokosc</p>
    <input type="number" name="wysokosc">
    <p>Podaj szerokosc</p>
    <input type="number" name="szerokosc">
    <input type="submit" name="Wygeneruj">


</form>

<hr>
<h2>Tabliczka mnożenia</h2>
<%
    String parametrWysokosc = request.getParameter("wysokosc");
    String parametrSzerokosc = request.getParameter("szerokosc");
    out.print("Tabliczka ma wielkość: " + parametrWysokosc + "x" + parametrSzerokosc);
    try{
        Integer wielkosc = Integer.parseInt(parametrWysokosc);
        Integer wielkosc1 = Integer.parseInt(parametrSzerokosc);
        out.print("<table border=\"1\">");
        for (int i = 1; i <= wielkosc; i++) {
            out.print("<tr>");
            for (int j = 1; j <= wielkosc1; j++) {
                out.print("<td>" + i*j + "</td>");
            }
        }
        out.print("</tr>");
        out.print("</table>");
    }catch (NumberFormatException nfe){
    }
%>

</body>
</html>
