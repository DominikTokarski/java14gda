<%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 24.09.2018
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Tabliczka mnozenia</title>
</head>
<body>
<h2>Formularz wielkości tabliczki mnożenia:</h2>
<form action="tabliczka_mnozenia.jsp" method="get">

    <input type="number" name="wielkosc">
    <input type="submit" value="Wygeneruj">

</form>

<hr>
<h2>Tabliczka mnożenia</h2>
<%
    String parametrWielkosc = request.getParameter("wielkosc");
    out.print("Tabliczka ma wielkość: " + parametrWielkosc + "x" +parametrWielkosc);
    try{
        Integer wielkosc = Integer.parseInt(parametrWielkosc);
        out.print("<table border=\"1\">");
        for (int i = 1; i <= wielkosc; i++) {
            out.print("<tr>");
            for (int j = 1; j <= wielkosc; j++) {
                out.print("<td>" + i*j + "</td>");
            }
        }
        out.print("</tr>");
        out.print("</table>");
    }catch (NumberFormatException nfe){
        out.print("Błąd, niepoprawna wartość parametru");
    }
%>

</body>
</html>
