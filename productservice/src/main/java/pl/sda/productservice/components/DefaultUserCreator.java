package pl.sda.productservice.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import pl.sda.productservice.model.AppUser;
import pl.sda.productservice.model.dto.CreateUserDto;
import pl.sda.productservice.service.AppUserService;

import java.util.Optional;

@Component
@Lazy
public class DefaultUserCreator {

    @Autowired
    public DefaultUserCreator(AppUserService appUserService) {
        Optional<AppUser> clientOptional = appUserService.findByUsername("admin");
        if (!clientOptional.isPresent()) {
            appUserService.addUser(
                    new CreateUserDto(true,"admin","admin@admin.admin","admin"));
        }
    }
}