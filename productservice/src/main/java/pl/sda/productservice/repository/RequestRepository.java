package pl.sda.productservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.productservice.model.Request;

@Repository
public interface RequestRepository extends JpaRepository<Request,Long> {
}
