package pl.sda.productservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.sda.productservice.components.DefaultUserCreator;
import pl.sda.productservice.model.AppUser;

import java.util.Optional;


@Service
public class LoginService implements UserDetailsService {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private DefaultUserCreator defaultUserCreator;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<AppUser> optionalAppUser =appUserService.findByUsername(username);
        if (optionalAppUser.isPresent()){
            AppUser appUser = optionalAppUser.get();

            if (appUser.isAdmin()) {
                return User.withUsername(appUser.getUsername())
                        .password(appUser.getPassword()).roles("ADMIN", "USER").build();
            }
            return User.withUsername(appUser.getUsername()).password(appUser.getPassword()).roles("USER").build();
        }

        return null;
    }



    public Optional<AppUser> getLoggedInUser(){
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()){
           return Optional.empty();
        }
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return appUserService.findByUsername(user.getUsername());
        }
        return Optional.empty();
    }

    public boolean IsDtoAndLoggedUserIdIsEquals(Long id){
        if (SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null ||
                !SecurityContextHolder.getContext().getAuthentication().isAuthenticated()){
            return false;
        }
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (appUserService.findByUsername(user.getUsername()).get().getId() == id){
                return true;
            }
        }
        return false;
    }
}
