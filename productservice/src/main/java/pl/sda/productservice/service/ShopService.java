package pl.sda.productservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.productservice.model.Shop;
import pl.sda.productservice.model.dto.AddShopDto;
import pl.sda.productservice.repository.ShopRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ShopService {

    @Autowired
    private ShopRepository shopRepository;

    public List<Shop> getAll() {
        return shopRepository.findAll();
    }

    public Optional<Shop> add(AddShopDto dto) {
            Shop shop = new Shop();
            shop.setLink(dto.getLink());
            shop.setName(dto.getName());
            shopRepository.save(shop);
            return Optional.of(shop);
    }

    public boolean delete(Long id) {
        if (shopRepository.existsById(id)){
            shopRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
