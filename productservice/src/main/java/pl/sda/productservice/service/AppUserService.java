package pl.sda.productservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.productservice.model.AppUser;
import pl.sda.productservice.model.dto.CreateUserDto;
import pl.sda.productservice.repository.AppUserRepository;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;


    public Optional<AppUser> addUser(CreateUserDto user) {
        if (!appUserRepository.findByUsername(user.getUsername()).isPresent()) {

            AppUser createdUser = AppUser.createFromDto(user);
            createdUser.setPassword(encoder.encode(user.getPassword()));
            createdUser = appUserRepository.save(createdUser);
            return Optional.of(createdUser);
        }

        return Optional.empty();
    }



    public Optional<AppUser> findByUsername(String email) {
        return appUserRepository.findByUsername(email);
    }

    public List<AppUser> getAll() {
        return appUserRepository.findAll();
    }

    public Optional<AppUser> findUserById(Long id) {
        return appUserRepository.findById(id);
    }

    public boolean delete(Long id) {
        if (appUserRepository.existsById(id)) {
            appUserRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
