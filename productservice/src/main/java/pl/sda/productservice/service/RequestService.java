package pl.sda.productservice.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.productservice.model.AppUser;
import pl.sda.productservice.model.Request;
import pl.sda.productservice.model.dto.AddRequestToUserDto;
import pl.sda.productservice.repository.AppUserRepository;
import pl.sda.productservice.repository.RequestRepository;

import java.util.Optional;

@Service
public class RequestService {

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private AppUserRepository appUserRepository;



    public Optional<Request> add(AddRequestToUserDto dto) {
        Optional<AppUser> optionalAppUser = appUserRepository.findById(dto.getAppUserId());
        if (optionalAppUser.isPresent()) {
            AppUser appUser = optionalAppUser.get();
            Request request = new Request();
            request.setLink(dto.getLink());
            request.setPrice(dto.getPrice());
            request.setQuantity(dto.getQuantity());
            request.setShopList(dto.getShopList());
            requestRepository.save(request);
            appUser.getRequestList().add(request);
            appUserRepository.save(appUser);

            return Optional.of(request);
        }
        return Optional.empty();


    }

    public Optional<Request> findById(Long requestId) {
        return requestRepository.findById(requestId);
    }
}
