package pl.sda.productservice.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AddShopDto {

    private String name;
    private String link;


}
