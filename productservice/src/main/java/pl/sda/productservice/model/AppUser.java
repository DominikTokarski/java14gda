package pl.sda.productservice.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.productservice.model.dto.CreateUserDto;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class AppUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String username;

    @Email
    private String email;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String password;

    private String name;
    private String surname;

    private boolean isAdmin;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Request> requestList;

    public static AppUser createFromDto(CreateUserDto appUserDto){
        AppUser appUser = new AppUser();
        appUser.setAdmin(appUserDto.isAdmin());
        appUser.setUsername(appUserDto.getUsername());
        appUser.setEmail(appUserDto.getEmail());
        appUser.setPassword(appUserDto.getPassword());
        return appUser;
    }

}
