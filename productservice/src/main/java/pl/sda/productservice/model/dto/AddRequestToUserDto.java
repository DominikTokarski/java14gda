package pl.sda.productservice.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.sda.productservice.model.Shop;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddRequestToUserDto {

    private Long appUserId;
    private Double quantity;
    private Double price;
    private String link;

    private List<Shop> shopList;
}
