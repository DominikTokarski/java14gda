package pl.sda.productservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.productservice.model.AppUser;
import pl.sda.productservice.model.Request;
import pl.sda.productservice.model.dto.AddRequestToUserDto;
import pl.sda.productservice.service.AppUserService;
import pl.sda.productservice.service.LoginService;
import pl.sda.productservice.service.RequestService;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/view/request")
public class RequestViewController {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private RequestService requestService;

    @GetMapping("/add")
    public String add(Model model) {
        AddRequestToUserDto addRequestToUserDto = new AddRequestToUserDto();
        model.addAttribute("added_request", addRequestToUserDto);
        return "request/add_form";
    }

    @PostMapping("/add")
    public String add(Model model, AddRequestToUserDto dto) {
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        if (optionalAppUser.isPresent()) {
            dto.setAppUserId(optionalAppUser.get().getId());
            if (dto.getAppUserId() == null || dto.getLink().isEmpty() ||
                    dto.getPrice() == null || dto.getQuantity() == null) {
                model.addAttribute("added_request", dto);
                model.addAttribute("error_message", "Some field is empty.");
                return "request/add_form";
            }
        }
        requestService.add(dto);
        return "redirect:/";
    }

    @GetMapping("/delete/{id}/{requestId}")
    public String delete(@PathVariable(name = "id") Long id,@PathVariable(name = "requestId")Long requestId) {
        Optional<AppUser> optionalAppUser = appUserService.findUserById(id);
        if (optionalAppUser.isPresent()){
            AppUser appUser = optionalAppUser.get();
            List<Request> list = appUser.getRequestList();
            Optional<Request> optionalRequest = requestService.findById(requestId);
            if (optionalRequest.isPresent()){
                Request request = optionalRequest.get();
                list.remove(request);

            }

        }
        return "user/profile/" + id;
    }
}
