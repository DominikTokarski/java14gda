package pl.sda.productservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.productservice.model.AppUser;
import pl.sda.productservice.model.Request;
import pl.sda.productservice.service.AppUserService;
import pl.sda.productservice.service.LoginService;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/view/user")
public class AppUserViewController {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/profile")
    public String viewProfile(Model model){
        Optional<AppUser> appUserOptional = loginService.getLoggedInUser();
        if (!appUserOptional.isPresent()){
            return "redirect:/";
        }
        AppUser appUser = appUserOptional.get();
        List<Request> list = appUser.getRequestList();
        model.addAttribute("logged",appUser);
        model.addAttribute("user", appUser);
        model.addAttribute("request_list",list);
        return "user/profile";
    }

    @GetMapping("/profile/{id}/")
    public String viewProfile(Model model, @PathVariable(name = "id")Long id){
        Optional<AppUser> optionalAppUser = loginService.getLoggedInUser();
        Optional<AppUser> appUserOptional = appUserService.findUserById(id);
        if (!appUserOptional.isPresent()){
            return "redirect:/";
        }
        AppUser user = optionalAppUser.get();
        AppUser appUser = appUserOptional.get();
        List<Request> list = appUser.getRequestList();
        model.addAttribute("logged",user);
        model.addAttribute("user", appUser);
        model.addAttribute("request_list",list);
        return "user/profile";
    }


    @GetMapping("/all")
    public String getAll(Model model){
        List<AppUser> list = appUserService.getAll();
        model.addAttribute("user_list",list);
        return "/user/list";
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        appUserService.delete(id);
        return "/user/list";
    }


}
