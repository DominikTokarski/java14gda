package pl.sda.productservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.productservice.model.Shop;
import pl.sda.productservice.model.dto.AddShopDto;
import pl.sda.productservice.service.ShopService;

import java.util.List;


@Controller
@RequestMapping("/view/shop")
public class ShopViewController {

    @Autowired
    private ShopService shopService;

    @GetMapping("/all")
    public String getAll(Model model){
        List<Shop> list = shopService.getAll();
        model.addAttribute("shop_list",list);
        return "/shop/list";
    }
    @GetMapping("/add")
    public String add(Model model) {
        AddShopDto addShopDto = new AddShopDto();
        model.addAttribute("added_request", addShopDto);
        return "shop/add_form";
    }

    @PostMapping("/add")
    public String add(Model model, AddShopDto dto) {
            if (dto.getLink().isEmpty() || dto.getName().isEmpty()) {
                model.addAttribute("added_shop", dto);
                model.addAttribute("error_message", "Some field is empty.");
                return "shop/add_form";
            }

        shopService.add(dto);
        return "shop/list";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable(name = "id") Long id) {
        shopService.delete(id);
        return "/user/list";
    }


}
