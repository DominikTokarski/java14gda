package pl.sda.productservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.productservice.model.AppUser;
import pl.sda.productservice.model.dto.CreateUserDto;
import pl.sda.productservice.service.AppUserService;
import pl.sda.productservice.service.LoginService;

import java.util.Optional;

@Controller
public class ClientRegisterController {

    @Autowired
    private AppUserService appUserService;

    @Autowired
    private LoginService loginService;

    @GetMapping("/add")
    public String add(Model model) {
        CreateUserDto createUserDto = new CreateUserDto();
        model.addAttribute("added_user", createUserDto);
        return "user/register_form";
    }

    @PostMapping("/add")
    public String add(Model model, CreateUserDto dto) {
        if (dto.getUsername() == null || dto.getEmail().isEmpty() || dto.getPassword() == null) {
            model.addAttribute("added_user", dto);
            model.addAttribute("error_message", "Some field is empty.");
            return "user/register_form";
        }
        appUserService.addUser(dto);
        return "redirect:/";
    }

    @GetMapping("/login")
    public String getLoginView(){
        return "user/login_form";
    }

    @GetMapping("/")
    public String getIndex(Model model){
        Optional<AppUser> appUserOptional = loginService.getLoggedInUser();
        if (appUserOptional.isPresent()){
            AppUser appUser = appUserOptional.get();
            model.addAttribute("username",appUser.getUsername());
        }
        return "/index";
    }
}
