package pl.sda.onp;

import pl.sda.algorytmy.stosy.StosOnList;


public class ONP {
    private StosOnList stack;

    public Object solveONP(String onp) {
        String[] chars = onp.split(" ");
        StosOnList integers = new StosOnList();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i].equals("/")) {
                int a = (Integer) stack.pop();
                int b = (Integer) stack.pop();
                a = a / b;
                stack.push(a);
            } else if (chars[i].equals("*")) {
                int a = (Integer) stack.pop();
                int b = (Integer) stack.pop();
                a = a * b;
                stack.push(a);
            } else if (chars[i].equals("+")) {
                int a = (Integer) stack.pop();
                int b = (Integer) stack.pop();
                a = a + b;
                stack.push(a);
            } else if (chars[i].equals("-")) {
                int a = (Integer) stack.pop();
                int b = (Integer) stack.pop();
                a = a - b;
                stack.push(a);
            } else {
                int nr = Integer.parseInt(chars[i]);
                integers.push(nr);
            }
        }
        return stack.pop();
    }

}
