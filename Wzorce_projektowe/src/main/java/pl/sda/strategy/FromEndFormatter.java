package pl.sda.strategy;

import java.awt.*;

public class FromEndFormatter implements IFormatter{
    public String format(String text){
        char[] chars = text.toCharArray();
        StringBuilder temp = new StringBuilder();
        for (int i = chars.length -1; i > 0; i--) {
            temp.append(chars[i]);
        }
        return temp.toString();
    }
}
