package pl.sda.strategy;

public class CapitalFormatter implements IFormatter{

    public String format(String text) {
        final int capitalLetterIndex = 0;
        final int restOfWordStartIndex = 1;
        String[] strings = text.split(" ");
        text = "";
        for (int i = 0; i < strings.length; i++) {
            if (!strings[i].equals("")) {
                char temp = strings[i].toUpperCase().charAt(capitalLetterIndex);
                strings[i] = temp + strings[i].substring(restOfWordStartIndex);
                text = text + " " + strings[i];
            }
        }
        return text;
    }
}
