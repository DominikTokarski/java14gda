package pl.sda.strategy;

public enum FormattingType {
    TOUPPER,
    TOLOWER,
    TRIM,
    CLEAN,
    CAPITAL,
    TOASCII,
    FROMEND;

}
