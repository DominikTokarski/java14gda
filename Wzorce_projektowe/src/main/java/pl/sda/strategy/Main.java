package pl.sda.strategy;

public class Main {
    public static void main(String[] args) {
        final String text = "nowe przygody gerwanta";
        StringFormat(FormattingType.CAPITAL,text);

    }

    private static String StringFormat(FormattingType type, String text) {
        switch (type) {
            case TOUPPER:
                return new ToUpperFormatter().format(text);
            case TOLOWER:
                return new ToLowerFormatter().format(text);
            case TRIM:
                return new TrimFormatter().format(text);
            case CLEAN:
                return new CleanFormatter().format(text);
            case CAPITAL:
                return new CapitalFormatter().format(text);
            case TOASCII:
                return new ToAsciiFormatter().format(text);
            case FROMEND:
                return new FromEndFormatter().format(text);
            default:
                return "";
        }

    }


}
