package pl.sda.strategy;

public class TrimFormatter implements IFormatter {
    public String format(String text) {
        return text.trim();
    }
}
