package pl.sda.strategy;

public interface IFormatter {
    String format(String text);
}
