package pl.sda.strategy;

public class ToAsciiFormatter implements IFormatter{
    public String format(String text) {
        char[] chars = text.toCharArray();
        String temp = "";
        int[] ascii = new int[text.length()];
        for (int i = 0; i < text.length(); i++) {
            ascii[i] = chars[i];
            temp = temp + " " + ascii[i];
        }
        return temp;
    }
}
