package pl.sda.strategy;

public class ToLowerFormatter implements IFormatter{
    public String format(String text) {
        return text.toLowerCase();
    }
}
