package pl.sda.strategy;

public class ToUpperFormatter implements IFormatter{

    public String format(String text) {
        return text.toUpperCase();
    }
}
