package pl.sda.adapter;

public class Warrior {
    public void useSword(){
        System.out.println("Sword Swing");
    }

    public void riseShield(){
        System.out.println("Shields Up");
    }

    public void goTheChicks(){
        System.out.println("Get Some Princess");
    }

    public void layDrunk(){
        System.out.println("Get Drunk");
    }

    public void pickUpArtifact(){
        System.out.println("Get Artifact");
    }
}
