package pl.sda.adapter;

import java.util.Arrays;
import java.util.Iterator;

public class Hero {
    private Fighter[] army;
    private Iterator<Fighter> iterator;

    public Hero(int sizeOfArmy) {
        army = new Fighter[sizeOfArmy];
        for (int i = 0; i < sizeOfArmy ; i++) {
            if (i < sizeOfArmy/2){
                army[i] = new WizzardAdapter();
            }else {
                army[i] = new WarriorAdapter();
            }
        }
        iterator = Arrays.asList(army).iterator();
    }

    public void chrge(){
        for (Fighter unit: army){
            unit.attack();
        }
    }

    public void allMagesCastSpell(){
        for (Fighter unit: army){
            if (unit instanceof WarriorAdapter){
                unit.attack();
            }
        }
    }

    public void sendUnit(){

        if (iterator.hasNext()){
            iterator.next().attack();
        }
    }
}
