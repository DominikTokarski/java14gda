package pl.sda.adapter;

public class Wizzard {
    public void castDestructionSpell(){
        System.out.println("Kula Ognia");
    }

    public void shield(){
        System.out.println("Tarcza Magii");
    }

    public void portal(){
        System.out.println("Portal Wymiarów");
    }
}
