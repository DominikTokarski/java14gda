package pl.sda.adapter;

public class WarriorAdapter implements Fighter{
    private final Warrior warrior;

    public WarriorAdapter() {
        this.warrior = new Warrior();
    }

    public void attack() {
        warrior.useSword();
        System.out.println("Worrior need to cooldown");
    }

    public void defend() {
        warrior.riseShield();
        System.out.println("Shield are broken");
        escape();
    }

    public void escape() {
        warrior.pickUpArtifact();
        warrior.goTheChicks();
        warrior.layDrunk();
    }


}
