package pl.sda.adapter;

public class WizzardAdapter implements Fighter {
    private final Wizzard wizzard;

    public WizzardAdapter(){
        this.wizzard = new Wizzard();
    }

    public void attack() {
        this.wizzard.castDestructionSpell();
    }

    public void defend() {
        this.wizzard.shield();
    }

    public void escape() {
        this.wizzard.portal();
    }
}
