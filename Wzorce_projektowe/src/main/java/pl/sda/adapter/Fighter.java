package pl.sda.adapter;

public interface Fighter {
    void attack();
    void defend();
    void escape();
}
