package structures;

import java.util.ArrayList;
import java.util.List;

public class MyHashMap<K, V> {
    //Początkowy rozmiar mapy
    private int mapSize = 10000;

    //Struktura przechowująca wszystkie wpisy w naszej mapie
    private List<MyEntry<K,V>> entries;

    public MyHashMap() {
        //inicjalizujemy strukturę i wypełniamy nullami,
        //w przeciwnym wypadku nie moglibyśmy dostać się do indexu większego od 0
        //czyli nasze haszowanie nie ma sensu
        entries = new ArrayList<MyEntry<K, V>>(mapSize);
        for(int i = 0; i < mapSize; i++) {
            entries.add(null);
        }
    }

    public void put(K key, V value) {
        Pair<K, V> pair = new Pair<K, V>(key, value);
        //operacja haszująca, hashCode() zwraca obliczoną wartość wg. klucza,
        //modulo dodane, aby nie wyjść po za zakres listy
        int hashCode = key.hashCode() % mapSize;
        //pobieramy wpis z listy, w celu późniejszej weryfikacji
        MyEntry<K,V> entry = entries.get(hashCode);
        //jeżeli nie znaleziono wpisu tworzymy nowy, dodajemy do niego Pair
        // i wrzucamy wpis do entries w miejsce wskazane przez hashCode
        if(entry == null) {
            MyEntry<K, V> newEntry = new MyEntry<K, V>();
            newEntry.add(pair);
            entries.set(hashCode, newEntry);
        } else if (entry.getPairs().size() == 1) { //jeżeli jednak znaleźliśmy wpis, ale liczba par == 1 to nadpisujemy jej wartość
            entry.getPairs().get(0).setValue(value);
        } else { //jeżeli par jest więcej, sprawdzamy każda porównując klucz. Jeżeli jakiś klucz się zgadza to znaczy,
                //że był już wcześniej dodany do mapy i nadpisujemy wartość w parze,
                //jeżeli żaden klucz się nie zgadza z wprowadzanym to dodajemy parę do wpisu
            for(Pair foundPair : entry.getPairs()) {
                if(foundPair.getKey().equals(pair.getKey())) {
                    foundPair.setValue(pair.getValue());
                    return;
                }
            }
            entry.add(pair);
        }
    }

    public V get(K key) {
        //operacja haszująca, hashCode() zwraca obliczoną wartość wg. klucza,
        //modulo dodane, aby nie wyjść po za zakres listy
        int hashCode = key.hashCode() % mapSize;
        MyEntry<K, V> entry = entries.get(hashCode);
        V value = null;
        if (entry != null) {
            //jeżeli znaleźliśmy wpis, i liczba par == 1 to pobieramy jedyną parę z wpisu i zwracamy wartość (value)
            if (entry.getPairs().size() == 1) {
                value = entry.getPairs().get(0).getValue();
            } else {
                // jeżeli jest ich więcej, iterujemy po parach tak długo aż klucz którejś z par będzie równy kluczowi pary wprowadzanej
                // i zwracamy wartość tej pary, jeżeli żadna z par się nie pokryje zwrócona zostanie wartość domyślna czyli null
                for (Pair<K, V> foundPair : entry.getPairs()) {
                    if (foundPair.getKey().equals(key)) {
                        value = foundPair.getValue();
                    }
                }
            }
        }
        return value;
    }

    public void remove(K key) {
        //operacja haszująca, hashCode() zwraca obliczoną wartość wg. klucza,
        //modulo dodane, aby nie wyjść po za zakres listy
        int hashCode = key.hashCode() % mapSize;
        //pobieramy wpis z listy, w celu późniejszej weryfikacji
        MyEntry<K,V> myEntry = entries.get(hashCode);
        //jeżeli znaleziono wpis, sprawdzamy jego pary i usuwamy parę jeżeli klucz którejś z nich jest taki sam jak klucz wprowadzany,
        //Po usunięciu weryfikujemy czy wpis posiada jeszcze jakieś pary, jeżeli nie to też go usuwamy
        if(myEntry != null) {
            for(Pair pair : myEntry.getPairs()) {
                if(pair.getKey().equals(key)) {
                    myEntry.getPairs().remove(pair);
                    if(myEntry.getPairs().isEmpty()) {
                        entries.set(hashCode, null);
                    }
                }
            }
        }
    }

    public MyHashMap<K, V> copyInto(MyHashMap<K, V> copiedMap) {
         for(MyEntry<K,V> entry : copiedMap.getEntries()) {
             if(entry == null) {
                 continue;
             }
             for(Pair<K,V> pair : entry.getPairs()) {
                 this.put(pair.getKey(), pair.getValue());
             }
         }
        return this;
    }

    public List<MyEntry<K, V>> getEntries() {
        return entries;
    }

    public void putIfAbsent(K key, V value) {
        if (get(key) == null)
            put(key, value);
       /* int hashCode = key.hashCode() % mapSize;
        MyEntry<K, V> myEntry = entries.get(hashCode);
        Pair<K, V> newPair = new Pair<>(key, value);
        if (myEntry == null) {
            //put(newPair.getKey(), newPair.getValue());

            MyEntry<K, V> newEntry = new MyEntry<>();
            newEntry.add(newPair);
            entries.set(hashCode, newEntry);
        } else {
            for (Pair<K, V> pair : myEntry.getPairs()) {
                if (pair.getKey().equals(newPair.getKey())) {
                    return;
                }
            }
        }*/
    }
}
