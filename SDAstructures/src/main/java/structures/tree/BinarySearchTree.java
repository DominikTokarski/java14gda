package structures.tree;

public class BinarySearchTree {
    private TreeNode root = null;

    public void add(int value) {
        root = add(value, root);
    }

    private TreeNode add(int value, TreeNode currentNode) {
        if(currentNode == null) {
            currentNode = new TreeNode(value);
        }

        if(currentNode.getValue() > value) {
            currentNode.setLeft(add(value, currentNode.getLeft()));
        } else if(currentNode.getValue() < value ) {
            currentNode.setRight(add(value, currentNode.getRight()));
        }
        return currentNode;
    }
}
