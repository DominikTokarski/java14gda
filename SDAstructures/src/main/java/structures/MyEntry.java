package structures;

import java.util.LinkedList;

public class MyEntry<K, V> {
    private LinkedList<Pair<K,V>> pairs;

    public LinkedList<Pair<K, V>> getPairs() {
        return pairs;
    }

    public void setPairs(LinkedList<Pair<K,V>> pairs) {
        this.pairs = pairs;
    }

    public void add(Pair<K,V> pair) {
        if(pairs == null) {
            pairs = new LinkedList<Pair<K,V>>();
        }
        pairs.add(pair);
    }


}
