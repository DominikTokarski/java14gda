package structures;

public class MyArrayQueue<E> {

    private E[] queue;

    private int queueSize = 0;

    private int pointer = -1;

    public MyArrayQueue(int queueSize) {
        this.queueSize = queueSize;
        queue = (E[]) new Object[queueSize];
    }

    public void enqueue(E element) {
        if(pointer < queueSize) {
            pointer++;
            queue[pointer] = element;
        } else {
            queue = copyQueue();
            enqueue(element);
        }
    }

    public E dequeue() {
        E returnedValue = queue[0];
        if(pointer == -1) {
            returnedValue = null;
            System.out.println("Brak elementów w kolejce!");
        }
        if(pointer == 0) {
            pointer--;
        }
        if(pointer > 0) {
            for(int i = 0; i < queue.length - 1; i++) {
                queue[i] = queue[i+1];
            }
            pointer--;
        }
        return returnedValue;
    }

    private E[] copyQueue() {
        E[] temp = (E[]) new Object[this.queue.length * 2];
        queueSize = temp.length;
        for(int i = 0; i < queue.length; i++) {
            temp[i] = queue[i];
        }
        return temp;
    }
}
