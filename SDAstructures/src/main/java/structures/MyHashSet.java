package structures;

public class MyHashSet<E> {

    private static final int MAX_SIZE = 5000;
    private E[] elements;

    public MyHashSet() {
        this.elements = (E[]) new Object[MAX_SIZE];
    }

    public void add(E element) {
        int hashCode = Math.abs(element.hashCode() % elements.length);
        if(elements[hashCode] == null) {
            elements[hashCode] = element;
        } else {
            int index = hashCode;
            while(elements[index] != null) {
                index++;
                if(index == elements.length) {
                    index = 0;
                } else if (index == hashCode){
                    resizeArray();
                    add(element);
                }
            }
            elements[index] = element;
        }
    }

    private void resizeArray() {
        E[] resizedArray = (E[])new Object[elements.length * 2];
        for(int i = 0; i < elements.length; i++) {
            resizedArray[i] = elements[i];
        }
        elements = resizedArray;
    }

    public boolean remove(E element) {
        int hashcode = Math.abs(element.hashCode() % elements.length);
        if(elements[hashcode] == null) {
            return false;
        } else if(elements[hashcode] == element) {
            elements[hashcode] = null;
            return true;
        } else {
            int index = hashcode;
            while(elements[index] != null) {
                index++;
                if(index == elements.length) {
                    index = 0;
                } else if (index == hashcode){
                    return false;
                }
            }
            elements[index] = null;
            return  true;
        }
    }

    public boolean contains(E element) {
        int hashCode = Math.abs(element.hashCode() % elements.length);
        if(elements[hashCode] == null) {
            return false;
        } else if(elements[hashCode] == element) {
            return true;
        } else {
            int index = hashCode;
            while (elements[index] != null) {
                index++;
                if (index == elements.length) {
                    index = 0;
                } else if (index == hashCode) {
                    return false;
                }
            }
            return true;
        }
    }

}
