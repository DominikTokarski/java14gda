package pl.sda.interfejs;

public class Main {
    public static void main(String[] args) {
        Gitara gitara = new Gitara();
        gitara.graj();
        Pianino pianino = new Pianino();
        pianino.graj();
        Beben beben = new Beben();
        beben.graj();
    }
}
