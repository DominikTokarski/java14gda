package pl.sda.sortowanie;

public class Student implements Comparable {
    String imie;
    String nazwisko;
    int nrAlbumu;

    public Student(String imie, String nazwisko, int nrAlbumu) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrAlbumu = nrAlbumu;
    }


    @Override
    public int compareTo(Object o) {
        return ((Student)o).nrAlbumu - this.nrAlbumu;
    }

    @Override
    public String toString() {
        return "Student{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", nrAlbumu=" + nrAlbumu +
                '}';
    }
}
