package pl.sda.sortowanie;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student student = new Student("Adam","Radek",111);
        Student student1 = new Student("Cezary","Cezary",94);
        Student student2 = new Student("Alicja","Nowak",184);
        Student[] studenci = {student,student1,student2};
        System.out.println(Arrays.toString(studenci));
        Arrays.sort(studenci);
        System.out.println(Arrays.toString(studenci));
    }
}
