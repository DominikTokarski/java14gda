package pl.sda.rodzina;

public class Matka implements CzlonekRodziny {
    String imie;

    public Matka(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("i am mother, my name is " + imie);
    }

    @Override
    public boolean jestDorosły() {
        return true;
    }
}
