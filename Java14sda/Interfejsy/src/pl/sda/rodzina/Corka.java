package pl.sda.rodzina;

public class Corka implements CzlonekRodziny {
    String imie;

    public Corka(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("i am doughter ;) my name is " + imie);
    }

    @Override
    public boolean jestDorosły() {
        return false;
    }
}
