package pl.sda.rodzina;

public class Syn implements CzlonekRodziny {
    String imie;

    public Syn(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("who's asking ? my name is " + imie);
    }

    @Override
    public boolean jestDorosły() {
        return false;
    }
}
