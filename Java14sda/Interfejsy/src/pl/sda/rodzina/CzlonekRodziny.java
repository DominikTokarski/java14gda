package pl.sda.rodzina;

public interface CzlonekRodziny {
    void przedstawSie();
    boolean jestDorosły();
}
