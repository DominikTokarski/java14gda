package pl.sda.rodzina;

public class Main {
    public static void main(String[] args) {
        Matka matka = new Matka("Joanna");
        Ojciec ojciec = new Ojciec("Zenon");
        Corka corka = new Corka("Marta");
        Syn syn = new Syn("Maksymilian");
        matka.przedstawSie();
        matka.jestDorosły();
        ojciec.przedstawSie();
        ojciec.jestDorosły();
        corka.przedstawSie();
        corka.jestDorosły();
        syn.przedstawSie();
        syn.jestDorosły();
    }
}
