package pl.sda.rodzina;

public class Ojciec implements CzlonekRodziny {
    String imie;

    public Ojciec(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("i am father, my name is " + imie);
    }

    @Override
    public boolean jestDorosły() {
        return true;
    }
}
