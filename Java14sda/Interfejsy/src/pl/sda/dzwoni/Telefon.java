package pl.sda.dzwoni;

import java.util.Random;

public class Telefon implements Dzwoni {
     private String numerTelefonu;
    private int czasRozmowy = 0;

    public Telefon(String numerTelefonu) {
        this.numerTelefonu = numerTelefonu;
    }

    @Override
    public void zadzwon() {
        Random random = new Random();
        int i = random.nextInt(2);

        if (i == 1){
            System.out.println("dodzwoniłęś się");
            czasRozmowy = random.nextInt(59) + 1;
            System.out.println("rozmawiałeś przez " + czasRozmowy + " minut");
        }else {
            System.out.println("nr zajety");
        }
    }

    @Override
    public void zadzwonNaNrAlarmowy() {

    }
}
