package pl.sda.jedzenie;

public class Main {
    public static void main(String[] args) {
        Krokodyl krokodyl = new Krokodyl();
        Weganin weganin = new Weganin();
        Programista programista = new Programista();
        Pokarm kurczak = new Pokarm("Kurczak",TypPokarmu.MIESO,200);
        Pokarm jogurt = new Pokarm("Jogurt",TypPokarmu.NABIAL,50);
        Pokarm truskawki = new Pokarm("Truskawiki",TypPokarmu.OWOCE,30);
        krokodyl.jedz(kurczak);
        weganin.jedz(kurczak);
        programista.jedz(kurczak);
        programista.jedz(jogurt);
        programista.jedz(truskawki);
        krokodyl.ileGramowZjedzoen();
        krokodyl.ilePosilkowZjedzone();
        weganin.ileGramowZjedzoen();
        weganin.ilePosilkowZjedzone();
        programista.ileGramowZjedzoen();
        programista.ilePosilkowZjedzone();

        Jedzacy[] jedzacies = {krokodyl,weganin,programista};

    }
}
