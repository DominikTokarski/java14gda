package pl.sda.jedzenie;

public class Krokodyl implements Jedzacy {
    int iloscGramow;
    int iloscPosilkow;

    @Override
    public void jedz(Pokarm pokarm) {
        if (pokarm.getTypPokarmu() == TypPokarmu.MIESO){
            iloscGramow = iloscPosilkow + pokarm.getWaga();
            iloscPosilkow += 1;
        }
    }

    @Override
    public int ilePosilkowZjedzone() {
        System.out.printf("Krokodyl zjadł %d posiłki",iloscPosilkow);
        System.out.println();
        return 0;
    }

    @Override
    public int ileGramowZjedzoen() {
        System.out.printf("Krokodyl zjadł %d gram",iloscGramow);
        System.out.println();
        return 0;
    }
}
