package pl.sda.jedzenie;

public class Programista implements Jedzacy {
    int iloscGramow;
    int iloscPosilkow;

    @Override
    public void jedz(Pokarm pokarm) {
        iloscGramow = iloscGramow + pokarm.getWaga();
        iloscPosilkow += 1;

    }

    @Override
    public int ilePosilkowZjedzone() {
        System.out.printf("Programista zjadł %d posiłki",iloscPosilkow);
        System.out.println();
        return 0;
    }

    @Override
    public int ileGramowZjedzoen() {
        System.out.printf("Programista zjadł %d gram",iloscGramow);
        System.out.println();
        return iloscGramow;
    }
}
