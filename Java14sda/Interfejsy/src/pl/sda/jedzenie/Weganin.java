package pl.sda.jedzenie;

public class Weganin implements Jedzacy{
  int iloscGramow;
    int iloscPosilkow;

    @Override
    public void jedz(Pokarm pokarm) {
        if (pokarm.getTypPokarmu() == TypPokarmu.OWOCE){
            iloscGramow = iloscGramow + pokarm.getWaga();
            iloscPosilkow += 1;
        }
    }

    @Override
    public int ilePosilkowZjedzone() {
        System.out.printf("Weganin zjadł %d posiłki",iloscPosilkow);
        System.out.println();
        return 0;
    }

    @Override
    public int ileGramowZjedzoen() {
        System.out.printf("Weganin zjadł %d gram",iloscGramow);
        System.out.println();
        return 0;
    }
}
