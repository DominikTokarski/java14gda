package pl.sda.jedzenie;

public interface Jedzacy {
    void jedz(Pokarm pokarm);
    int ilePosilkowZjedzone();
    int ileGramowZjedzoen();
}
