package pl.sda.chlodziarka;

public interface Grzeje {
    double pobierzTemp();
    void  zwiekszTemp();
    default void wyswietlTemp(){
        pobierzTemp();
    }
}
