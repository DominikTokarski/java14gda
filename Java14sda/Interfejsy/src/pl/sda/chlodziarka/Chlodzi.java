package pl.sda.chlodziarka;

public interface Chlodzi {
    double pobierzTemp();
    void schlodz();
    default void wyswietlTemp(){
        pobierzTemp();
    }
}
