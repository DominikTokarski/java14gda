package pl.sda.chlodziarka;

public class Wiatrak implements Chlodzi {
    private int temperatura;

    public Wiatrak(int temperatura) {
        this.temperatura = temperatura;
    }

    @Override
    public double pobierzTemp() {
        System.out.println("aktualna temperatura: " +temperatura + "c");
        return 0;
    }

    @Override
    public void schlodz() {
        temperatura = temperatura - 3;
        System.out.println("wiatrak zmniejsza temperature o 3 stopnie");

    }
}
