package pl.sda.chlodziarka;

public class Farelka implements Grzeje {
    private int temperatura;

    public Farelka(int temperatura) {
        this.temperatura = temperatura;
    }

    @Override
    public double pobierzTemp() {
        System.out.println("aktualna temperatura: " +temperatura + "c");
        return 0;
    }

    @Override
    public void zwiekszTemp() {
        temperatura = temperatura + 3;
        System.out.println("farelka zwieksza tempertaure o 3 stopnie");

    }
}
