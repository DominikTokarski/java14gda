package pl.sda.chlodziarka;

public class Main {
    public static void main(String[] args) {
      Farelka farelka = new Farelka(20);
      Wiatrak wiatrak = new Wiatrak(20);
      Klimatyzacja klimatyzacja = new Klimatyzacja(20);
      farelka.zwiekszTemp();
      farelka.zwiekszTemp();
      farelka.pobierzTemp();
      wiatrak.schlodz();
      wiatrak.pobierzTemp();
      klimatyzacja.schlodz();
      klimatyzacja.zwiekszTemp();
      klimatyzacja.zwiekszTemp();
      klimatyzacja.pobierzTemp();
      klimatyzacja.wyswietlTemp();
      farelka.wyswietlTemp();
      wiatrak.wyswietlTemp();
    }
}
