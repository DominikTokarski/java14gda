package pl.sda.chlodziarka;

public class Klimatyzacja implements Grzeje,Chlodzi {
    private int temperatura;

    public Klimatyzacja(int temperatura) {
        this.temperatura = temperatura;
    }

    @Override
    public void schlodz() {
        temperatura = temperatura - 3;
        System.out.println("klimatyzacja zmniejsza temperature o 3 stopnie");

    }

    @Override
    public double pobierzTemp() {
        System.out.println("aktualna temperatura: " +temperatura + "c");
        return 0;
    }

    @Override
    public void zwiekszTemp() {
        temperatura = temperatura + 3;
        System.out.println("klimatyzacja zwieksza tempertaure o 3 stopnie");

    }

    @Override
    public void wyswietlTemp() {
        System.out.println("aktualna temperatura: " +temperatura + "c");
    }
}
