package pl.sda.objektowosc.haslo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Gra gra = new Gra();

        while (!gra.czyKoniec()){
            System.out.println("Podaj liczbę");
            int liczba = scanner.nextInt();
            gra.czyToTenNumer(liczba);
        }
    }
}
