package pl.sda.objektowosc.haslo;

import java.util.Random;

public class Gra {
    int zgadywanaLiczba;
    int liczbaZgadniec;
    boolean czyKoniecGry;
    boolean czyZwyciestwo;

    public Gra() {
        Random random = new Random();
        zgadywanaLiczba = 1+random.nextInt(5);
    }

    public boolean czyToTenNumer(int numer){
        liczbaZgadniec++;
        if (liczbaZgadniec>5){
            czyKoniecGry=true;
        }
        if (numer<zgadywanaLiczba){
            System.out.println("Podana liczba jest" +
                    " mniejsza od zgadywanej");
            return false;
        }else if(numer>zgadywanaLiczba){
            System.out.println("Podana liczba jest" +
                    " większa od zgadywanej");
            return false;
        }else{
            System.out.println("Zgadłeś!");
            czyZwyciestwo = true;
            czyKoniecGry = true;
            return true;
        }
    }

    public boolean czyKoniec() {
        if (liczbaZgadniec>=5 || czyKoniecGry){
            return true;
        }
        return false;
    }
}


