package pl.sda.objektowosc.ulamki;

public class Ulamek {
    private int licznik;
    private int mianownik;

    public Ulamek(int licznik, int mianownik) {
        this.licznik = licznik;
        this.mianownik = mianownik;
    }

    public void wyswietl(){
        System.out.println(String.format("%d/%d",licznik,mianownik));
    }

    public Ulamek pomnoz(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.licznik;
        int mianownik = this.mianownik * ulamek.mianownik;

        return new Ulamek(licznik, mianownik);
    }

    public Ulamek dodaj(Ulamek ulamek){
        int mianownik = this.mianownik * ulamek.mianownik;
        int licznik = this.licznik * ulamek.mianownik + ulamek.licznik * this.mianownik;
        int dzielnik;



        return  new Ulamek(licznik,mianownik);
    }

    public Ulamek podziel(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.mianownik;
        int mianownik = this.mianownik * ulamek.licznik;

        return new Ulamek(licznik, mianownik);
    }
    public Ulamek odejmij(Ulamek ulamek) {
       int mianownik = this.mianownik * ulamek.mianownik;
       int licznik = this.licznik * ulamek.mianownik - ulamek.licznik * this.mianownik;

        return new Ulamek(licznik, mianownik);
    }
}
