package pl.sda.objektowosc.ulamki;

public class Main {
    public static void main(String[] args) {
        Ulamek pol = new Ulamek(1,2);
        pol.wyswietl();

        Ulamek cwierc = pol.pomnoz(pol);
        cwierc.wyswietl();

        Ulamek osemka = pol.pomnoz(pol).pomnoz(pol);
        osemka.wyswietl();

        Ulamek nowy = pol.dodaj(osemka);
        nowy.wyswietl();

        Ulamek nowy1 = osemka.podziel(cwierc);
        nowy1.wyswietl();

        Ulamek nowy2 = pol.odejmij(cwierc);
        nowy2.wyswietl();
    }
}
