package pl.sda.wyscig;

import java.util.Random;

public class Zawodnik {
    String imie;
    int identyfikator;
    double predkoscMinimalna;
    double predkoscMaksymalna;
    double pokonanaOdleglosc;

    public double getPokonanaOdleglosc() {
        return pokonanaOdleglosc;

    }

    public Zawodnik(String imie, int identyfikator, double predkoscMinimalna, double predkoscMaksymalna) {
        this.imie = imie;
        this.identyfikator = identyfikator;
        this.predkoscMinimalna = predkoscMinimalna;
        this.predkoscMaksymalna = predkoscMaksymalna;
    }

    void przedstawSie(){
        System.out.println(String.format("Nazywam się %s, mam numer %d " +
                "biegam z predkoscia %.2f km/h do %.2f km/h.", imie, identyfikator, predkoscMinimalna, predkoscMaksymalna));
    }

    void biegnij(){
        Random random = new Random();
        double predkosc = predkoscMinimalna + random.nextDouble()*(predkoscMaksymalna - predkoscMinimalna);
        pokonanaOdleglosc +=predkosc;

    }
}
