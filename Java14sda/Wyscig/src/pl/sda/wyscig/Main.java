package pl.sda.wyscig;

public class Main {
    public static void main(String[] args) {

        Zawodnik adam = new Zawodnik("Adam",1,6,16.60);

        Zawodnik piotr = new Zawodnik("Piotr",2,6,17.20);

        Zawodnik lukasz = new Zawodnik("Lukasz",3,6,17.65);

        Zawodnik[] zawody = new Zawodnik[]{adam,piotr,lukasz};

        adam.przedstawSie();

        piotr.przedstawSie();

        lukasz.przedstawSie();

        Zawodnik zwyciezca = null;

        do {
            for (Zawodnik zawodnik : zawody) {
                zawodnik.biegnij();
                if (zawodnik.pokonanaOdleglosc >= 50) {
                    zwyciezca = zawodnik;
                    break;
                }
            }
        } while (zwyciezca == null);

        System.out.println("Zwyciezyl " + zwyciezca.imie);






    }
}
