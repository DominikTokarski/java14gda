package pl.sda.bilety;

public enum  Bilet {
    ULGOWY_GODZINNY(1.60, 60),
    ULGOWY_CALODNIOWY(5.20,24*60),
    NORMALNY_GODZINNY(3.20,60),
    NORMALNY_CALODNIOWY(11.40,24*60),
    BRAK_BILETU(0,0);

    private double cena;
    private int czasWMinutach;

    Bilet(double cena, int czasWMinutach) {
        this.cena = cena;
        this.czasWMinutach = czasWMinutach;
    }

    public double pobierzCeneBiletu() {
        return cena;
    }

    public int pobierzCzasJazdy(){
        return czasWMinutach;
    }

    public void wyswietlDaneOBilecie(){
        String a = this.toString().toLowerCase().split("_")[0];
        System.out.println(a + " " + this.pobierzCzasJazdy()/60 +"-godzinny");
    }

    public static Bilet kupBilet(int wiek, int czasWMinutach, double kwota){
        Bilet bilet;
        if (wiek <= 18 || wiek >= 65){
            if (czasWMinutach >= 60){
                bilet = Bilet.ULGOWY_CALODNIOWY;

            }else{
                bilet = Bilet.ULGOWY_GODZINNY;
            }
        }else {
            if (czasWMinutach >=60){
                bilet = Bilet.NORMALNY_CALODNIOWY;
            }else{
                bilet = Bilet.NORMALNY_GODZINNY;
            }
        }
        if(bilet.pobierzCeneBiletu() <= kwota){
            return bilet;
        }
        return Bilet.BRAK_BILETU;
    }
}
