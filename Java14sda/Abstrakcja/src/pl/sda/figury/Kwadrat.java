package pl.sda.figury;

public class Kwadrat extends Prostokat {

    public Kwadrat(double a) {
        super(a, a);
    }

    @Override
    public String toString() {
        return String.format("Kwadat o boku %.2f", a);
    }
}
