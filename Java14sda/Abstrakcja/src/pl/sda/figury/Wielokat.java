package pl.sda.figury;

public abstract class Wielokat extends Figura {
    private double [] boki;

    public Wielokat(double[] boki) {
        this.boki = boki;
    }

    @Override
    public double obliczObwod() {
        double obwod = 0;
        for (double bok:boki){
            obwod += bok;
        }
        return obwod;
    }
}
