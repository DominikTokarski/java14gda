package pl.sda.figury;

public abstract class Figura {

    public abstract double obliczObwod();
    public abstract double obliczPole();

}
