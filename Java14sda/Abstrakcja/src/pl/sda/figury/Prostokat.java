package pl.sda.figury;

public class Prostokat extends Czworokat {
    protected double a,b;

    public Prostokat(double a, double b) {
        super(a, b, a, b);
        this.a = a;
        this.b = b;
    }

    @Override
    public double obliczPole() {
        return a*b;
    }
    @Override
    public String toString() {
        return String.format("Prostokąt o bokach %.2f %.2f",a ,b);
    }
}
