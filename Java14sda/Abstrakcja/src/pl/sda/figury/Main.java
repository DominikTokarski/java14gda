package pl.sda.figury;

public class Main {
    public static void main(String[] args) {
        Okreg okreg = new Okreg(5);

        System.out.println(okreg.obliczObwod());
        System.out.println(okreg.obliczPole());

        Kwadrat kwadrat = new Kwadrat(4);
        System.out.println(kwadrat.obliczObwod());
        System.out.println(kwadrat.obliczPole());

        Figura[] figury = new Figura[]{okreg,kwadrat};
        double sumaPowierzchni = 0;
        for (Figura figura:figury){
            sumaPowierzchni += figura.obliczPole();
        }
        System.out.println(sumaPowierzchni);

        Prostokat prostokat = new Kwadrat(2.5);
        System.out.println(prostokat);



    }
}
