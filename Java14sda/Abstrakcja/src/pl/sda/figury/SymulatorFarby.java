package pl.sda.figury;

public class SymulatorFarby {

    public static int obliczZapotrebowanieNaFarbe(Figura[] ksztalty,
            double wielkoscPojemnikaNaFarbe){
        double powierzchniaKsztaltow = 0;
            for (Figura figura:ksztalty){
                powierzchniaKsztaltow += figura.obliczPole();

            }
            if (powierzchniaKsztaltow % wielkoscPojemnikaNaFarbe > 0) {
                return (int) (powierzchniaKsztaltow / wielkoscPojemnikaNaFarbe) + 1;
            }else{
                return (int) (powierzchniaKsztaltow / wielkoscPojemnikaNaFarbe);
            }

    }

    public static void main(String[] args) {
        Kwadrat kwadrat = new Kwadrat(5);
        Okreg okrag = new Okreg(10);
        Prostokat prostokat = new Prostokat(6,10);
        Figura[] figury = new Figura[]{kwadrat,okrag,prostokat};
        double wiadro = 50;
        System.out.println(obliczZapotrebowanieNaFarbe(figury,wiadro));



    }
}
