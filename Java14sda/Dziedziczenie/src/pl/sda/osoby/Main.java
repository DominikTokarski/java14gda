package pl.sda.osoby;

public class Main {
    public static void main(String[] args) {
        Osoba roman = new Osoba("Roman","Kowalski",25);
        roman.przedstawSie();

        Student olek = new Student("Aleksander","Nowak",21,111);
        olek.przedstawSie();

        System.out.println(olek);

        Student bolek = new Student("Bolesław","Zieliński",35, 111);

        System.out.println(olek.equals(bolek));
        System.out.println(olek.equals("Bolesław"));
    }
}
