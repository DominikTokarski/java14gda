package pl.sda.osoby;

public class Student extends Osoba {
    private long numerIndeksu;

    public Student(String imie, String nazwisko, int wiek, long numerIndeksu) {
        super(imie, nazwisko, wiek);
        this.numerIndeksu = numerIndeksu;
    }

    @Override
    public void przedstawSie() {
        System.out.println(String.format("Cześć, jestem %s i studiuje prawo",imie));
    }

    @Override
    public String toString() {
        String opis = String.format("%s %s, nr albumu: %d",imie,nazwisko,numerIndeksu);

        return opis;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Student){
            Student ten = (Student) obj;
            if (this.numerIndeksu == ten.numerIndeksu){
                return true;
            }
        }
        return false;
    }
}
