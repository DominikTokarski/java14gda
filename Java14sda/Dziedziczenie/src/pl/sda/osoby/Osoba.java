package pl.sda.osoby;

public class Osoba {
    String imie;
    String nazwisko;
    int wiek;

    public Osoba(String imie, String nazwisko, int wiek) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
    }

    public void przedstawSie (){
        System.out.printf("Cześć nazywam się %s %s, mam %d lat\n",imie,nazwisko,wiek);
    }
}
