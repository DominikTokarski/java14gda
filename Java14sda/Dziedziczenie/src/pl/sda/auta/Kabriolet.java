package pl.sda.auta;

public class Kabriolet extends Samochod{
    boolean czyZlozonyDach = false;

    public Kabriolet(String kolor, String marka, int rocznik) {
        super(kolor, marka, rocznik);
    }

    public void schowajDach (){
        czyZlozonyDach = true;
    }

    public boolean czyDachSchowany (){
        if (czyZlozonyDach == true){
            System.out.println("Dach jest już schowany");
        }else {
            System.out.println("Dach jest zamknięty");
        }
        return false;

    }

    @Override
    public void przyspiesz() {
        if (predkosc >= 180) {
            System.out.println("Samochód nie może jechać więcej niż 180km/h");
        } else {
            predkosc += 10;
            System.out.printf("Przyspieszam do %d km/h", predkosc);
        }
    }

    @Override
    public String toString() {
        return super.toString() + " z rozsuwanym dachem";
    }
}
