package pl.sda.auta;

public class Main {
    public static void main(String[] args) {
        Samochod bmw = new Samochod("Czarny","BMW",1995);
        Kabriolet mazda = new Kabriolet("Czerwony","Mazda",2015);
        Kabriolet bmw1 = new Kabriolet("Czarny","BMW",1995);

//        for (int i = 0; i <20 ; i++) {
//            mazda.przyspiesz();
//            bmw.przyspiesz();
//
//        }

        mazda.czyDachSchowany();

        bmw1.schowajDach();
        bmw1.czyDachSchowany();

        bmw.wlaczSwiatla();
        bmw.czySwiatlaWlaczone();

        mazda.czySwiatlaWlaczone();

        System.out.println(bmw.equals(mazda));
        System.out.println(bmw.equals(bmw1));

        System.out.println(mazda.toString());
        System.out.println(bmw.toString());

    }
}
