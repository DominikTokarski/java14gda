package pl.sda.auta;

import pl.sda.osoby.Student;

public class Samochod {
    int predkosc = 0;
    boolean czyWlaczoneSwiatla = false;
    protected String kolor;
    protected String marka;
    protected int rocznik;

    public Samochod(String kolor, String marka, int rocznik) {
        this.kolor = kolor;
        this.marka = marka;
        this.rocznik = rocznik;
    }

    public void przyspiesz () {
        if (predkosc >= 120) {
            System.out.println("Samochód nie może jechać więcej niż 120km/h");
        } else {
            predkosc += 10;
            System.out.printf("Przyspieszam do %d km/h", predkosc);
        }
    }
    public void wlaczSwiatla (){
        czyWlaczoneSwiatla = true;
    }

    public boolean czySwiatlaWlaczone (){
        if (czyWlaczoneSwiatla == true){
            System.out.println("Swiatła włączone");
        }else{
            System.out.println("Swiatła wyłączone");
        }
        return false;
    }

    @Override
    public String toString() {
        return String.format("%s samochód marki %s rocznik %d",kolor,marka,rocznik);

    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Samochod) {
            Samochod ten = (Samochod) obj;
            if (this.kolor == ten.kolor && this.marka == ten.marka && this.rocznik == ten.rocznik) {
                return true;
            }
        }
        return false;
    }
}
