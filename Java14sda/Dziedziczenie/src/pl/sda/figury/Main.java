package pl.sda.figury;

public class Main {
    public static void main(String[] args) {
        Kolo male = new Kolo(2.5);
        MojeKolo duze = new MojeKolo(5,10);

        System.out.println(male.obliczPole());
        System.out.println(duze.obliczPole());
    }
}
