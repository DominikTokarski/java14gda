package pl.sda.figury;

public class Kolo {
    double promien;

    public Kolo(double promien) {
        this.promien = promien;
    }

    protected final static double PI = 3.14;

    public double obliczPole(){
      return PI * promien * promien;
    }

}
