package pl.sda.figury;

public class MojeKolo extends Kolo {
    double srednica;

    public MojeKolo(double promien, double srednica) {
        super(promien);
        this.srednica = srednica;
    }


    public double obliczPole() {
        return PI * (srednica / 2) * (srednica / 2);
    }
}
