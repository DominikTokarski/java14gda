package pl.sda.biblioteka;

import java.util.Arrays;

public abstract class Egzemplarz {
    protected Autor[] autorzy;
    protected int rokWydania;
    protected int iloscStron;

    public Egzemplarz(Autor[] autorzy, int rokWydania, int iloscStron) {
        this.autorzy = autorzy;
        this.rokWydania = rokWydania;
        this.iloscStron = iloscStron;
    }

    public abstract String pobierzTytul();

    @Override
    public String toString() {
        return String.format("%s autorstwa %s, rok %d",pobierzTytul(), Arrays.toString(autorzy), rokWydania);
    }
}
