package pl.sda.biblioteka;

public class Czlowiek {
    protected String imie;
    protected String nazwisko;

    public Czlowiek(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    @Override
    public String toString() {
        return String.format("%s %s",imie ,nazwisko);
    }
}
