package pl.sda.biblioteka;

public class Czasopismo extends Egzemplarz {
    private String nazwa;
    private int nr;

    public Czasopismo(String nazwa, int nr, Autor[] atorzy, int rokWydania, int iloscStron) {
        super(atorzy, rokWydania, iloscStron);
        this.nazwa = nazwa;
        this.nr = nr;
    }

    @Override
    public String pobierzTytul() {
        return String.format("%s %d", nazwa, nr);
    }
}
