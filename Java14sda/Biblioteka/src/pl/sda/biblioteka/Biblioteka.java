package pl.sda.biblioteka;

public class Biblioteka {
    private Egzemplarz[] zbior;

    public Biblioteka(Egzemplarz[] zbior) {
        this.zbior = zbior;
    }

    public Egzemplarz[] szukajPotytule(String tytul) {
        Egzemplarz[] znalezione = new Egzemplarz[10];
        int iloscZnalezionych = 0;

        for (Egzemplarz egzemplarz : zbior) {
            if (egzemplarz.pobierzTytul().contains(tytul)) {
                znalezione[iloscZnalezionych] = egzemplarz;
                iloscZnalezionych++;
            }

        }
        return przytnij(znalezione);
    }

    public Egzemplarz[] szukajPoAutorze(String autor) {
        Egzemplarz[] znalezione = new Egzemplarz[10];
        int iloscZnalezionych = 0;

        for (Egzemplarz egzemplarz : zbior) {
            for (Autor autor1: egzemplarz.autorzy) {
                if (autor1.toString().contains(autor)) {
                    znalezione[iloscZnalezionych] = egzemplarz;
                    iloscZnalezionych++;
                }
            }

        }
        return przytnij(znalezione);
    }

    public Egzemplarz[] szukajPoWszystkim(String fraza) {
        Egzemplarz[] znalezione = new Egzemplarz[10];
        int iloscZnalezionych = 0;

        for (Egzemplarz egzemplarz : zbior) {
            if (egzemplarz.toString().contains(fraza)) {
                znalezione[iloscZnalezionych] = egzemplarz;
                iloscZnalezionych++;
            }

        }
        return przytnij(znalezione);
    }

    private Egzemplarz[] przytnij(Egzemplarz[] pozycje) {
        int niePustePozycje = 0;
        for (Egzemplarz egzemplarz:pozycje){
            if (egzemplarz != null){
                niePustePozycje++;
        }
    }
    Egzemplarz[] przycieta = new Egzemplarz[niePustePozycje];
        for (int i = 0; i < przycieta.length; i++) {
            przycieta[i] = pozycje [i];
        }
            return przycieta;

    }

}


