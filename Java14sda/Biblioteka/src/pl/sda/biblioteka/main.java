package pl.sda.biblioteka;

import java.util.Arrays;

public class main {
    public static void main(String[] args) {
        Autor tolkien = new Autor("JRR", "Tolkien", "angielski");
        Autor sienkiewicz = new Autor("Henryk", "Sienkiewicz","polski");
        Autor prus = new Autor("Bolesław", "Prus", "polski");

        Autor[] pracaGrupowa = new Autor[] {sienkiewicz, prus};

        Ksiazka hobbit = new Ksiazka("Hobbit", new Autor[]{tolkien}, 2005, 300);
        Ksiazka lotr = new Ksiazka("Władca Pierścieni 1", new Autor[]{tolkien}, 2009, 300);
        Ksiazka lotr2 = new Ksiazka("Władca Pierścieni 2", new Autor[]{tolkien}, 2009, 300);
        Ksiazka lotr3 = new Ksiazka("Władca Pierścieni 3", new Autor[]{tolkien}, 2009, 300);

        Ksiazka krzyzacy = new Ksiazka("Krzyżacy", new Autor[]{sienkiewicz}, 1987, 200);
        Czasopismo ogrodnik = new Czasopismo("Ogrodnik", 54, pracaGrupowa, 2000, 30);

        Egzemplarz[] zbior = new Egzemplarz[]{hobbit, lotr, lotr2, lotr3, krzyzacy, ogrodnik};

        Biblioteka biblioteka = new Biblioteka(zbior);

        Egzemplarz[] znalezione = biblioteka.szukajPotytule("Krzyż");
        System.out.println(Arrays.toString(znalezione));

        System.out.println();

        znalezione = biblioteka.szukajPoAutorze("Bolesław");
        System.out.println(Arrays.toString(znalezione));

        System.out.println();

        znalezione = biblioteka.szukajPoWszystkim("Henryk");
        System.out.println(Arrays.toString(znalezione));

    }
}
