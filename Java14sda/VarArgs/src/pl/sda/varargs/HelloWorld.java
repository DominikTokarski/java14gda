package pl.sda.varargs;

public class HelloWorld {
    public static void main(String[] args) {
    hello();
    hello("Ania", "Jacek","Krzysiek");
    }

    public static void hello(String... imiona){
        for (String imie:imiona){
            System.out.println("Cześć "+ imie);
        }
    }
}
