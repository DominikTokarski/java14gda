package pl.sda.varargs;

public class Kalkulator {
    public int dodaj(int... args){
        int suma = 0;
        for (int liczba:args) {
            suma += liczba;
        }
        return suma;
    }
    public int odejmij(int... args){
        int suma = 0;
        for (int i = 2; i < args.length; i++) {
            suma -= args[i];
        }
        return suma;
    }

}
