package pl.sda.obiektowosc.bank;

public class KontoBankowe {
    private long numerKonta;
    private int stanKonta;

    public long getNumerKonta() {
        return numerKonta;
    }

    public KontoBankowe(long numerKonta, int stanKonta) {
        this.numerKonta = numerKonta;
        this.stanKonta = stanKonta;
    }

    public void wyswietlStanKonta(){
        System.out.println("Stank konta #" + numerKonta
        + " wynosi " + stanKonta);
    }

    public void wplacSrodki (int kwota){
        stanKonta += kwota;
    }

    public int pobierzSrodki(int kwota){
        if (kwota > stanKonta){
            return 0;
        }else {
            stanKonta -= kwota;
            return kwota;
        }
    }
}
