package pl.sda.obiektowosc.bohaterowie;

public class SuperBohater {
    String nazwa;
    String supermoc;

    public SuperBohater(String nazwa, String supermoc) {
        this.nazwa = nazwa;
        this.supermoc = supermoc;
    }

    public static void main(String[] args) {
        SuperBohater Batman = new SuperBohater("Batman","Pieniądze");
        SuperBohater Superman = new SuperBohater("Superman", "Laser z oczu");
        SuperBohater ZielonaLatarnia = new SuperBohater("Zielona Latarnia", "Zielone Pole");

//        Batman = Superman; Batman = null;
//        System.out.println(Superman == null);

        Batman = null; Superman = Batman; Batman = ZielonaLatarnia;
        System.out.println(Batman == null);
        System.out.println(Superman == null);
        System.out.println(ZielonaLatarnia == null);

    }
}

