package pl.sda.obiektowosc.sale;

public class Main {
    public static void main(String[] args) {
        Sala gdansk = new Sala();
        gdansk.nazwa = "Gdańsk";
        gdansk.iloscm2 = 50.05;
        gdansk.liczbastanowisk = 8;
        gdansk.czyjesrzutnik = true;

        Sala sztukaW = new Sala();
        sztukaW.nazwa = "Java";
        sztukaW.iloscm2 = 45;
        sztukaW.liczbastanowisk = 12;
        sztukaW.czyjesrzutnik = false;

        Sala nowa = new Sala("Nowa", 100.02,
                30, false);

        Sala[] zarzadzaneSale = new Sala[]{gdansk,sztukaW,nowa};

        Menadzer jan = new Menadzer();
        jan.imie = "Jan";
        jan.sale = zarzadzaneSale;

        jan.wyswietlDostepneSale();
        jan.zabookujSale("Java");
        jan.wyswietlDostepneSale();
    }



}
