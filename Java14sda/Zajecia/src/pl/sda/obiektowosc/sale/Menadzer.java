package pl.sda.obiektowosc.sale;

public class Menadzer {
    String imie;
    Sala[] sale;

    void wyswietlDostepneSale(){
        System.out.println("#Dostępne sale :");
        for (Sala sala:sale){
            if (sala.czyjestwolna){
                sala.wyswietlopisSali();
            }
        }
    }

    boolean zabookujSale(String nazwaSali){
        for (Sala sala:sale){
            if (sala.nazwa.equals(nazwaSali) && sala.czyjestwolna){
                sala.czyjestwolna=false;
                return true;
            }
        }
        return false;
    }
}
