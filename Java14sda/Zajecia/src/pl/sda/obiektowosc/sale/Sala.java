package pl.sda.obiektowosc.sale;

public class Sala {
    String nazwa;
    double iloscm2;
    int liczbastanowisk;
    boolean czyjesrzutnik;
    boolean czyjestwolna = true;

    public Sala(String nazwa, double iloscm2, int liczbastanowisk, boolean czyjesrzutnik) {
        this.nazwa = nazwa;
        this.iloscm2 = iloscm2;
        this.liczbastanowisk = liczbastanowisk;
        this.czyjesrzutnik = czyjesrzutnik;
    }

    public Sala(){

    }

    void wyswietlopisSali(){
        String opis = String.format("Sala %s o pow. %.2f, " +
                "liczba stanowis: %d",nazwa,iloscm2,liczbastanowisk);
        if (czyjesrzutnik){
            opis +=", sala posiada rzutnik.";
        }else {
            opis +=", sala nie posiada rzutnika.";
        }
        System.out.println(opis);
    }

}


