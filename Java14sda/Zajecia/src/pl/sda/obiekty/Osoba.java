package pl.sda.obiekty;

import java.util.Date;

public class Osoba {
    String imie;
    int rokUrodzenia;

    // Początek1stworzenie Konstruktora --> public Osoba --> klawisze Alt + fn +  Insert
    public Osoba(String imie, int rokUrodzenia) {
        this.imie = imie;
        this.rokUrodzenia = rokUrodzenia;
    }

    public Osoba() {
    }
    // Koniec_1

    public static void main(String[] args) {

        Osoba Ania = new Osoba();
        Ania.imie = "Ania";
        Ania.rokUrodzenia = 25;

        Osoba Andrzej = new Osoba();
        Andrzej.imie = "Andrzej";
        Andrzej.rokUrodzenia = 54;

        Osoba Mariola = new Osoba();
        Mariola.imie = "Mariola";
        Mariola.rokUrodzenia = 68;

        //dodanie 3 nowych osób:
        Osoba osoba1 = new Osoba("Dominik", 24);
        Osoba osoba2 = new Osoba("Aleksandra", 28);
        Osoba osoba3 = new Osoba("Ania", 28);

        //umieszczenie osób w tablicy:
        int[] tablica = new int[100];
        Osoba[] tablicaOsoba = new Osoba[]{osoba1, osoba2, osoba3};

        //wyświetlenie wszystkich osób w pętli
        System.out.println("wszyscy");                 // zaznacz od System.put.println Do zrobienie metody Ctrl Alt M
        for (Osoba osoba : tablicaOsoba) {
            osoba.przedstawSie();
        }

        wyswietlTylkoPanie(tablicaOsoba);

        wyswietlPanow(tablicaOsoba);
    }

    private static void wyswietlTylkoPanie(Osoba[] tablicaOsoba) {
        System.out.println("panie");
        for (Osoba osoba : tablicaOsoba) {
            if (osoba.imie.endsWith("a")) {
                osoba.przedstawSie();
            }
        }
    }

    private static void wyswietlPanow(Osoba[] tablicaOsoba) {
        System.out.println("panowie");
        for (Osoba osoba : tablicaOsoba) {
            if (!osoba.imie.endsWith("a")) {
                osoba.przedstawSie();
            }
        }
    }

    private void przedstawSie() {
        System.out.println(String.format("Mam na imię %s i mam %d lat. \n", imie, rokUrodzenia));
    }

}