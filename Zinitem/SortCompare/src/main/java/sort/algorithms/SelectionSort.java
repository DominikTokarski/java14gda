package sort.algorithms;

import java.util.ArrayList;
import java.util.List;

public class SelectionSort implements SortAlgorithm {
    public List<Integer> sort(List<Integer> notSorted) {
        List<Integer> sorted = new ArrayList<Integer>();
        sorted.addAll(notSorted);

        for(int j = 0; j < sorted.size(); j++) {
            int min = j;
            for(int i = j + 1; i < sorted.size(); i++) {
                if(sorted.get(i) < sorted.get(min)) {
                    min = i;
                    int temp = sorted.get(j);
                    sorted.set(j, sorted.get(min));
                    sorted.set(min, temp);
                }
            }
        }
        return sorted;
    }
}
