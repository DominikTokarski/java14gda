package sort.algorithms;

import pl.sda.algorytmy.drzewa.Heap;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SortingComparator {
    private int TEST_UNITS = 50000;
    public void compareSortingAlgorithms() {
        List<Integer> randomValues = generateRandomValues();
        System.out.println("--------- BUBBLE SORT ---------");
        System.out.println("Time start: " + getActualTime());
        new BubbleSort().sort(randomValues);
        System.out.println("Time end: " + getActualTime());

        System.out.println("--------- INSERT SORT ---------");
        System.out.println("Time start: " + getActualTime());
        new InsertionSort().sort(randomValues);
        System.out.println("Time end: " + getActualTime());

        System.out.println("--------- SELECTION SORT ---------");
        System.out.println("Time start: " + getActualTime());
        new SelectionSort().sort(randomValues);
        System.out.println("Time end: " + getActualTime());

        System.out.println("--------- COLLECTIONS SORT ---------");
        System.out.println("Time start: " + getActualTime());
        Collections.sort(randomValues);
        System.out.println("Time end: " + getActualTime());

        System.out.println("--------- HEAP SORT ---------");
        System.out.println("Time start: " + getActualTime());
        Rng().sort();
        System.out.println("Time end: " + getActualTime());

    }

    private List<Integer> generateRandomValues() {
        List<Integer> numbers = new ArrayList<Integer>();
        for(int i = 0; i < TEST_UNITS; i++) {
            numbers.add(new Random().nextInt(TEST_UNITS));
        }
        return numbers;
    }

    private LocalTime getActualTime() {
        return LocalTime.now();
    }

    private Heap Rng(){
        Heap heap = new Heap();
        for (int i = 0; i < TEST_UNITS; i++) {
            heap.put(new Random().nextInt(TEST_UNITS));
        }
        return heap;
    }
}
