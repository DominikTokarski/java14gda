package sort.algorithms;

import java.util.List;

public interface SortAlgorithm {
    List<Integer> sort(List<Integer> notSorted);
}
