package sort.algorithms;

import java.util.ArrayList;
import java.util.List;

public class BubbleSort implements SortAlgorithm {
    public List<Integer> sort(List<Integer> notSorted) {
        List<Integer> sorted = new ArrayList<Integer>();
        sorted.addAll(notSorted);
        for(int i = 0; i < sorted.size(); i++) {
            for(int j = 0; j < sorted.size(); j++) {
                if(sorted.get(j) > sorted.get(i)) {
                    Integer temp = sorted.get(i);
                    sorted.set(i, sorted.get(j));
                    sorted.set(j, temp);
                }
            }
        }
        return sorted;
    }
}
