package sort.algorithms;

import java.util.ArrayList;
import java.util.List;

public class InsertionSort implements SortAlgorithm {
    public List<Integer> sort(List<Integer> notSorted) {
        List<Integer> sorted = new ArrayList<Integer>();
        sorted.addAll(notSorted);
        for(int j = 1; j < sorted.size(); j++) {
            int key = sorted.get(j);
            int i = j - 1;
            while(i >= 0 && key < sorted.get(i)) {
                sorted.set(i + 1, sorted.get(i));
                i--;
            }
            i++;
            sorted.set(i, key);
        }
        return sorted;
    }
}
