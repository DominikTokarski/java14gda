import sort.algorithms.InsertionSort;
import sort.algorithms.SortingComparator;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        new SortingComparator().compareSortingAlgorithms();
    }
}
