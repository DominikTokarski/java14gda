package pl.sda.abstrakcjaKompuerowa;

public abstract class FactoryPC {

    public static AbstractPC madeSamsungC(){
        return new SamsungPC("C",COMPUTER_BRAND.SAMSUNG,
                8192,2048,true);
    }

    public static AbstractPC madeAsusForPlayers(){
        return new AsusPC("For Players",COMPUTER_BRAND.ASUS,
                4096,1024,false);
    }
}
