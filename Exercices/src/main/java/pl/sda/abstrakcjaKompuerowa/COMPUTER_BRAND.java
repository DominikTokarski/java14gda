package pl.sda.abstrakcjaKompuerowa;

public enum COMPUTER_BRAND {
    ASUS,
    HP,
    SAMSUNG,
    APPLE;
}
