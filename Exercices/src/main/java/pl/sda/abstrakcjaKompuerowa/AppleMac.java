package pl.sda.abstrakcjaKompuerowa;

public class AppleMac extends AbstractPC {

    public AppleMac(String name, COMPUTER_BRAND computer_brand,
                    int power, double graphicPower, boolean isOverclocked) {
        super(name, computer_brand, power, graphicPower, isOverclocked);
    }

    public static AbstractPC madeAppleAir(){
        return new AppleMac("AppleAir",COMPUTER_BRAND.APPLE,
                2048,1024,false);
    }

    public static AbstractPC madeAppleAirPro(){
        return new AppleMac("AppleAirPro",COMPUTER_BRAND.APPLE,
                16368,4096,false);
    }


}
