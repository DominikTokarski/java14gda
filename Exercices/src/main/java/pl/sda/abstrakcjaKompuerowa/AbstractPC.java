package pl.sda.abstrakcjaKompuerowa;

public abstract class AbstractPC {
    private String name;
    private COMPUTER_BRAND computer_brand;
    private int power;
    private double graphicPower;
    private boolean isOverclocked;

    @Override
    public String toString() {
        return "AbstractPC{" +
                "name='" + name + '\'' +
                ", computer_brand=" + computer_brand +
                ", power=" + power +
                ", graphicPower=" + graphicPower +
                ", isOverclocked=" + isOverclocked +
                '}';
    }

    public AbstractPC(String name, COMPUTER_BRAND computer_brand,
                      int power, double graphicPower, boolean isOverclocked) {
        this.name = name;
        this.computer_brand = computer_brand;
        this.power = power;
        this.graphicPower = graphicPower;
        this.isOverclocked = isOverclocked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public COMPUTER_BRAND getComputer_brand() {
        return computer_brand;
    }

    public void setComputer_brand(COMPUTER_BRAND computer_brand) {
        this.computer_brand = computer_brand;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public double getGraphicPower() {
        return graphicPower;
    }

    public void setGraphicPower(double graphicPower) {
        this.graphicPower = graphicPower;
    }

    public boolean isOverclocked() {
        return isOverclocked;
    }

    public void setOverclocked(boolean overclocked) {
        isOverclocked = overclocked;
    }
}
