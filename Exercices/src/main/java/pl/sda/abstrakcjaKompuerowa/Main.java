package pl.sda.abstrakcjaKompuerowa;

public class Main {
    public static void main(String[] args) {
        AbstractPC hp1 = HpPC.madeHPOmen();
        AbstractPC asus1 = FactoryPC.madeAsusForPlayers();
        AbstractPC apple1 = AppleMac.madeAppleAirPro();
        AbstractPC asus2 = AsusPC.madeAsusROG();
        AbstractPC samsung = SamsungPC.madeSamsungB();
        System.out.println(hp1);
        System.out.println(asus1);
        System.out.println(apple1);
        System.out.println(asus2);
        System.out.println(samsung);
    }
}
