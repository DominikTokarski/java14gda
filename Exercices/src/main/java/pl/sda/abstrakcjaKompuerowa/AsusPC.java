package pl.sda.abstrakcjaKompuerowa;

public class AsusPC extends AbstractPC {
    public AsusPC(String name, COMPUTER_BRAND computer_brand,
                  int power, double graphicPower, boolean isOverclocked) {
        super(name, computer_brand, power, graphicPower, isOverclocked);
    }

    public static AbstractPC madeAsusROG(){
        return new AsusPC("ROG",COMPUTER_BRAND.ASUS,
                16368,8192,true);
    }

    public static AbstractPC madeAsusJust(){
        return new AsusPC("Just",COMPUTER_BRAND.ASUS,
                2048,2048,false);
    }
}
