package pl.sda.abstrakcjaKompuerowa;

public class SamsungPC extends AbstractPC {
    public SamsungPC(String name, COMPUTER_BRAND computer_brand,
                     int power, double graphicPower, boolean isOverclocked) {
        super(name, computer_brand, power, graphicPower, isOverclocked);
    }

    public static AbstractPC madeSamsungA(){
        return new SamsungPC("A",COMPUTER_BRAND.SAMSUNG,
                4096,1024,false);
    }

    public static AbstractPC madeSamsungB(){
        return new SamsungPC("B",COMPUTER_BRAND.SAMSUNG,
                8192,4096,true);
    }
}
