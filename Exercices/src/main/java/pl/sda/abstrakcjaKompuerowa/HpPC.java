package pl.sda.abstrakcjaKompuerowa;

public class HpPC extends AbstractPC {
    public HpPC(String name, COMPUTER_BRAND computer_brand,
                int power, double graphicPower, boolean isOverclocked) {
        super(name, computer_brand, power, graphicPower, isOverclocked);
    }

    public static AbstractPC madeHPOmen(){
        return new HpPC("Omen",COMPUTER_BRAND.HP,
                32736,16368,true);
    }

    public static AbstractPC madeHPCasual(){
        return new HpPC("Casual",COMPUTER_BRAND.HP,
                1024,512,false);
    }
}
