package pl.sda.pizzeria;

import java.util.List;

public class Pizza {
    private List<Ingredient> ingredients;


    @Override
    public String toString() {
        return "Pizza{" +
                "ingredients=" + ingredients +
                '}';
    }

    public static class Builder {
        private Ingredient ingredient1 = new Ingredient("cake",1);
        private Ingredient ingredient2 = new Ingredient("tomato souce", 1);
        private Ingredient ingredient3 = new Ingredient("cheese",1);
        private Ingredient ingredient4;
        private Ingredient ingredient5;
        private Ingredient ingredient6;
        private Ingredient ingredient7;
        private List<Ingredient> ingredients;

        @Override
        public String toString() {
            return "Builder{" +
                    "ingredient1=" + ingredient1 +
                    ", ingredient2=" + ingredient2 +
                    ", ingredient3=" + ingredient3 +
                    ", ingredient4=" + ingredient4 +
                    ", ingredient5=" + ingredient5 +
                    ", ingredient6=" + ingredient6 +
                    ", ingredient7=" + ingredient7 +
                    ", ingredients=" + ingredients +
                    '}';
        }

        public Builder setIngredient1(Ingredient ingredient1) {
            this.ingredient1 = ingredient1;
            return this;
        }

        public Builder setIngredient2(Ingredient ingredient2) {
            this.ingredient2 = ingredient2;
            return this;
        }

        public Builder setIngredient3(Ingredient ingredient3) {
            this.ingredient3 = ingredient3;
            return this;
        }

        public Builder setIngredient4(Ingredient ingredient4) {
            this.ingredient4 = ingredient4;
            return this;
        }

        public Builder setIngredient5(Ingredient ingredient5) {
            this.ingredient5 = ingredient5;
            return this;
        }

        public Builder setIngredient6(Ingredient ingredient6) {
            this.ingredient6 = ingredient6;
            return this;
        }

        public Builder setIngredient7(Ingredient ingredient7) {
            this.ingredient7 = ingredient7;
            return this;
        }

        public Builder setIngredients(List<Ingredient> ingredients) {
            ingredients.add(ingredient1);
            ingredients.add(ingredient2);
            ingredients.add(ingredient3);
            ingredients.add(ingredient4);
            ingredients.add(ingredient5);
            ingredients.add(ingredient6);
            ingredients.add(ingredient7);
            this.ingredients = ingredients;
            return this;
        }
    }
}
