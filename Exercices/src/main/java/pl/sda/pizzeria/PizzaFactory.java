package pl.sda.pizzeria;

import java.util.List;

public class PizzaFactory extends Pizza{



    public static Pizza.Builder getHawai(){
        Ingredient ingredient4 = new Ingredient("pineaplle",24);
        Ingredient ingredient5 = new Ingredient("chicken",32);
        Pizza.Builder hawai = new Pizza.Builder()
                .setIngredient4(ingredient4)
                .setIngredient5(ingredient5);
        return hawai;
    }

    public static Pizza.Builder getCheese(){
        Ingredient ingredient4 = new Ingredient("ementaler",4);
        Ingredient ingredient5 = new Ingredient("mozzarella",4);
        Ingredient ingredient6 = new Ingredient("gorgonzola",4);
        Ingredient ingredient7 = new Ingredient("parmezan",4);
        Pizza.Builder cheese = new Pizza.Builder()
                .setIngredient4(ingredient4)
                .setIngredient5(ingredient5)
                .setIngredient6(ingredient6)
                .setIngredient7(ingredient7);
        return cheese;
    }

    public static Pizza.Builder getVillage(){
        Ingredient ingredient4 = new Ingredient("sausage",20);
        Ingredient ingredient5 = new Ingredient("bacon",20);
        Ingredient ingredient6 = new Ingredient("pea",20);
        Ingredient ingredient7 = new Ingredient("onion",20);
        Pizza.Builder village = new Pizza.Builder()
                .setIngredient4(ingredient4)
                .setIngredient5(ingredient5)
                .setIngredient6(ingredient6)
                .setIngredient7(ingredient7);
        return village;
    }


}
