package pl.sda.classRegister;

public class StudentsData {
    private String name;
    private String lastName;
    private String id;

    public StudentsData(String name, String lastName, String id) {
        this.name = name;
        this.lastName = lastName;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getId() {
        return id;
    }
}
