package pl.sda.classRegister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassRegister {
    private HashMap<String,StudentsData> students;
    private HashMap<String,StudentGrades> grades;

    public void addStudent(String id,String name,String lastName){
        StudentsData student = new StudentsData(name,lastName,id);
        students.put(id,student);
    }

    public void addGradeToStudent(String id,Subjects subject, Double grade){
        StudentGrades studentGrades = new StudentGrades();
        studentGrades.addGrade(subject,grade);
        grades.put(id,studentGrades);
    }

    public void averegeOfStudentGrades(String id){
        System.out.println(grades.get(id).getAverge());
    }

    public void gradesOfStudent(String id){
        for (Map.Entry<Subjects,List<Double>> gradesFromSubject :
                grades.get(id).getGrades().entrySet()){
            System.out.println("" + gradesFromSubject.getKey()
                    + " : " + gradesFromSubject.getValue());
        }
    }


}
