package pl.sda.classRegister;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.OptionalDouble;

public class StudentGrades {
    private HashMap<Subjects,List<Double>> grades = new HashMap<Subjects, List<Double>>();

   public void addGrade(Subjects subject ,Double grade){
       List<Double> gradesFromSubject = grades.get(subject);
       if (gradesFromSubject == null){
           gradesFromSubject = new ArrayList<Double>();
       }
       gradesFromSubject.add(grade);
       grades.put(subject,gradesFromSubject);
   }

   public List<Double> getGradesFromSubject(Subjects subjects){
       return grades.get(subjects);

   }

   public Double getAverge(){
       OptionalDouble averge = grades.values()
               .stream().mapToDouble(list -> list
               .stream().mapToDouble(e -> e).average().getAsDouble()).average();
       return averge.getAsDouble();
   }

    public HashMap<Subjects, List<Double>> getGrades() {
        return grades;
    }
}
