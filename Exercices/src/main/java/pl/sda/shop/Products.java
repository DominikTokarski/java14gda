package pl.sda.shop;

public class Products {
    private String productName;
    private double price;
    private ProductClass productClass;
    private ProductType productType;

    @Override
    public String toString() {
        return "Products{" +
                "productName='" + productName + '\'' +
                ", price=" + price +
                ", productClass=" + productClass +
                ", productType=" + productType +
                '}';
    }

    public Products(String productName, double price,
                    ProductClass productClass, ProductType productType) {
        this.productName = productName;
        this.price = price;
        this.productClass = productClass;
        this.productType = productType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ProductClass getProductClass() {
        return productClass;
    }

    public void setProductClass(ProductClass productClass) {
        this.productClass = productClass;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }
}
