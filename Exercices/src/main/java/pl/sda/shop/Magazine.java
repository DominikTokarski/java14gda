package pl.sda.shop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Magazine {
    private Map<ProductClass,List<Products>> state = new HashMap<>();

    public void addProduct(String productName, double price,
                           ProductType productType, ProductClass productClass){
        Products product = new Products(productName,price,productClass,productType);
        List<Products> list = state.get(productClass);
        if (list == null){
            list = new ArrayList<>();
        }
        list.add(product);
        state.put(productClass,list);

    }





}
