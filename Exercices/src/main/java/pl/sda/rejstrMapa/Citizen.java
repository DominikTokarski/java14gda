package pl.sda.rejstrMapa;

public class Citizen {
    private String id;
    private String name;
    private String lastName;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public Citizen(String id, String name, String lastName) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
    }
}
