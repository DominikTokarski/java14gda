package pl.sda.rejstrMapa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CitizenRegistry {
    private HashMap<String,Citizen> citizenRegistry;

    public void addCitizen(String id,String name,String lastName){
        Citizen citizen = new Citizen(id,name,lastName);
        citizenRegistry.put(id,citizen);
    }

    public List<Citizen> findCitizensBirthUnderYear(int year){
        List<Citizen> OlderCitizens = new ArrayList<Citizen>();
        String years = String.valueOf(year);
        years = years.substring(2,3);
        for (String id: citizenRegistry.keySet()) {
            String idYear = id.substring(0,1);
            if (Integer.valueOf(idYear) < Integer.valueOf(years)){
                OlderCitizens.add(citizenRegistry.get(id));
            }

        }
        return OlderCitizens;
    }

    public  List<Citizen> findCitizensBirthInYearAndHaveName(int year,String name){
        List<Citizen> foundCitizens = new ArrayList<Citizen>();
        String years = String.valueOf(year);
        years = years.substring(2,3);
        for (String id: citizenRegistry.keySet()) {
            String idYear = id.substring(0,1);
            if (idYear.equals(years) && name.equals(citizenRegistry.get(id).getName())){
                foundCitizens.add(citizenRegistry.get(id));
            }

        }
        return foundCitizens;
    }

    public List<Citizen> findCitizenByLastName(String lastName){
        List<Citizen> foundCitizens = new ArrayList<Citizen>();
        for (String id: citizenRegistry.keySet()){
            if (citizenRegistry.get(id).getLastName().equals(lastName)){
                foundCitizens.add(citizenRegistry.get(id));
            }
        }
        return foundCitizens;
    }

    public List<Citizen> findCitizenId(String idSearch){
        List<Citizen> foundCitizens = new ArrayList<Citizen>();
        for (String id: citizenRegistry.keySet()){
            if (citizenRegistry.get(id).getId().equals(id)){
                foundCitizens.add(citizenRegistry.get(id));
            }
        }
        return foundCitizens;
    }
}
