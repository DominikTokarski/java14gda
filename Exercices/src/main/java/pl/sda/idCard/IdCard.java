package pl.sda.idCard;


public class IdCard {
    private static int[] factors = new int[]{7,3,1,9,7,3,1,7,3};

    public static void main(String[] args) {
        String IdNumber = "FEL055819";
        System.out.println(validateIdCard(IdNumber));

    }

    public static boolean validateIdCard(String IdNumber){
        if (IdNumber == null || IdNumber.length() != 9){
            return false;
        }
        String[] splited = IdNumber.split("");
        char[] changes = new char[splited.length];
        int[] numbers = new int[splited.length];
        int sum = 0;
        for (int i = 0; i < splited.length; i++) {
           changes[i] = splited[i].charAt(0);
           if (!Character.isLetter(changes[i]) && i < 3){
               return false;
           }
            if (Character.isLetter(changes[4])){
                return false;
            }
            if (Character.isLetter(changes[i])){
                numbers[i] = changes[i] - 55;
            }else numbers [i] = changes[i] - 48;

            sum += numbers[i] * factors[i];
        }
        return sum % 10 == 0;
    }
}
