package pl.sda.wiadomosci.java8;


import pl.sda.wiadomosci.MarketingNews;
import pl.sda.wiadomosci.Message;

import java.util.ArrayList;



public class Main {
    public static void main(String[] args) {
        NewStation station = new NewStation();
        station.addWatcher(new Watcher("Andrew",5));
        station.addWatcher(new Watcher("Kate",3));
        station.addWatcher(new Watcher("Malcolm",8));
        station.sendMessage(new Message(
                "Terrorist attac in London", 7));
        station.sendMarketingNews(new MarketingNews(new ArrayList<>()));
    }
}
