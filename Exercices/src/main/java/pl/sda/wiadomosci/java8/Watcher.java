package pl.sda.wiadomosci.java8;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import pl.sda.wiadomosci.MarketingNews;
import pl.sda.wiadomosci.Message;
import pl.sda.wiadomosci.Text;

public class Watcher implements ChangeListener<Text> {
    private String name;
    private int panic_level;

    public Watcher(String name, int panic_level) {
        this.name = name;
        this.panic_level = panic_level;
    }

    @Override
    public void changed(ObservableValue<? extends Text> observable, Text oldValue, Text newValue) {
        if (oldValue instanceof Message) {
            if (((Message) oldValue).getmessageImportance() > panic_level) {
                System.out.println(name + " starts panic");
            } else {
                System.out.println(name + " get message");
            }
        }else if (oldValue instanceof MarketingNews){
            System.out.println(name + " get message" + oldValue);
        }

    }

}
