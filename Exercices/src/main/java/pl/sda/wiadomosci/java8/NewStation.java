package pl.sda.wiadomosci.java8;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import pl.sda.wiadomosci.MarketingNews;
import pl.sda.wiadomosci.Message;
import pl.sda.wiadomosci.Text;

public class NewStation {
    private ObjectProperty<Text> messageObjectProperty =
            new SimpleObjectProperty<>();

    public void addWatcher(ChangeListener<Text> messageChangeListener){
        messageObjectProperty.addListener(messageChangeListener);
    }

    public void sendMessage(Text message){
        messageObjectProperty.setValue(message);
    }

    public void sendMarketingNews(Text message){
        messageObjectProperty.setValue(message);
    }
}
