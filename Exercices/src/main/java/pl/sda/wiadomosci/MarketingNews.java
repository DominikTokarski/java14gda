package pl.sda.wiadomosci;

import pl.sda.shop.Products;

import java.util.ArrayList;
import java.util.List;

public class MarketingNews extends Text{
 private List<Products> productsList;

    @Override
    public String toString() {
        return "MarketingNews{" +
                "productsList=" + productsList +
                '}';
    }

    public MarketingNews(List<Products> productsList) {
        this.productsList = productsList;
    }

    private void addProduct(Products product){
     productsList.add(product);
 }


}
