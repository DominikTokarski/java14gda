package pl.sda.wiadomosci.java7.station_Watcher;


import pl.sda.wiadomosci.Message;

import java.util.Observable;

public class NewStation extends Observable {

    public void sendMessage(Message message){
        setChanged();
        notifyObservers(message);
    }
}
