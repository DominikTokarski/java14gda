package pl.sda.wiadomosci.java7.station_Watcher;

import pl.sda.wiadomosci.Message;

import java.util.Observable;
import java.util.Observer;

public class Watcher implements Observer {
    private String name;
    private int panic_level;

    public Watcher(String name, int panic_level) {
        this.name = name;
        this.panic_level = panic_level;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Message) {
            if (((Message) arg).getmessageImportance() > panic_level) {
                System.out.println(name + " starts panic");
            } else {
                System.out.println(name + " get message");
            }
        }else {
            System.out.println(name + " get message");
        }

    }
}
