package pl.sda.wiadomosci.java7.station_Watcher;

import pl.sda.wiadomosci.Message;

public class Main {
    public static void main(String[] args) {
        NewStation station = new NewStation();
        station.addObserver(new Watcher("Andrew",5));
        station.addObserver(new Watcher("Kate",3));
        station.addObserver(new Watcher("Malcolm",8));
        station.sendMessage(new Message("Terrorist attac in London", 7));
    }
}
