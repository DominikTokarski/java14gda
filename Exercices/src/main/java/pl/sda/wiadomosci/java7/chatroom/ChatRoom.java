package pl.sda.wiadomosci.java7.chatroom;

import pl.sda.wiadomosci.Message;

import java.util.Observable;

public class ChatRoom extends Observable {

    public void sendMessage(Message message){
        setChanged();
        notifyObservers(message);
    }

}
