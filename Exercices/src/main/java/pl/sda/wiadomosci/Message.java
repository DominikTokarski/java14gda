package pl.sda.wiadomosci;

public class Message extends Text{
    private String content;
    private int messageImportance;

    public String getContent() {
        return content;
    }

    public int getmessageImportance() {
        return messageImportance;
    }

    public Message(String content, int messageImportance) {
        this.content = content;
        this.messageImportance = messageImportance;
    }
}
