package pl.sda.sms;

import java.util.Scanner;

public class SMSresizer {
    public static void main(String[] args) {

    }
//    public static void makeSmaller(){
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("New Message");
//        String sms = scanner.next().trim().toLowerCase();
//        String[] words = sms.split(" ");
//        for (int i = 0; i < words.length; i++) {
//            char first = words[i].charAt(0);
//            first = Character.toUpperCase(first);
//            String abc = words[i].substring(1);
//            System.out.print(first + abc);
//
//        }
//        System.out.println();
//        String test = sms.trim().replace(" ","");
//        int size = (test.length()) / 160;
//        if (size > 1){
//            size = (test.length() + size*7) / 160;
//        }else size = 1;
//        System.out.println("pay for " + size + " message");
//    }


    public static String makeSmaller(String sms){
        if (sms == null){
            return null;
        }
        String[] words = sms.trim().toLowerCase().split(" ");
        StringBuilder stringBuilder = new StringBuilder();
        for (String word: words) {
            if (word.length() > 0) {
                String newWord = word.substring(0, 1).toUpperCase() + word.substring(1);
                stringBuilder.append(newWord);
            }
        }
        System.out.println();
        String test = howMuch(sms);
        System.out.println(test);
        return stringBuilder.toString();
    }

    public static String howMuch(String sms){
        String test = sms.trim().replace(" ","");
        int size = (test.length()) / 160;
        if (size > 1){
            size = (test.length() + size*7) / 160;
        }else size = 1;
        return "pay for " + size + " message";
    }
}

