package pl.sda.sms;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class SMSresizerTest {
    @Test
    public void simpleTest(){

        assertEquals("AlaMaKota",SMSresizer.makeSmaller("Ala ma kota"));
    }

    @Test
    public void simpleTestSecond(){
        assertEquals("AlaMaKota",SMSresizer.makeSmaller("Ala ma    kota"));
    }

    @Test
    public void simpleTestThird(){
        assertEquals(null,SMSresizer.makeSmaller(null));
    }

    @Test
    public void simpleTestFourth(){
        assertEquals("",SMSresizer.makeSmaller(""));
    }

    @Test
    public void howMuch(){
        assertEquals("pay for 1 message",SMSresizer.howMuch("Ala ma kota"));
    }

}
