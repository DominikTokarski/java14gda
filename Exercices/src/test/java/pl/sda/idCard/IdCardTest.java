package pl.sda.idCard;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class IdCardTest {

    @Test
    public void shouldReturnTrue() {
        assertEquals(true, IdCard.validateIdCard("FEL055819"));
    }

    @Test
    public void shouldReturnFalse(){
        assertEquals(false,IdCard.validateIdCard("ABC000405"));
    }

    @Test
    public void shouldReturnFalseNull() {
        assertEquals(false, IdCard.validateIdCard(null));
    }

    @Test
    public void shouldReturnFalseBadSizeSmaller(){
        assertEquals(false,IdCard.validateIdCard("ABC6589"));
    }

    @Test
    public void shouldReturnFalseBadSizeBigger(){
        assertEquals(false,IdCard.validateIdCard("ABC6589214"));
    }

    @Test
    public void shouldReturnFalseOnlyLetters(){
        assertEquals(false,IdCard.validateIdCard("ABCDEFGHJ"));
    }

    @Test
    public void shouldReturnFalseOnlyNumbers(){
        assertEquals(false,IdCard.validateIdCard("3216589214"));
    }
}
