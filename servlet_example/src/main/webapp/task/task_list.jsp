<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 04.10.2018
  Time: 20:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task List</title>
</head>
<body>
<a href="/task/add">Task Form</a>
<h2>Task List:</h2>
<table>
    <thead>
    <th>Id</th>
    <th>Title</th>
    <th>Content</th>
    <th>Done</th>
    <th>Action Remove</th>
    <th>Action modify</th>
    </thead>
    <tbody>
    <c:forEach var="task" items="${taskList}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.title}"/>
            </td>
            <td>
                <c:out value="${task.content}"/>
            </td>
            <td>
                <c:out value="${task.done}"/>
            </td>
            <td>
                <a href="/task/remove?id=<c:out value="${task.id}"/>">Remove</a>
            </td>
            <td>
                <a href="/task/modify?id=<c:out value="${task.id}"/>">Modify</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</body>
</html>
