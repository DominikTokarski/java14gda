<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Dominik
  Date: 04.10.2018
  Time: 20:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task Form:</title>
</head>
<body>
<a href="/task/list">Task List</a>
<span style="color: red; ">
<c:out value="${error_message}"/>
</span>

<c:choose>
<c:when test="${not empty task_to_modify}">
<h2>Task Form:</h2>
<form action="/task/modify" method="post">
    <input type="hidden" name="id" value="<c:out value="${task_to_modify.id}"/>">
    <div>
        <label for="title">Title:</label>
        <input id="title" name="title" type="text" value="<c:out value="${task_to_modify.title}"/>">
    </div>
    <div>
        <label for="content">Content:</label>
        <input id="content" name="content" type="text" value="<c:out value="${task_to_modify.content}"/>">
    </div>
    <div>
        <label for="done">Done:</label>
        <c:choose>
            <c:when test="${task_to_modify.done=='true'}">
                <input id="done" name="done" type="checkbox" checked>
            </c:when>
            <c:otherwise>
                <input id="done" name="done" type="checkbox">
            </c:otherwise>
        </c:choose>
    </div>
    <input type="submit" value="Modify">
    </c:when>
    <c:otherwise>
    <h2>Task Form:</h2>
        <form action="/task/add" method="post">
        <div>
            <label for="title">Title:</label>
            <input id="title" name="title" type="text">
        </div>
        <div>
            <label for="content">Content:</label>
            <input id="content" name="content" type="text">
        </div>
        <div>
            <label for="done">Done:</label>
            <input id="done" name="done" type="checkbox">
        </div>
        <input type="submit" value="Add">
        </c:otherwise>
        </c:choose>
    </form>
</body>
</html>
