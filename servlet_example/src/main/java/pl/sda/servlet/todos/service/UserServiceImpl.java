package pl.sda.servlet.todos.service;

import pl.sda.servlet.todos.dao.UserDao;
import pl.sda.servlet.todos.model.AppUser;

import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public UserServiceImpl() {
        userDao = new UserDao();
    }

    public void addUser(AppUser newUser){
        userDao.add(newUser);
    }

    public List<AppUser> getUserList() {
        return userDao.getAllUsers();
    }

    @Override
    public void removeUserWithId(final int id) {
        userDao.removeUserWithId(id);
    }

    @Override
    public Optional<AppUser> getUserWithId(int userId) {
       return userDao.findUserWithId(userId);

    }

    @Override
    public void modify(AppUser userToModify) {
        userDao.updateUser(userToModify);
    }


}