package pl.sda.servlet.todos.dao;

import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import pl.sda.servlet.todos.model.TodoTask;
import pl.sda.servlet.todos.util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaskDao {
    public void add(TodoTask task) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()){
            transaction = session.beginTransaction();
            session.save(task);

            transaction.commit();
        }catch (SessionException sessionException){
            transaction.rollback();
            System.out.printf("Error");
        }
    }

    public List<TodoTask> getAllTasks() {
        List<TodoTask> list;
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()){
            transaction = session.beginTransaction();
            list = session.createQuery("from TodoTask", TodoTask.class).list();

            return list;
        }catch (SessionException sessionException){
            transaction.rollback();
            System.out.printf("Error");
        }
        return new ArrayList<>();
    }

    public void removeTaskWithId(int id) {
        Optional<TodoTask> taskOpt = findTaskWithId(id);
        if (taskOpt.isPresent()){
            TodoTask task = taskOpt.get();

            Transaction transaction = null;
            try (Session session = HibernateUtil.getSessionFactory().openSession()){
                transaction = session.beginTransaction();
                session.delete(task);

                transaction.commit();
            }catch (SessionException sessionException){
                transaction.rollback();
                System.out.printf("Error");
            }
        }
    }

    public Optional<TodoTask> findTaskWithId(int taskId) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()){
            transaction = session.beginTransaction();
            Query<TodoTask> query = session.createQuery("from TodoTask where id= :id");
            query.setParameter("id",taskId);

            TodoTask result = query.uniqueResult();

            return Optional.of(result);
        }catch (SessionException sessionException){
            transaction.rollback();
            System.out.printf("Error");
        }
        return Optional.empty();
    }

    public Optional<TodoTask> updateTask(TodoTask taskToModify) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()){
             transaction = session.beginTransaction();
//            Query<AppUser> userQuery = session.createQuery("update AppUser set login=: login, " +
//                    " password=: password where id= :id",AppUser.class);
//            userQuery.setParameter("login",userToModify.getLogin());
//            userQuery.setParameter("password",userToModify.getPassword());
//            userQuery.setParameter("id",userToModify.getId());

            session.saveOrUpdate(taskToModify);

//            System.out.printf("Changed: " + userQuery.executeUpdate() + " records");

            transaction.commit();
        }catch (SessionException sessionException){
            transaction.rollback();
            System.out.printf("Error");
        }
        return null;
    }
}
