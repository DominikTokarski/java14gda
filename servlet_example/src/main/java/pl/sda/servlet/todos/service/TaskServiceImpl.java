package pl.sda.servlet.todos.service;

import pl.sda.servlet.todos.dao.TaskDao;
import pl.sda.servlet.todos.model.AppUser;
import pl.sda.servlet.todos.model.TodoTask;

import java.util.List;
import java.util.Optional;

public class TaskServiceImpl implements TaskService{
    private TaskDao taskDao;
    public TaskServiceImpl() {
        taskDao = new TaskDao();
    }

    @Override
    public void addTask(TodoTask newTask) {
        taskDao.add(newTask);
    }

    @Override
    public List<TodoTask> getTaskList() {
        return taskDao.getAllTasks();
    }

    @Override
    public void removeTaskWithId(int id) {
        taskDao.removeTaskWithId(id);
    }


    public Optional<TodoTask> getTaskWithId(int taskId) {
        return taskDao.findTaskWithId(taskId);

    }
    @Override
    public void modify(TodoTask taskToModify) {
        taskDao.updateTask(taskToModify);
    }

}
