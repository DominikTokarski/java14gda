package pl.sda.servlet.todos.controller;


import pl.sda.servlet.todos.model.TodoTask;
import pl.sda.servlet.todos.service.DependencyManager;
import pl.sda.servlet.todos.service.TaskService;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/add")
public class TaskAddController extends HttpServlet {
    private TaskService taskService;

    public TaskAddController() {
        taskService = DependencyManager.getInstance().getBean(TaskService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/task/task_form.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("title");
        String content = req.getParameter("content");
        boolean done = req.getParameter("done") !=null ? req.getParameter("done").equals("on") : false;
        if (title.isEmpty() || content.isEmpty()){
            req.setAttribute("error_message","Incorrect title or content!");

            req.getRequestDispatcher("/task/task_form.jsp").forward(req,resp);
            return;
        }
        TodoTask task = new TodoTask(title,content,done);
        taskService.addTask(task);

        resp.sendRedirect("/task/list");
    }
}
