package pl.sda.servlet.todos.controller;

import pl.sda.servlet.todos.service.DependencyManager;
import pl.sda.servlet.todos.service.TaskService;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/remove")
public class TaskRemoveController extends HttpServlet {

    private TaskService taskService;

    public TaskRemoveController() {
        taskService = DependencyManager.getInstance().getBean(TaskService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("id")!= null){
            int id = Integer.parseInt(req.getParameter("id"));

            taskService.removeTaskWithId(id);
        }

        resp.sendRedirect("/task/list");
    }
}
