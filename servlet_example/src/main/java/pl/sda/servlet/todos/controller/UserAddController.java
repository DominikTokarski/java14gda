package pl.sda.servlet.todos.controller;

import pl.sda.servlet.todos.model.AppUser;
import pl.sda.servlet.todos.service.DependencyManager;
import pl.sda.servlet.todos.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/user/add")
public class UserAddController extends HttpServlet {

    private UserService userService;

    public UserAddController() {
        userService = DependencyManager .getInstance().getBean(UserService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/user/user_form.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String password_confirm = req.getParameter("password-confirm");

        if (username.isEmpty() || password.isEmpty() || !password.equals(password_confirm)){
            req.setAttribute("error_messege","Incorrect username or password!");

            req.getRequestDispatcher("/user/user_form.jsp").forward(req,resp);
            return;
        }
        AppUser user = new AppUser(username, password);
        userService.addUser(user);

        resp.sendRedirect("/user/list");
    }

}
