package pl.sda.servlet.todos.service;

import pl.sda.servlet.todos.model.TodoTask;

import java.util.List;
import java.util.Optional;

public interface TaskService {
    public void addTask(TodoTask newTask);

    public List<TodoTask> getTaskList();

    public void removeTaskWithId(int id);

    public Optional<TodoTask> getTaskWithId(int taskId);

    public void modify(TodoTask taskToModify);
}
