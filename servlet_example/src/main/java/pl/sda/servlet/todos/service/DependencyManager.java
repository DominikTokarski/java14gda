package pl.sda.servlet.todos.service;

import pl.sda.servlet.todos.dao.UserDao;

import java.util.HashMap;
import java.util.Map;

public class DependencyManager {
    private static DependencyManager ourInstance = new DependencyManager();

    public static DependencyManager getInstance() {
        return ourInstance;
    }

    private DependencyManager() {
        registerBean(UserDao.class, new UserDao());
        registerBean(UserService.class, new UserServiceImpl());
        registerBean(TaskService.class, new TaskServiceImpl());

    }
    private Map<Class<?>,Object> beanMap = new HashMap<>();
    public <T> T getBean(Class<?> classname){
        return (T) beanMap.get(classname);
    }

    private void registerBean(Class<?> className, Object o){
        beanMap.put(className, o);
    }
}
