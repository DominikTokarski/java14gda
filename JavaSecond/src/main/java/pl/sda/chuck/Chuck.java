package pl.sda.chuck;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.net.URL;

public class Chuck {
    public static void main(String[] args) {
        for (int i = 0; i < 10 ; i++) {
            try {
                URL url = new URL("https://api.chucknorris.io/jokes/random");
                InputStream is = url.openStream();
                BufferedReader in = new BufferedReader(new InputStreamReader(is));
                String inputLine = in.readLine();
                in.close();
                GsonBuilder builder = new GsonBuilder();
                Gson gson = new Gson();
                Joke joke = gson.fromJson(inputLine, Joke.class);
                System.out.println(joke.value);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
