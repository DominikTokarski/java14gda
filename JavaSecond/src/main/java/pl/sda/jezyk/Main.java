package pl.sda.jezyk;

import com.detectlanguage.DetectLanguage;
import com.detectlanguage.Result;
import com.detectlanguage.errors.APIError;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        DetectLanguage.apiKey = "a929ace35dc993776d968f2a33a9c8ff ";
        try {
            List<Result> results = DetectLanguage.detect(
                    "Moja mama ugotowała dzisiaj grochówke na obiad");
            Result result = results.get(0);
            System.out.println(result.language);
        } catch (APIError apiError) {
            apiError.printStackTrace();
        }
        File folder = new File("C:/Users/Dominik/Desktop/Java14GDA/JavaSecond/Artykuły");
        StringBuilder builder = new StringBuilder();
        File[] files = folder.listFiles();
        for (int i = 0; i < files.length; i++) {
            BufferedReader reader = new BufferedReader(new FileReader(files[i]));
            String laneFromStart;
            StringBuilder words = null;
            while ((laneFromStart = reader.readLine()) != null) {
                words.append(laneFromStart).append(" ");
            }
            try {
                List<Result> results = DetectLanguage.detect(words.toString());
                Result result = results.get(0);
                System.out.println(result.language);
            } catch (APIError apiError) {
                apiError.printStackTrace();
            }
        }
    }
}
